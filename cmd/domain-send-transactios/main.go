package main

import (
	"domain/pkg/log"
	"flag"
	"fmt"
	"github.com/prometheus/common/version"
	"math/rand"
	"os"
	"runtime"
	"time"
)

var (
	//showVersion Show version information then exit
	showVersion bool
	// configFile is the config flag.
	configFile string
)

func init() {
	flag.BoolVar(&showVersion, "version", false, "Show version information then exit.")
	flag.StringVar(&configFile, "config", "config.yaml", "config path, eg: -conf config.yaml")
}

func main() {
	defer log.Flush()

	rand.Seed(time.Now().UTC().UnixNano())
	//设置最大可用CPU数量，默认所有CPU
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	fmt.Println(time.Now())
	fmt.Println(version.Print("domain"))
	flag.Parse()
	if showVersion {
		os.Exit(0)
	}

	cfg, loader := loadCfg(configFile)

	app, cleanup := initApp(cfg, loader)
	defer cleanup()

	// start and wait for stop signal
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
