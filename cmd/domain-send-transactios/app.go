package main

import (
	"domain/app"
	"domain/app/server"
	"domain/internal/metrics"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
	sendTransactions "domain/internal/send-transactions"
)

func initApp(cfg *Config, srv ...server.Server) (*app.App, func()) {
	//初始化 redis
	rds := cache.New(&cfg.Redis)
	register := rds.RegisterServer("domain-transactions")
	if register != nil {
		srv = append(srv, register)
	}

	//初始化 MySQL
	db := dataBase.New(&cfg.MySQL)

	//启用指标监控
	if cfg.Metrics.Enabled {
		m := metrics.New(&cfg.Metrics)
		srv = append(srv, m)
	}

	//启用Send
	send := sendTransactions.New(&cfg.Send, rds, db)
	srv = append(srv, send)

	app := app.New(app.Name("domain-transactions"), app.Server(srv...))
	cleanup := func() {
		rds.Close()
		db.Close()
	}
	return app, cleanup
}
