package main

import (
	"domain/app"
	"domain/app/server"
	"domain/internal/metrics"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
	"domain/internal/trace"
)

func initApp(cfg *Config, srv ...server.Server) (*app.App, func()) {
	//初始化 redis
	rds := cache.New(&cfg.Redis)
	register := rds.RegisterServer("domain-trace")
	if register != nil {
		srv = append(srv, register)
	}

	//初始化 MySQL
	db := dataBase.New(&cfg.MySQL)

	//启用指标监控
	if cfg.Metrics.Enabled {
		m := metrics.New(&cfg.Metrics)
		srv = append(srv, m)
	}

	//启用Trace
	if cfg.Trace.Enabled {
		trace := trace.New(&cfg.Trace, rds, db)
		srv = append(srv, trace)
	}

	app := app.New(app.Name("domain-trace"), app.Server(srv...))
	cleanup := func() {
		rds.Close()
		db.Close()
	}
	return app, cleanup
}
