package main

import (
	"domain/internal/metrics"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/config"
	"domain/internal/pkg/dataBase"
	"domain/internal/send/conf"
)

// Config 全局配置信息
type Config struct {
	Redis   cache.Config    `yaml:"redis"`   //redis 配置项，存储挖矿信息
	MySQL   dataBase.Config `yaml:"mysql"`   //mysql 数据库配置项， 持久化挖矿数据、收益分成、矿机状态
	Trace   conf.Config     `yaml:"trace"`   //trace 相关配置
	Send    conf.Config     `yaml:"send"`    //通知相关配置
	Metrics metrics.Config  `yaml:"metrics"` //监控指标
}

func loadCfg(configFile string) (*Config, *config.Loader) {
	cfg := &Config{
		Redis:   cache.DefaultConfig,
		MySQL:   dataBase.DefaultConfig,
		Trace:   conf.DefaultConfig,
		Metrics: metrics.DefaultConfig,
	}

	loader := config.NewLoader(configFile, cfg)

	if err := loader.Load(); err != nil {
		panic(err)
	}
	return cfg, loader
}
