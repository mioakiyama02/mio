// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package DomainERC721

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// DomainERC721ABI is the input ABI used to generate the binding from.
const DomainERC721ABI = "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"}],\"name\":\"AddCuratorDomain\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"approved\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"}],\"name\":\"MintDomain\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"}],\"name\":\"RemoveCuratorDomain\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"DOMAIN_SEPARATOR\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"PERMIT_TYPEHASH\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainExchange\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"nonces\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"permit\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenOfOwnerByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"name_\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"symbol_\",\"type\":\"string\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"internalType\":\"bytes32[]\",\"name\":\"domainList\",\"type\":\"bytes32[]\"}],\"name\":\"addCuratorDomainList\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"internalType\":\"bytes32[]\",\"name\":\"domainList\",\"type\":\"bytes32[]\"}],\"name\":\"removeCuratorDomainList\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32[]\",\"name\":\"domainList\",\"type\":\"bytes32[]\"}],\"name\":\"mintBatch\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainExchange_\",\"type\":\"address\"}],\"name\":\"setDomainExchange\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"register\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"has\",\"type\":\"bool\"}],\"name\":\"setVaultRegistrant\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"register\",\"type\":\"address\"}],\"name\":\"getVaultRegistrant\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"domainVault\",\"type\":\"address\"}],\"name\":\"registerVault\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCuratorList\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"curatorList\",\"type\":\"address[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"}],\"name\":\"getTokenIdsByCurator\",\"outputs\":[{\"internalType\":\"uint256[]\",\"name\":\"\",\"type\":\"uint256[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getDomainByTokenId\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIdList\",\"type\":\"uint256[]\"}],\"name\":\"getDomainListByTokenIds\",\"outputs\":[{\"internalType\":\"bytes32[]\",\"name\":\"domainList\",\"type\":\"bytes32[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32[]\",\"name\":\"domainList\",\"type\":\"bytes32[]\"}],\"name\":\"getTokenIdsByDomainList\",\"outputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIdList\",\"type\":\"uint256[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"}],\"name\":\"getDomainListByCurator\",\"outputs\":[{\"internalType\":\"bytes32[]\",\"name\":\"\",\"type\":\"bytes32[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"}],\"name\":\"getCuratorByDomain\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getVaultByTokenId\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"domainVault\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getVaultList\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"domainVaultList\",\"type\":\"address[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainVault\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOfForVault\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

// DomainERC721 is an auto generated Go binding around an Ethereum contract.
type DomainERC721 struct {
	DomainERC721Caller     // Read-only binding to the contract
	DomainERC721Transactor // Write-only binding to the contract
	DomainERC721Filterer   // Log filterer for contract events
}

// DomainERC721Caller is an auto generated read-only Go binding around an Ethereum contract.
type DomainERC721Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainERC721Transactor is an auto generated write-only Go binding around an Ethereum contract.
type DomainERC721Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainERC721Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type DomainERC721Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainERC721Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type DomainERC721Session struct {
	Contract     *DomainERC721     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// DomainERC721CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type DomainERC721CallerSession struct {
	Contract *DomainERC721Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// DomainERC721TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type DomainERC721TransactorSession struct {
	Contract     *DomainERC721Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// DomainERC721Raw is an auto generated low-level Go binding around an Ethereum contract.
type DomainERC721Raw struct {
	Contract *DomainERC721 // Generic contract binding to access the raw methods on
}

// DomainERC721CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type DomainERC721CallerRaw struct {
	Contract *DomainERC721Caller // Generic read-only contract binding to access the raw methods on
}

// DomainERC721TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type DomainERC721TransactorRaw struct {
	Contract *DomainERC721Transactor // Generic write-only contract binding to access the raw methods on
}

// NewDomainERC721 creates a new instance of DomainERC721, bound to a specific deployed contract.
func NewDomainERC721(address common.Address, backend bind.ContractBackend) (*DomainERC721, error) {
	contract, err := bindDomainERC721(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &DomainERC721{DomainERC721Caller: DomainERC721Caller{contract: contract}, DomainERC721Transactor: DomainERC721Transactor{contract: contract}, DomainERC721Filterer: DomainERC721Filterer{contract: contract}}, nil
}

// NewDomainERC721Caller creates a new read-only instance of DomainERC721, bound to a specific deployed contract.
func NewDomainERC721Caller(address common.Address, caller bind.ContractCaller) (*DomainERC721Caller, error) {
	contract, err := bindDomainERC721(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &DomainERC721Caller{contract: contract}, nil
}

// NewDomainERC721Transactor creates a new write-only instance of DomainERC721, bound to a specific deployed contract.
func NewDomainERC721Transactor(address common.Address, transactor bind.ContractTransactor) (*DomainERC721Transactor, error) {
	contract, err := bindDomainERC721(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &DomainERC721Transactor{contract: contract}, nil
}

// NewDomainERC721Filterer creates a new log filterer instance of DomainERC721, bound to a specific deployed contract.
func NewDomainERC721Filterer(address common.Address, filterer bind.ContractFilterer) (*DomainERC721Filterer, error) {
	contract, err := bindDomainERC721(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &DomainERC721Filterer{contract: contract}, nil
}

// bindDomainERC721 binds a generic wrapper to an already deployed contract.
func bindDomainERC721(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(DomainERC721ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainERC721 *DomainERC721Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainERC721.Contract.DomainERC721Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainERC721 *DomainERC721Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainERC721.Contract.DomainERC721Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainERC721 *DomainERC721Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainERC721.Contract.DomainERC721Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainERC721 *DomainERC721CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainERC721.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainERC721 *DomainERC721TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainERC721.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainERC721 *DomainERC721TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainERC721.Contract.contract.Transact(opts, method, params...)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_DomainERC721 *DomainERC721Caller) DOMAINSEPARATOR(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "DOMAIN_SEPARATOR")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_DomainERC721 *DomainERC721Session) DOMAINSEPARATOR() ([32]byte, error) {
	return _DomainERC721.Contract.DOMAINSEPARATOR(&_DomainERC721.CallOpts)
}

// DOMAINSEPARATOR is a free data retrieval call binding the contract method 0x3644e515.
//
// Solidity: function DOMAIN_SEPARATOR() view returns(bytes32)
func (_DomainERC721 *DomainERC721CallerSession) DOMAINSEPARATOR() ([32]byte, error) {
	return _DomainERC721.Contract.DOMAINSEPARATOR(&_DomainERC721.CallOpts)
}

// PERMITTYPEHASH is a free data retrieval call binding the contract method 0x30adf81f.
//
// Solidity: function PERMIT_TYPEHASH() view returns(bytes32)
func (_DomainERC721 *DomainERC721Caller) PERMITTYPEHASH(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "PERMIT_TYPEHASH")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// PERMITTYPEHASH is a free data retrieval call binding the contract method 0x30adf81f.
//
// Solidity: function PERMIT_TYPEHASH() view returns(bytes32)
func (_DomainERC721 *DomainERC721Session) PERMITTYPEHASH() ([32]byte, error) {
	return _DomainERC721.Contract.PERMITTYPEHASH(&_DomainERC721.CallOpts)
}

// PERMITTYPEHASH is a free data retrieval call binding the contract method 0x30adf81f.
//
// Solidity: function PERMIT_TYPEHASH() view returns(bytes32)
func (_DomainERC721 *DomainERC721CallerSession) PERMITTYPEHASH() ([32]byte, error) {
	return _DomainERC721.Contract.PERMITTYPEHASH(&_DomainERC721.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_DomainERC721 *DomainERC721Caller) BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_DomainERC721 *DomainERC721Session) BalanceOf(owner common.Address) (*big.Int, error) {
	return _DomainERC721.Contract.BalanceOf(&_DomainERC721.CallOpts, owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_DomainERC721 *DomainERC721CallerSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _DomainERC721.Contract.BalanceOf(&_DomainERC721.CallOpts, owner)
}

// DomainExchange is a free data retrieval call binding the contract method 0x8081b4b0.
//
// Solidity: function domainExchange() view returns(address)
func (_DomainERC721 *DomainERC721Caller) DomainExchange(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "domainExchange")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainExchange is a free data retrieval call binding the contract method 0x8081b4b0.
//
// Solidity: function domainExchange() view returns(address)
func (_DomainERC721 *DomainERC721Session) DomainExchange() (common.Address, error) {
	return _DomainERC721.Contract.DomainExchange(&_DomainERC721.CallOpts)
}

// DomainExchange is a free data retrieval call binding the contract method 0x8081b4b0.
//
// Solidity: function domainExchange() view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) DomainExchange() (common.Address, error) {
	return _DomainERC721.Contract.DomainExchange(&_DomainERC721.CallOpts)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Caller) GetApproved(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getApproved", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Session) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.GetApproved(&_DomainERC721.CallOpts, tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.GetApproved(&_DomainERC721.CallOpts, tokenId)
}

// GetCuratorByDomain is a free data retrieval call binding the contract method 0x503bd8f8.
//
// Solidity: function getCuratorByDomain(bytes32 domain) view returns(address)
func (_DomainERC721 *DomainERC721Caller) GetCuratorByDomain(opts *bind.CallOpts, domain [32]byte) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getCuratorByDomain", domain)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetCuratorByDomain is a free data retrieval call binding the contract method 0x503bd8f8.
//
// Solidity: function getCuratorByDomain(bytes32 domain) view returns(address)
func (_DomainERC721 *DomainERC721Session) GetCuratorByDomain(domain [32]byte) (common.Address, error) {
	return _DomainERC721.Contract.GetCuratorByDomain(&_DomainERC721.CallOpts, domain)
}

// GetCuratorByDomain is a free data retrieval call binding the contract method 0x503bd8f8.
//
// Solidity: function getCuratorByDomain(bytes32 domain) view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) GetCuratorByDomain(domain [32]byte) (common.Address, error) {
	return _DomainERC721.Contract.GetCuratorByDomain(&_DomainERC721.CallOpts, domain)
}

// GetCuratorList is a free data retrieval call binding the contract method 0xe8406415.
//
// Solidity: function getCuratorList() view returns(address[] curatorList)
func (_DomainERC721 *DomainERC721Caller) GetCuratorList(opts *bind.CallOpts) ([]common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getCuratorList")

	if err != nil {
		return *new([]common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)

	return out0, err

}

// GetCuratorList is a free data retrieval call binding the contract method 0xe8406415.
//
// Solidity: function getCuratorList() view returns(address[] curatorList)
func (_DomainERC721 *DomainERC721Session) GetCuratorList() ([]common.Address, error) {
	return _DomainERC721.Contract.GetCuratorList(&_DomainERC721.CallOpts)
}

// GetCuratorList is a free data retrieval call binding the contract method 0xe8406415.
//
// Solidity: function getCuratorList() view returns(address[] curatorList)
func (_DomainERC721 *DomainERC721CallerSession) GetCuratorList() ([]common.Address, error) {
	return _DomainERC721.Contract.GetCuratorList(&_DomainERC721.CallOpts)
}

// GetDomainByTokenId is a free data retrieval call binding the contract method 0x965bd527.
//
// Solidity: function getDomainByTokenId(uint256 tokenId) view returns(bytes32)
func (_DomainERC721 *DomainERC721Caller) GetDomainByTokenId(opts *bind.CallOpts, tokenId *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getDomainByTokenId", tokenId)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// GetDomainByTokenId is a free data retrieval call binding the contract method 0x965bd527.
//
// Solidity: function getDomainByTokenId(uint256 tokenId) view returns(bytes32)
func (_DomainERC721 *DomainERC721Session) GetDomainByTokenId(tokenId *big.Int) ([32]byte, error) {
	return _DomainERC721.Contract.GetDomainByTokenId(&_DomainERC721.CallOpts, tokenId)
}

// GetDomainByTokenId is a free data retrieval call binding the contract method 0x965bd527.
//
// Solidity: function getDomainByTokenId(uint256 tokenId) view returns(bytes32)
func (_DomainERC721 *DomainERC721CallerSession) GetDomainByTokenId(tokenId *big.Int) ([32]byte, error) {
	return _DomainERC721.Contract.GetDomainByTokenId(&_DomainERC721.CallOpts, tokenId)
}

// GetDomainListByCurator is a free data retrieval call binding the contract method 0x7355bc8f.
//
// Solidity: function getDomainListByCurator(address curator) view returns(bytes32[])
func (_DomainERC721 *DomainERC721Caller) GetDomainListByCurator(opts *bind.CallOpts, curator common.Address) ([][32]byte, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getDomainListByCurator", curator)

	if err != nil {
		return *new([][32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([][32]byte)).(*[][32]byte)

	return out0, err

}

// GetDomainListByCurator is a free data retrieval call binding the contract method 0x7355bc8f.
//
// Solidity: function getDomainListByCurator(address curator) view returns(bytes32[])
func (_DomainERC721 *DomainERC721Session) GetDomainListByCurator(curator common.Address) ([][32]byte, error) {
	return _DomainERC721.Contract.GetDomainListByCurator(&_DomainERC721.CallOpts, curator)
}

// GetDomainListByCurator is a free data retrieval call binding the contract method 0x7355bc8f.
//
// Solidity: function getDomainListByCurator(address curator) view returns(bytes32[])
func (_DomainERC721 *DomainERC721CallerSession) GetDomainListByCurator(curator common.Address) ([][32]byte, error) {
	return _DomainERC721.Contract.GetDomainListByCurator(&_DomainERC721.CallOpts, curator)
}

// GetDomainListByTokenIds is a free data retrieval call binding the contract method 0x4787e3bd.
//
// Solidity: function getDomainListByTokenIds(uint256[] tokenIdList) view returns(bytes32[] domainList)
func (_DomainERC721 *DomainERC721Caller) GetDomainListByTokenIds(opts *bind.CallOpts, tokenIdList []*big.Int) ([][32]byte, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getDomainListByTokenIds", tokenIdList)

	if err != nil {
		return *new([][32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([][32]byte)).(*[][32]byte)

	return out0, err

}

// GetDomainListByTokenIds is a free data retrieval call binding the contract method 0x4787e3bd.
//
// Solidity: function getDomainListByTokenIds(uint256[] tokenIdList) view returns(bytes32[] domainList)
func (_DomainERC721 *DomainERC721Session) GetDomainListByTokenIds(tokenIdList []*big.Int) ([][32]byte, error) {
	return _DomainERC721.Contract.GetDomainListByTokenIds(&_DomainERC721.CallOpts, tokenIdList)
}

// GetDomainListByTokenIds is a free data retrieval call binding the contract method 0x4787e3bd.
//
// Solidity: function getDomainListByTokenIds(uint256[] tokenIdList) view returns(bytes32[] domainList)
func (_DomainERC721 *DomainERC721CallerSession) GetDomainListByTokenIds(tokenIdList []*big.Int) ([][32]byte, error) {
	return _DomainERC721.Contract.GetDomainListByTokenIds(&_DomainERC721.CallOpts, tokenIdList)
}

// GetTokenIdsByCurator is a free data retrieval call binding the contract method 0xfb5f11c7.
//
// Solidity: function getTokenIdsByCurator(address curator) view returns(uint256[])
func (_DomainERC721 *DomainERC721Caller) GetTokenIdsByCurator(opts *bind.CallOpts, curator common.Address) ([]*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getTokenIdsByCurator", curator)

	if err != nil {
		return *new([]*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)

	return out0, err

}

// GetTokenIdsByCurator is a free data retrieval call binding the contract method 0xfb5f11c7.
//
// Solidity: function getTokenIdsByCurator(address curator) view returns(uint256[])
func (_DomainERC721 *DomainERC721Session) GetTokenIdsByCurator(curator common.Address) ([]*big.Int, error) {
	return _DomainERC721.Contract.GetTokenIdsByCurator(&_DomainERC721.CallOpts, curator)
}

// GetTokenIdsByCurator is a free data retrieval call binding the contract method 0xfb5f11c7.
//
// Solidity: function getTokenIdsByCurator(address curator) view returns(uint256[])
func (_DomainERC721 *DomainERC721CallerSession) GetTokenIdsByCurator(curator common.Address) ([]*big.Int, error) {
	return _DomainERC721.Contract.GetTokenIdsByCurator(&_DomainERC721.CallOpts, curator)
}

// GetTokenIdsByDomainList is a free data retrieval call binding the contract method 0x05639645.
//
// Solidity: function getTokenIdsByDomainList(bytes32[] domainList) view returns(uint256[] tokenIdList)
func (_DomainERC721 *DomainERC721Caller) GetTokenIdsByDomainList(opts *bind.CallOpts, domainList [][32]byte) ([]*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getTokenIdsByDomainList", domainList)

	if err != nil {
		return *new([]*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)

	return out0, err

}

// GetTokenIdsByDomainList is a free data retrieval call binding the contract method 0x05639645.
//
// Solidity: function getTokenIdsByDomainList(bytes32[] domainList) view returns(uint256[] tokenIdList)
func (_DomainERC721 *DomainERC721Session) GetTokenIdsByDomainList(domainList [][32]byte) ([]*big.Int, error) {
	return _DomainERC721.Contract.GetTokenIdsByDomainList(&_DomainERC721.CallOpts, domainList)
}

// GetTokenIdsByDomainList is a free data retrieval call binding the contract method 0x05639645.
//
// Solidity: function getTokenIdsByDomainList(bytes32[] domainList) view returns(uint256[] tokenIdList)
func (_DomainERC721 *DomainERC721CallerSession) GetTokenIdsByDomainList(domainList [][32]byte) ([]*big.Int, error) {
	return _DomainERC721.Contract.GetTokenIdsByDomainList(&_DomainERC721.CallOpts, domainList)
}

// GetVaultByTokenId is a free data retrieval call binding the contract method 0xd2d33ba2.
//
// Solidity: function getVaultByTokenId(uint256 tokenId) view returns(address domainVault)
func (_DomainERC721 *DomainERC721Caller) GetVaultByTokenId(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getVaultByTokenId", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetVaultByTokenId is a free data retrieval call binding the contract method 0xd2d33ba2.
//
// Solidity: function getVaultByTokenId(uint256 tokenId) view returns(address domainVault)
func (_DomainERC721 *DomainERC721Session) GetVaultByTokenId(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.GetVaultByTokenId(&_DomainERC721.CallOpts, tokenId)
}

// GetVaultByTokenId is a free data retrieval call binding the contract method 0xd2d33ba2.
//
// Solidity: function getVaultByTokenId(uint256 tokenId) view returns(address domainVault)
func (_DomainERC721 *DomainERC721CallerSession) GetVaultByTokenId(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.GetVaultByTokenId(&_DomainERC721.CallOpts, tokenId)
}

// GetVaultList is a free data retrieval call binding the contract method 0xce385a92.
//
// Solidity: function getVaultList() view returns(address[] domainVaultList)
func (_DomainERC721 *DomainERC721Caller) GetVaultList(opts *bind.CallOpts) ([]common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getVaultList")

	if err != nil {
		return *new([]common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)

	return out0, err

}

// GetVaultList is a free data retrieval call binding the contract method 0xce385a92.
//
// Solidity: function getVaultList() view returns(address[] domainVaultList)
func (_DomainERC721 *DomainERC721Session) GetVaultList() ([]common.Address, error) {
	return _DomainERC721.Contract.GetVaultList(&_DomainERC721.CallOpts)
}

// GetVaultList is a free data retrieval call binding the contract method 0xce385a92.
//
// Solidity: function getVaultList() view returns(address[] domainVaultList)
func (_DomainERC721 *DomainERC721CallerSession) GetVaultList() ([]common.Address, error) {
	return _DomainERC721.Contract.GetVaultList(&_DomainERC721.CallOpts)
}

// GetVaultRegistrant is a free data retrieval call binding the contract method 0x73fbca34.
//
// Solidity: function getVaultRegistrant(address register) view returns(bool)
func (_DomainERC721 *DomainERC721Caller) GetVaultRegistrant(opts *bind.CallOpts, register common.Address) (bool, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "getVaultRegistrant", register)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// GetVaultRegistrant is a free data retrieval call binding the contract method 0x73fbca34.
//
// Solidity: function getVaultRegistrant(address register) view returns(bool)
func (_DomainERC721 *DomainERC721Session) GetVaultRegistrant(register common.Address) (bool, error) {
	return _DomainERC721.Contract.GetVaultRegistrant(&_DomainERC721.CallOpts, register)
}

// GetVaultRegistrant is a free data retrieval call binding the contract method 0x73fbca34.
//
// Solidity: function getVaultRegistrant(address register) view returns(bool)
func (_DomainERC721 *DomainERC721CallerSession) GetVaultRegistrant(register common.Address) (bool, error) {
	return _DomainERC721.Contract.GetVaultRegistrant(&_DomainERC721.CallOpts, register)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_DomainERC721 *DomainERC721Caller) IsApprovedForAll(opts *bind.CallOpts, owner common.Address, operator common.Address) (bool, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "isApprovedForAll", owner, operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_DomainERC721 *DomainERC721Session) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _DomainERC721.Contract.IsApprovedForAll(&_DomainERC721.CallOpts, owner, operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_DomainERC721 *DomainERC721CallerSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _DomainERC721.Contract.IsApprovedForAll(&_DomainERC721.CallOpts, owner, operator)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_DomainERC721 *DomainERC721Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_DomainERC721 *DomainERC721Session) Name() (string, error) {
	return _DomainERC721.Contract.Name(&_DomainERC721.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_DomainERC721 *DomainERC721CallerSession) Name() (string, error) {
	return _DomainERC721.Contract.Name(&_DomainERC721.CallOpts)
}

// Nonces is a free data retrieval call binding the contract method 0x141a468c.
//
// Solidity: function nonces(uint256 tokenId) view returns(uint256)
func (_DomainERC721 *DomainERC721Caller) Nonces(opts *bind.CallOpts, tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "nonces", tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Nonces is a free data retrieval call binding the contract method 0x141a468c.
//
// Solidity: function nonces(uint256 tokenId) view returns(uint256)
func (_DomainERC721 *DomainERC721Session) Nonces(tokenId *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.Nonces(&_DomainERC721.CallOpts, tokenId)
}

// Nonces is a free data retrieval call binding the contract method 0x141a468c.
//
// Solidity: function nonces(uint256 tokenId) view returns(uint256)
func (_DomainERC721 *DomainERC721CallerSession) Nonces(tokenId *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.Nonces(&_DomainERC721.CallOpts, tokenId)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainERC721 *DomainERC721Caller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainERC721 *DomainERC721Session) Owner() (common.Address, error) {
	return _DomainERC721.Contract.Owner(&_DomainERC721.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) Owner() (common.Address, error) {
	return _DomainERC721.Contract.Owner(&_DomainERC721.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Caller) OwnerOf(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "ownerOf", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Session) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.OwnerOf(&_DomainERC721.CallOpts, tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.OwnerOf(&_DomainERC721.CallOpts, tokenId)
}

// OwnerOfForVault is a free data retrieval call binding the contract method 0xc243f82d.
//
// Solidity: function ownerOfForVault(address domainVault, uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Caller) OwnerOfForVault(opts *bind.CallOpts, domainVault common.Address, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "ownerOfForVault", domainVault, tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOfForVault is a free data retrieval call binding the contract method 0xc243f82d.
//
// Solidity: function ownerOfForVault(address domainVault, uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721Session) OwnerOfForVault(domainVault common.Address, tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.OwnerOfForVault(&_DomainERC721.CallOpts, domainVault, tokenId)
}

// OwnerOfForVault is a free data retrieval call binding the contract method 0xc243f82d.
//
// Solidity: function ownerOfForVault(address domainVault, uint256 tokenId) view returns(address)
func (_DomainERC721 *DomainERC721CallerSession) OwnerOfForVault(domainVault common.Address, tokenId *big.Int) (common.Address, error) {
	return _DomainERC721.Contract.OwnerOfForVault(&_DomainERC721.CallOpts, domainVault, tokenId)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainERC721 *DomainERC721Caller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainERC721 *DomainERC721Session) Paused() (bool, error) {
	return _DomainERC721.Contract.Paused(&_DomainERC721.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainERC721 *DomainERC721CallerSession) Paused() (bool, error) {
	return _DomainERC721.Contract.Paused(&_DomainERC721.CallOpts)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_DomainERC721 *DomainERC721Caller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_DomainERC721 *DomainERC721Session) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _DomainERC721.Contract.SupportsInterface(&_DomainERC721.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_DomainERC721 *DomainERC721CallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _DomainERC721.Contract.SupportsInterface(&_DomainERC721.CallOpts, interfaceId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_DomainERC721 *DomainERC721Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_DomainERC721 *DomainERC721Session) Symbol() (string, error) {
	return _DomainERC721.Contract.Symbol(&_DomainERC721.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_DomainERC721 *DomainERC721CallerSession) Symbol() (string, error) {
	return _DomainERC721.Contract.Symbol(&_DomainERC721.CallOpts)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721Caller) TokenByIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "tokenByIndex", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721Session) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.TokenByIndex(&_DomainERC721.CallOpts, index)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721CallerSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.TokenByIndex(&_DomainERC721.CallOpts, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721Caller) TokenOfOwnerByIndex(opts *bind.CallOpts, owner common.Address, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "tokenOfOwnerByIndex", owner, index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721Session) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.TokenOfOwnerByIndex(&_DomainERC721.CallOpts, owner, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_DomainERC721 *DomainERC721CallerSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _DomainERC721.Contract.TokenOfOwnerByIndex(&_DomainERC721.CallOpts, owner, index)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_DomainERC721 *DomainERC721Caller) TokenURI(opts *bind.CallOpts, tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "tokenURI", tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_DomainERC721 *DomainERC721Session) TokenURI(tokenId *big.Int) (string, error) {
	return _DomainERC721.Contract.TokenURI(&_DomainERC721.CallOpts, tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_DomainERC721 *DomainERC721CallerSession) TokenURI(tokenId *big.Int) (string, error) {
	return _DomainERC721.Contract.TokenURI(&_DomainERC721.CallOpts, tokenId)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_DomainERC721 *DomainERC721Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainERC721.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_DomainERC721 *DomainERC721Session) TotalSupply() (*big.Int, error) {
	return _DomainERC721.Contract.TotalSupply(&_DomainERC721.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_DomainERC721 *DomainERC721CallerSession) TotalSupply() (*big.Int, error) {
	return _DomainERC721.Contract.TotalSupply(&_DomainERC721.CallOpts)
}

// AddCuratorDomainList is a paid mutator transaction binding the contract method 0xa57cf57d.
//
// Solidity: function addCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Transactor) AddCuratorDomainList(opts *bind.TransactOpts, curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "addCuratorDomainList", curator, domainList)
}

// AddCuratorDomainList is a paid mutator transaction binding the contract method 0xa57cf57d.
//
// Solidity: function addCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Session) AddCuratorDomainList(curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.AddCuratorDomainList(&_DomainERC721.TransactOpts, curator, domainList)
}

// AddCuratorDomainList is a paid mutator transaction binding the contract method 0xa57cf57d.
//
// Solidity: function addCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721TransactorSession) AddCuratorDomainList(curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.AddCuratorDomainList(&_DomainERC721.TransactOpts, curator, domainList)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Transactor) Approve(opts *bind.TransactOpts, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "approve", to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Session) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.Approve(&_DomainERC721.TransactOpts, to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721TransactorSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.Approve(&_DomainERC721.TransactOpts, to, tokenId)
}

// Initialize is a paid mutator transaction binding the contract method 0x4cd88b76.
//
// Solidity: function initialize(string name_, string symbol_) returns()
func (_DomainERC721 *DomainERC721Transactor) Initialize(opts *bind.TransactOpts, name_ string, symbol_ string) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "initialize", name_, symbol_)
}

// Initialize is a paid mutator transaction binding the contract method 0x4cd88b76.
//
// Solidity: function initialize(string name_, string symbol_) returns()
func (_DomainERC721 *DomainERC721Session) Initialize(name_ string, symbol_ string) (*types.Transaction, error) {
	return _DomainERC721.Contract.Initialize(&_DomainERC721.TransactOpts, name_, symbol_)
}

// Initialize is a paid mutator transaction binding the contract method 0x4cd88b76.
//
// Solidity: function initialize(string name_, string symbol_) returns()
func (_DomainERC721 *DomainERC721TransactorSession) Initialize(name_ string, symbol_ string) (*types.Transaction, error) {
	return _DomainERC721.Contract.Initialize(&_DomainERC721.TransactOpts, name_, symbol_)
}

// Mint is a paid mutator transaction binding the contract method 0xadf2cead.
//
// Solidity: function mint(bytes32 domain) returns()
func (_DomainERC721 *DomainERC721Transactor) Mint(opts *bind.TransactOpts, domain [32]byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "mint", domain)
}

// Mint is a paid mutator transaction binding the contract method 0xadf2cead.
//
// Solidity: function mint(bytes32 domain) returns()
func (_DomainERC721 *DomainERC721Session) Mint(domain [32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.Mint(&_DomainERC721.TransactOpts, domain)
}

// Mint is a paid mutator transaction binding the contract method 0xadf2cead.
//
// Solidity: function mint(bytes32 domain) returns()
func (_DomainERC721 *DomainERC721TransactorSession) Mint(domain [32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.Mint(&_DomainERC721.TransactOpts, domain)
}

// MintBatch is a paid mutator transaction binding the contract method 0xbdc49095.
//
// Solidity: function mintBatch(bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Transactor) MintBatch(opts *bind.TransactOpts, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "mintBatch", domainList)
}

// MintBatch is a paid mutator transaction binding the contract method 0xbdc49095.
//
// Solidity: function mintBatch(bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Session) MintBatch(domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.MintBatch(&_DomainERC721.TransactOpts, domainList)
}

// MintBatch is a paid mutator transaction binding the contract method 0xbdc49095.
//
// Solidity: function mintBatch(bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721TransactorSession) MintBatch(domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.MintBatch(&_DomainERC721.TransactOpts, domainList)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainERC721 *DomainERC721Transactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainERC721 *DomainERC721Session) Pause() (*types.Transaction, error) {
	return _DomainERC721.Contract.Pause(&_DomainERC721.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainERC721 *DomainERC721TransactorSession) Pause() (*types.Transaction, error) {
	return _DomainERC721.Contract.Pause(&_DomainERC721.TransactOpts)
}

// Permit is a paid mutator transaction binding the contract method 0x7ac2ff7b.
//
// Solidity: function permit(address spender, uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) payable returns()
func (_DomainERC721 *DomainERC721Transactor) Permit(opts *bind.TransactOpts, spender common.Address, tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "permit", spender, tokenId, deadline, v, r, s)
}

// Permit is a paid mutator transaction binding the contract method 0x7ac2ff7b.
//
// Solidity: function permit(address spender, uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) payable returns()
func (_DomainERC721 *DomainERC721Session) Permit(spender common.Address, tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.Permit(&_DomainERC721.TransactOpts, spender, tokenId, deadline, v, r, s)
}

// Permit is a paid mutator transaction binding the contract method 0x7ac2ff7b.
//
// Solidity: function permit(address spender, uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) payable returns()
func (_DomainERC721 *DomainERC721TransactorSession) Permit(spender common.Address, tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.Permit(&_DomainERC721.TransactOpts, spender, tokenId, deadline, v, r, s)
}

// RegisterVault is a paid mutator transaction binding the contract method 0x6a51fd63.
//
// Solidity: function registerVault(uint256 tokenId, address domainVault) returns()
func (_DomainERC721 *DomainERC721Transactor) RegisterVault(opts *bind.TransactOpts, tokenId *big.Int, domainVault common.Address) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "registerVault", tokenId, domainVault)
}

// RegisterVault is a paid mutator transaction binding the contract method 0x6a51fd63.
//
// Solidity: function registerVault(uint256 tokenId, address domainVault) returns()
func (_DomainERC721 *DomainERC721Session) RegisterVault(tokenId *big.Int, domainVault common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.RegisterVault(&_DomainERC721.TransactOpts, tokenId, domainVault)
}

// RegisterVault is a paid mutator transaction binding the contract method 0x6a51fd63.
//
// Solidity: function registerVault(uint256 tokenId, address domainVault) returns()
func (_DomainERC721 *DomainERC721TransactorSession) RegisterVault(tokenId *big.Int, domainVault common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.RegisterVault(&_DomainERC721.TransactOpts, tokenId, domainVault)
}

// RemoveCuratorDomainList is a paid mutator transaction binding the contract method 0x6f29115c.
//
// Solidity: function removeCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Transactor) RemoveCuratorDomainList(opts *bind.TransactOpts, curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "removeCuratorDomainList", curator, domainList)
}

// RemoveCuratorDomainList is a paid mutator transaction binding the contract method 0x6f29115c.
//
// Solidity: function removeCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721Session) RemoveCuratorDomainList(curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.RemoveCuratorDomainList(&_DomainERC721.TransactOpts, curator, domainList)
}

// RemoveCuratorDomainList is a paid mutator transaction binding the contract method 0x6f29115c.
//
// Solidity: function removeCuratorDomainList(address curator, bytes32[] domainList) returns()
func (_DomainERC721 *DomainERC721TransactorSession) RemoveCuratorDomainList(curator common.Address, domainList [][32]byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.RemoveCuratorDomainList(&_DomainERC721.TransactOpts, curator, domainList)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainERC721 *DomainERC721Transactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainERC721 *DomainERC721Session) RenounceOwnership() (*types.Transaction, error) {
	return _DomainERC721.Contract.RenounceOwnership(&_DomainERC721.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainERC721 *DomainERC721TransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainERC721.Contract.RenounceOwnership(&_DomainERC721.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Transactor) SafeTransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "safeTransferFrom", from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Session) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.SafeTransferFrom(&_DomainERC721.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721TransactorSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.SafeTransferFrom(&_DomainERC721.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_DomainERC721 *DomainERC721Transactor) SafeTransferFrom0(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "safeTransferFrom0", from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_DomainERC721 *DomainERC721Session) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.SafeTransferFrom0(&_DomainERC721.TransactOpts, from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_DomainERC721 *DomainERC721TransactorSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _DomainERC721.Contract.SafeTransferFrom0(&_DomainERC721.TransactOpts, from, to, tokenId, _data)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_DomainERC721 *DomainERC721Transactor) SetApprovalForAll(opts *bind.TransactOpts, operator common.Address, approved bool) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "setApprovalForAll", operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_DomainERC721 *DomainERC721Session) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetApprovalForAll(&_DomainERC721.TransactOpts, operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_DomainERC721 *DomainERC721TransactorSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetApprovalForAll(&_DomainERC721.TransactOpts, operator, approved)
}

// SetDomainExchange is a paid mutator transaction binding the contract method 0x443a386a.
//
// Solidity: function setDomainExchange(address domainExchange_) returns()
func (_DomainERC721 *DomainERC721Transactor) SetDomainExchange(opts *bind.TransactOpts, domainExchange_ common.Address) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "setDomainExchange", domainExchange_)
}

// SetDomainExchange is a paid mutator transaction binding the contract method 0x443a386a.
//
// Solidity: function setDomainExchange(address domainExchange_) returns()
func (_DomainERC721 *DomainERC721Session) SetDomainExchange(domainExchange_ common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetDomainExchange(&_DomainERC721.TransactOpts, domainExchange_)
}

// SetDomainExchange is a paid mutator transaction binding the contract method 0x443a386a.
//
// Solidity: function setDomainExchange(address domainExchange_) returns()
func (_DomainERC721 *DomainERC721TransactorSession) SetDomainExchange(domainExchange_ common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetDomainExchange(&_DomainERC721.TransactOpts, domainExchange_)
}

// SetVaultRegistrant is a paid mutator transaction binding the contract method 0x0cbdf3ba.
//
// Solidity: function setVaultRegistrant(address register, bool has) returns()
func (_DomainERC721 *DomainERC721Transactor) SetVaultRegistrant(opts *bind.TransactOpts, register common.Address, has bool) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "setVaultRegistrant", register, has)
}

// SetVaultRegistrant is a paid mutator transaction binding the contract method 0x0cbdf3ba.
//
// Solidity: function setVaultRegistrant(address register, bool has) returns()
func (_DomainERC721 *DomainERC721Session) SetVaultRegistrant(register common.Address, has bool) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetVaultRegistrant(&_DomainERC721.TransactOpts, register, has)
}

// SetVaultRegistrant is a paid mutator transaction binding the contract method 0x0cbdf3ba.
//
// Solidity: function setVaultRegistrant(address register, bool has) returns()
func (_DomainERC721 *DomainERC721TransactorSession) SetVaultRegistrant(register common.Address, has bool) (*types.Transaction, error) {
	return _DomainERC721.Contract.SetVaultRegistrant(&_DomainERC721.TransactOpts, register, has)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Transactor) TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "transferFrom", from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721Session) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.TransferFrom(&_DomainERC721.TransactOpts, from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_DomainERC721 *DomainERC721TransactorSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainERC721.Contract.TransferFrom(&_DomainERC721.TransactOpts, from, to, tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainERC721 *DomainERC721Transactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainERC721 *DomainERC721Session) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.TransferOwnership(&_DomainERC721.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainERC721 *DomainERC721TransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainERC721.Contract.TransferOwnership(&_DomainERC721.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainERC721 *DomainERC721Transactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainERC721.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainERC721 *DomainERC721Session) Unpause() (*types.Transaction, error) {
	return _DomainERC721.Contract.Unpause(&_DomainERC721.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainERC721 *DomainERC721TransactorSession) Unpause() (*types.Transaction, error) {
	return _DomainERC721.Contract.Unpause(&_DomainERC721.TransactOpts)
}

// DomainERC721AddCuratorDomainIterator is returned from FilterAddCuratorDomain and is used to iterate over the raw logs and unpacked data for AddCuratorDomain events raised by the DomainERC721 contract.
type DomainERC721AddCuratorDomainIterator struct {
	Event *DomainERC721AddCuratorDomain // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721AddCuratorDomainIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721AddCuratorDomain)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721AddCuratorDomain)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721AddCuratorDomainIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721AddCuratorDomainIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721AddCuratorDomain represents a AddCuratorDomain event raised by the DomainERC721 contract.
type DomainERC721AddCuratorDomain struct {
	Curator common.Address
	Domain  [32]byte
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterAddCuratorDomain is a free log retrieval operation binding the contract event 0x060b36e89c68b9a990cb70879e15e01f11f1e1809cf4f1b9c7626eeaef16dd35.
//
// Solidity: event AddCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) FilterAddCuratorDomain(opts *bind.FilterOpts) (*DomainERC721AddCuratorDomainIterator, error) {

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "AddCuratorDomain")
	if err != nil {
		return nil, err
	}
	return &DomainERC721AddCuratorDomainIterator{contract: _DomainERC721.contract, event: "AddCuratorDomain", logs: logs, sub: sub}, nil
}

// WatchAddCuratorDomain is a free log subscription operation binding the contract event 0x060b36e89c68b9a990cb70879e15e01f11f1e1809cf4f1b9c7626eeaef16dd35.
//
// Solidity: event AddCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) WatchAddCuratorDomain(opts *bind.WatchOpts, sink chan<- *DomainERC721AddCuratorDomain) (event.Subscription, error) {

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "AddCuratorDomain")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721AddCuratorDomain)
				if err := _DomainERC721.contract.UnpackLog(event, "AddCuratorDomain", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAddCuratorDomain is a log parse operation binding the contract event 0x060b36e89c68b9a990cb70879e15e01f11f1e1809cf4f1b9c7626eeaef16dd35.
//
// Solidity: event AddCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) ParseAddCuratorDomain(log types.Log) (*DomainERC721AddCuratorDomain, error) {
	event := new(DomainERC721AddCuratorDomain)
	if err := _DomainERC721.contract.UnpackLog(event, "AddCuratorDomain", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the DomainERC721 contract.
type DomainERC721ApprovalIterator struct {
	Event *DomainERC721Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721Approval represents a Approval event raised by the DomainERC721 contract.
type DomainERC721Approval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, tokenId []*big.Int) (*DomainERC721ApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainERC721ApprovalIterator{contract: _DomainERC721.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *DomainERC721Approval, owner []common.Address, approved []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721Approval)
				if err := _DomainERC721.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) ParseApproval(log types.Log) (*DomainERC721Approval, error) {
	event := new(DomainERC721Approval)
	if err := _DomainERC721.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721ApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the DomainERC721 contract.
type DomainERC721ApprovalForAllIterator struct {
	Event *DomainERC721ApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721ApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721ApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721ApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721ApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721ApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721ApprovalForAll represents a ApprovalForAll event raised by the DomainERC721 contract.
type DomainERC721ApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_DomainERC721 *DomainERC721Filterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*DomainERC721ApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &DomainERC721ApprovalForAllIterator{contract: _DomainERC721.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_DomainERC721 *DomainERC721Filterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *DomainERC721ApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721ApprovalForAll)
				if err := _DomainERC721.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_DomainERC721 *DomainERC721Filterer) ParseApprovalForAll(log types.Log) (*DomainERC721ApprovalForAll, error) {
	event := new(DomainERC721ApprovalForAll)
	if err := _DomainERC721.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721MintDomainIterator is returned from FilterMintDomain and is used to iterate over the raw logs and unpacked data for MintDomain events raised by the DomainERC721 contract.
type DomainERC721MintDomainIterator struct {
	Event *DomainERC721MintDomain // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721MintDomainIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721MintDomain)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721MintDomain)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721MintDomainIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721MintDomainIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721MintDomain represents a MintDomain event raised by the DomainERC721 contract.
type DomainERC721MintDomain struct {
	Curator common.Address
	TokenId *big.Int
	Domain  [32]byte
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterMintDomain is a free log retrieval operation binding the contract event 0x89402e274e8337c1f640b7d84153793cd3d97479756105a5635de5c4d8fd16fc.
//
// Solidity: event MintDomain(address curator, uint256 tokenId, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) FilterMintDomain(opts *bind.FilterOpts) (*DomainERC721MintDomainIterator, error) {

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "MintDomain")
	if err != nil {
		return nil, err
	}
	return &DomainERC721MintDomainIterator{contract: _DomainERC721.contract, event: "MintDomain", logs: logs, sub: sub}, nil
}

// WatchMintDomain is a free log subscription operation binding the contract event 0x89402e274e8337c1f640b7d84153793cd3d97479756105a5635de5c4d8fd16fc.
//
// Solidity: event MintDomain(address curator, uint256 tokenId, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) WatchMintDomain(opts *bind.WatchOpts, sink chan<- *DomainERC721MintDomain) (event.Subscription, error) {

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "MintDomain")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721MintDomain)
				if err := _DomainERC721.contract.UnpackLog(event, "MintDomain", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMintDomain is a log parse operation binding the contract event 0x89402e274e8337c1f640b7d84153793cd3d97479756105a5635de5c4d8fd16fc.
//
// Solidity: event MintDomain(address curator, uint256 tokenId, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) ParseMintDomain(log types.Log) (*DomainERC721MintDomain, error) {
	event := new(DomainERC721MintDomain)
	if err := _DomainERC721.contract.UnpackLog(event, "MintDomain", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721OwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the DomainERC721 contract.
type DomainERC721OwnershipTransferredIterator struct {
	Event *DomainERC721OwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721OwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721OwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721OwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721OwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721OwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721OwnershipTransferred represents a OwnershipTransferred event raised by the DomainERC721 contract.
type DomainERC721OwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainERC721 *DomainERC721Filterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*DomainERC721OwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &DomainERC721OwnershipTransferredIterator{contract: _DomainERC721.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainERC721 *DomainERC721Filterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *DomainERC721OwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721OwnershipTransferred)
				if err := _DomainERC721.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainERC721 *DomainERC721Filterer) ParseOwnershipTransferred(log types.Log) (*DomainERC721OwnershipTransferred, error) {
	event := new(DomainERC721OwnershipTransferred)
	if err := _DomainERC721.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721PausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the DomainERC721 contract.
type DomainERC721PausedIterator struct {
	Event *DomainERC721Paused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721PausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721Paused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721Paused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721PausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721PausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721Paused represents a Paused event raised by the DomainERC721 contract.
type DomainERC721Paused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainERC721 *DomainERC721Filterer) FilterPaused(opts *bind.FilterOpts) (*DomainERC721PausedIterator, error) {

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &DomainERC721PausedIterator{contract: _DomainERC721.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainERC721 *DomainERC721Filterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *DomainERC721Paused) (event.Subscription, error) {

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721Paused)
				if err := _DomainERC721.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainERC721 *DomainERC721Filterer) ParsePaused(log types.Log) (*DomainERC721Paused, error) {
	event := new(DomainERC721Paused)
	if err := _DomainERC721.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721RemoveCuratorDomainIterator is returned from FilterRemoveCuratorDomain and is used to iterate over the raw logs and unpacked data for RemoveCuratorDomain events raised by the DomainERC721 contract.
type DomainERC721RemoveCuratorDomainIterator struct {
	Event *DomainERC721RemoveCuratorDomain // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721RemoveCuratorDomainIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721RemoveCuratorDomain)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721RemoveCuratorDomain)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721RemoveCuratorDomainIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721RemoveCuratorDomainIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721RemoveCuratorDomain represents a RemoveCuratorDomain event raised by the DomainERC721 contract.
type DomainERC721RemoveCuratorDomain struct {
	Curator common.Address
	Domain  [32]byte
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterRemoveCuratorDomain is a free log retrieval operation binding the contract event 0xa7b696b4724ea77364cc0f2b3e2f982897b62800f77cd80f08f858bc20daafe9.
//
// Solidity: event RemoveCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) FilterRemoveCuratorDomain(opts *bind.FilterOpts) (*DomainERC721RemoveCuratorDomainIterator, error) {

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "RemoveCuratorDomain")
	if err != nil {
		return nil, err
	}
	return &DomainERC721RemoveCuratorDomainIterator{contract: _DomainERC721.contract, event: "RemoveCuratorDomain", logs: logs, sub: sub}, nil
}

// WatchRemoveCuratorDomain is a free log subscription operation binding the contract event 0xa7b696b4724ea77364cc0f2b3e2f982897b62800f77cd80f08f858bc20daafe9.
//
// Solidity: event RemoveCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) WatchRemoveCuratorDomain(opts *bind.WatchOpts, sink chan<- *DomainERC721RemoveCuratorDomain) (event.Subscription, error) {

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "RemoveCuratorDomain")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721RemoveCuratorDomain)
				if err := _DomainERC721.contract.UnpackLog(event, "RemoveCuratorDomain", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRemoveCuratorDomain is a log parse operation binding the contract event 0xa7b696b4724ea77364cc0f2b3e2f982897b62800f77cd80f08f858bc20daafe9.
//
// Solidity: event RemoveCuratorDomain(address curator, bytes32 domain)
func (_DomainERC721 *DomainERC721Filterer) ParseRemoveCuratorDomain(log types.Log) (*DomainERC721RemoveCuratorDomain, error) {
	event := new(DomainERC721RemoveCuratorDomain)
	if err := _DomainERC721.contract.UnpackLog(event, "RemoveCuratorDomain", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the DomainERC721 contract.
type DomainERC721TransferIterator struct {
	Event *DomainERC721Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721Transfer represents a Transfer event raised by the DomainERC721 contract.
type DomainERC721Transfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address, tokenId []*big.Int) (*DomainERC721TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainERC721TransferIterator{contract: _DomainERC721.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *DomainERC721Transfer, from []common.Address, to []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721Transfer)
				if err := _DomainERC721.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_DomainERC721 *DomainERC721Filterer) ParseTransfer(log types.Log) (*DomainERC721Transfer, error) {
	event := new(DomainERC721Transfer)
	if err := _DomainERC721.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainERC721UnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the DomainERC721 contract.
type DomainERC721UnpausedIterator struct {
	Event *DomainERC721Unpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainERC721UnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainERC721Unpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainERC721Unpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainERC721UnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainERC721UnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainERC721Unpaused represents a Unpaused event raised by the DomainERC721 contract.
type DomainERC721Unpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainERC721 *DomainERC721Filterer) FilterUnpaused(opts *bind.FilterOpts) (*DomainERC721UnpausedIterator, error) {

	logs, sub, err := _DomainERC721.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &DomainERC721UnpausedIterator{contract: _DomainERC721.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainERC721 *DomainERC721Filterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *DomainERC721Unpaused) (event.Subscription, error) {

	logs, sub, err := _DomainERC721.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainERC721Unpaused)
				if err := _DomainERC721.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainERC721 *DomainERC721Filterer) ParseUnpaused(log types.Log) (*DomainERC721Unpaused, error) {
	event := new(DomainERC721Unpaused)
	if err := _DomainERC721.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
