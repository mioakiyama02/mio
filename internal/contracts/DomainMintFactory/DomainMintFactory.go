// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package DomainMintFactory

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// DomainMintFactoryABI is the input ABI used to generate the binding from.
const DomainMintFactoryABI = "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC721_\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"domainSettings_\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"wrapperNativeToken_\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"proxyAdmin_\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"domainVaultLogic_\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"erc721\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"erc721TokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"erc20\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"totalSupply\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"vault\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"reservePrice\",\"type\":\"uint256\"}],\"name\":\"MintVault\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"domainERC721\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainSettings\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainVaultLogic\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"proxyAdmin\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wrapperNativeToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"symbol\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"totalSupply\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"domainERC721TokenId_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"reservePrice_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"voteDuration_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"votePercentage_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"auctionDuration_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"mintWithPermit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"symbol\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"totalSupply\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"domainERC721TokenId_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"reservePrice_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"voteDuration_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"votePercentage_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"auctionDuration_\",\"type\":\"uint256\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC721_\",\"type\":\"address\"}],\"name\":\"setDomainERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainSettings_\",\"type\":\"address\"}],\"name\":\"setDomainSettings\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// DomainMintFactory is an auto generated Go binding around an Ethereum contract.
type DomainMintFactory struct {
	DomainMintFactoryCaller     // Read-only binding to the contract
	DomainMintFactoryTransactor // Write-only binding to the contract
	DomainMintFactoryFilterer   // Log filterer for contract events
}

// DomainMintFactoryCaller is an auto generated read-only Go binding around an Ethereum contract.
type DomainMintFactoryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainMintFactoryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type DomainMintFactoryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainMintFactoryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type DomainMintFactoryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainMintFactorySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type DomainMintFactorySession struct {
	Contract     *DomainMintFactory // Generic contract binding to set the session for
	CallOpts     bind.CallOpts      // Call options to use throughout this session
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// DomainMintFactoryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type DomainMintFactoryCallerSession struct {
	Contract *DomainMintFactoryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts            // Call options to use throughout this session
}

// DomainMintFactoryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type DomainMintFactoryTransactorSession struct {
	Contract     *DomainMintFactoryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts            // Transaction auth options to use throughout this session
}

// DomainMintFactoryRaw is an auto generated low-level Go binding around an Ethereum contract.
type DomainMintFactoryRaw struct {
	Contract *DomainMintFactory // Generic contract binding to access the raw methods on
}

// DomainMintFactoryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type DomainMintFactoryCallerRaw struct {
	Contract *DomainMintFactoryCaller // Generic read-only contract binding to access the raw methods on
}

// DomainMintFactoryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type DomainMintFactoryTransactorRaw struct {
	Contract *DomainMintFactoryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewDomainMintFactory creates a new instance of DomainMintFactory, bound to a specific deployed contract.
func NewDomainMintFactory(address common.Address, backend bind.ContractBackend) (*DomainMintFactory, error) {
	contract, err := bindDomainMintFactory(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactory{DomainMintFactoryCaller: DomainMintFactoryCaller{contract: contract}, DomainMintFactoryTransactor: DomainMintFactoryTransactor{contract: contract}, DomainMintFactoryFilterer: DomainMintFactoryFilterer{contract: contract}}, nil
}

// NewDomainMintFactoryCaller creates a new read-only instance of DomainMintFactory, bound to a specific deployed contract.
func NewDomainMintFactoryCaller(address common.Address, caller bind.ContractCaller) (*DomainMintFactoryCaller, error) {
	contract, err := bindDomainMintFactory(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryCaller{contract: contract}, nil
}

// NewDomainMintFactoryTransactor creates a new write-only instance of DomainMintFactory, bound to a specific deployed contract.
func NewDomainMintFactoryTransactor(address common.Address, transactor bind.ContractTransactor) (*DomainMintFactoryTransactor, error) {
	contract, err := bindDomainMintFactory(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryTransactor{contract: contract}, nil
}

// NewDomainMintFactoryFilterer creates a new log filterer instance of DomainMintFactory, bound to a specific deployed contract.
func NewDomainMintFactoryFilterer(address common.Address, filterer bind.ContractFilterer) (*DomainMintFactoryFilterer, error) {
	contract, err := bindDomainMintFactory(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryFilterer{contract: contract}, nil
}

// bindDomainMintFactory binds a generic wrapper to an already deployed contract.
func bindDomainMintFactory(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(DomainMintFactoryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainMintFactory *DomainMintFactoryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainMintFactory.Contract.DomainMintFactoryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainMintFactory *DomainMintFactoryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.DomainMintFactoryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainMintFactory *DomainMintFactoryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.DomainMintFactoryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainMintFactory *DomainMintFactoryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainMintFactory.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainMintFactory *DomainMintFactoryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainMintFactory *DomainMintFactoryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.contract.Transact(opts, method, params...)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) DomainERC721(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "domainERC721")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) DomainERC721() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainERC721(&_DomainMintFactory.CallOpts)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) DomainERC721() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainERC721(&_DomainMintFactory.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) DomainSettings(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "domainSettings")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) DomainSettings() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainSettings(&_DomainMintFactory.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) DomainSettings() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainSettings(&_DomainMintFactory.CallOpts)
}

// DomainVaultLogic is a free data retrieval call binding the contract method 0xac6ced66.
//
// Solidity: function domainVaultLogic() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) DomainVaultLogic(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "domainVaultLogic")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainVaultLogic is a free data retrieval call binding the contract method 0xac6ced66.
//
// Solidity: function domainVaultLogic() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) DomainVaultLogic() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainVaultLogic(&_DomainMintFactory.CallOpts)
}

// DomainVaultLogic is a free data retrieval call binding the contract method 0xac6ced66.
//
// Solidity: function domainVaultLogic() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) DomainVaultLogic() (common.Address, error) {
	return _DomainMintFactory.Contract.DomainVaultLogic(&_DomainMintFactory.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) Owner() (common.Address, error) {
	return _DomainMintFactory.Contract.Owner(&_DomainMintFactory.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) Owner() (common.Address, error) {
	return _DomainMintFactory.Contract.Owner(&_DomainMintFactory.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainMintFactory *DomainMintFactoryCaller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainMintFactory *DomainMintFactorySession) Paused() (bool, error) {
	return _DomainMintFactory.Contract.Paused(&_DomainMintFactory.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainMintFactory *DomainMintFactoryCallerSession) Paused() (bool, error) {
	return _DomainMintFactory.Contract.Paused(&_DomainMintFactory.CallOpts)
}

// ProxyAdmin is a free data retrieval call binding the contract method 0x3e47158c.
//
// Solidity: function proxyAdmin() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) ProxyAdmin(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "proxyAdmin")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// ProxyAdmin is a free data retrieval call binding the contract method 0x3e47158c.
//
// Solidity: function proxyAdmin() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) ProxyAdmin() (common.Address, error) {
	return _DomainMintFactory.Contract.ProxyAdmin(&_DomainMintFactory.CallOpts)
}

// ProxyAdmin is a free data retrieval call binding the contract method 0x3e47158c.
//
// Solidity: function proxyAdmin() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) ProxyAdmin() (common.Address, error) {
	return _DomainMintFactory.Contract.ProxyAdmin(&_DomainMintFactory.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCaller) WrapperNativeToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainMintFactory.contract.Call(opts, &out, "wrapperNativeToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainMintFactory *DomainMintFactorySession) WrapperNativeToken() (common.Address, error) {
	return _DomainMintFactory.Contract.WrapperNativeToken(&_DomainMintFactory.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainMintFactory *DomainMintFactoryCallerSession) WrapperNativeToken() (common.Address, error) {
	return _DomainMintFactory.Contract.WrapperNativeToken(&_DomainMintFactory.CallOpts)
}

// Mint is a paid mutator transaction binding the contract method 0x93073ea7.
//
// Solidity: function mint(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) Mint(opts *bind.TransactOpts, name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "mint", name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_)
}

// Mint is a paid mutator transaction binding the contract method 0x93073ea7.
//
// Solidity: function mint(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_) returns()
func (_DomainMintFactory *DomainMintFactorySession) Mint(name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Mint(&_DomainMintFactory.TransactOpts, name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_)
}

// Mint is a paid mutator transaction binding the contract method 0x93073ea7.
//
// Solidity: function mint(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) Mint(name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Mint(&_DomainMintFactory.TransactOpts, name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_)
}

// MintWithPermit is a paid mutator transaction binding the contract method 0x5cb93043.
//
// Solidity: function mintWithPermit(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) MintWithPermit(opts *bind.TransactOpts, name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "mintWithPermit", name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, deadline, v, r, s)
}

// MintWithPermit is a paid mutator transaction binding the contract method 0x5cb93043.
//
// Solidity: function mintWithPermit(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainMintFactory *DomainMintFactorySession) MintWithPermit(name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.MintWithPermit(&_DomainMintFactory.TransactOpts, name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, deadline, v, r, s)
}

// MintWithPermit is a paid mutator transaction binding the contract method 0x5cb93043.
//
// Solidity: function mintWithPermit(string name, string symbol, uint256 totalSupply, uint256 domainERC721TokenId_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) MintWithPermit(name string, symbol string, totalSupply *big.Int, domainERC721TokenId_ *big.Int, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.MintWithPermit(&_DomainMintFactory.TransactOpts, name, symbol, totalSupply, domainERC721TokenId_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, deadline, v, r, s)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainMintFactory *DomainMintFactorySession) Pause() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Pause(&_DomainMintFactory.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) Pause() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Pause(&_DomainMintFactory.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainMintFactory *DomainMintFactorySession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.RenounceOwnership(&_DomainMintFactory.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.RenounceOwnership(&_DomainMintFactory.TransactOpts)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) SetDomainERC721(opts *bind.TransactOpts, domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "setDomainERC721", domainERC721_)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainMintFactory *DomainMintFactorySession) SetDomainERC721(domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.SetDomainERC721(&_DomainMintFactory.TransactOpts, domainERC721_)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) SetDomainERC721(domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.SetDomainERC721(&_DomainMintFactory.TransactOpts, domainERC721_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) SetDomainSettings(opts *bind.TransactOpts, domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "setDomainSettings", domainSettings_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainMintFactory *DomainMintFactorySession) SetDomainSettings(domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.SetDomainSettings(&_DomainMintFactory.TransactOpts, domainSettings_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) SetDomainSettings(domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.SetDomainSettings(&_DomainMintFactory.TransactOpts, domainSettings_)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainMintFactory *DomainMintFactorySession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.TransferOwnership(&_DomainMintFactory.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainMintFactory.Contract.TransferOwnership(&_DomainMintFactory.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainMintFactory *DomainMintFactoryTransactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainMintFactory.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainMintFactory *DomainMintFactorySession) Unpause() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Unpause(&_DomainMintFactory.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainMintFactory *DomainMintFactoryTransactorSession) Unpause() (*types.Transaction, error) {
	return _DomainMintFactory.Contract.Unpause(&_DomainMintFactory.TransactOpts)
}

// DomainMintFactoryMintVaultIterator is returned from FilterMintVault and is used to iterate over the raw logs and unpacked data for MintVault events raised by the DomainMintFactory contract.
type DomainMintFactoryMintVaultIterator struct {
	Event *DomainMintFactoryMintVault // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainMintFactoryMintVaultIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainMintFactoryMintVault)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainMintFactoryMintVault)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainMintFactoryMintVaultIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainMintFactoryMintVaultIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainMintFactoryMintVault represents a MintVault event raised by the DomainMintFactory contract.
type DomainMintFactoryMintVault struct {
	Curator       common.Address
	Erc721        common.Address
	Erc721TokenId *big.Int
	Erc20         common.Address
	TotalSupply   *big.Int
	Vault         common.Address
	ReservePrice  *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterMintVault is a free log retrieval operation binding the contract event 0xc46fa595daef03706c42221c897473f005b95d758301cc74279b7954a9a1403e.
//
// Solidity: event MintVault(address indexed curator, address erc721, uint256 erc721TokenId, address erc20, uint256 totalSupply, address vault, uint256 reservePrice)
func (_DomainMintFactory *DomainMintFactoryFilterer) FilterMintVault(opts *bind.FilterOpts, curator []common.Address) (*DomainMintFactoryMintVaultIterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	logs, sub, err := _DomainMintFactory.contract.FilterLogs(opts, "MintVault", curatorRule)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryMintVaultIterator{contract: _DomainMintFactory.contract, event: "MintVault", logs: logs, sub: sub}, nil
}

// WatchMintVault is a free log subscription operation binding the contract event 0xc46fa595daef03706c42221c897473f005b95d758301cc74279b7954a9a1403e.
//
// Solidity: event MintVault(address indexed curator, address erc721, uint256 erc721TokenId, address erc20, uint256 totalSupply, address vault, uint256 reservePrice)
func (_DomainMintFactory *DomainMintFactoryFilterer) WatchMintVault(opts *bind.WatchOpts, sink chan<- *DomainMintFactoryMintVault, curator []common.Address) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	logs, sub, err := _DomainMintFactory.contract.WatchLogs(opts, "MintVault", curatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainMintFactoryMintVault)
				if err := _DomainMintFactory.contract.UnpackLog(event, "MintVault", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMintVault is a log parse operation binding the contract event 0xc46fa595daef03706c42221c897473f005b95d758301cc74279b7954a9a1403e.
//
// Solidity: event MintVault(address indexed curator, address erc721, uint256 erc721TokenId, address erc20, uint256 totalSupply, address vault, uint256 reservePrice)
func (_DomainMintFactory *DomainMintFactoryFilterer) ParseMintVault(log types.Log) (*DomainMintFactoryMintVault, error) {
	event := new(DomainMintFactoryMintVault)
	if err := _DomainMintFactory.contract.UnpackLog(event, "MintVault", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainMintFactoryOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the DomainMintFactory contract.
type DomainMintFactoryOwnershipTransferredIterator struct {
	Event *DomainMintFactoryOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainMintFactoryOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainMintFactoryOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainMintFactoryOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainMintFactoryOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainMintFactoryOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainMintFactoryOwnershipTransferred represents a OwnershipTransferred event raised by the DomainMintFactory contract.
type DomainMintFactoryOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainMintFactory *DomainMintFactoryFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*DomainMintFactoryOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainMintFactory.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryOwnershipTransferredIterator{contract: _DomainMintFactory.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainMintFactory *DomainMintFactoryFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *DomainMintFactoryOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainMintFactory.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainMintFactoryOwnershipTransferred)
				if err := _DomainMintFactory.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainMintFactory *DomainMintFactoryFilterer) ParseOwnershipTransferred(log types.Log) (*DomainMintFactoryOwnershipTransferred, error) {
	event := new(DomainMintFactoryOwnershipTransferred)
	if err := _DomainMintFactory.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainMintFactoryPausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the DomainMintFactory contract.
type DomainMintFactoryPausedIterator struct {
	Event *DomainMintFactoryPaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainMintFactoryPausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainMintFactoryPaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainMintFactoryPaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainMintFactoryPausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainMintFactoryPausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainMintFactoryPaused represents a Paused event raised by the DomainMintFactory contract.
type DomainMintFactoryPaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) FilterPaused(opts *bind.FilterOpts) (*DomainMintFactoryPausedIterator, error) {

	logs, sub, err := _DomainMintFactory.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryPausedIterator{contract: _DomainMintFactory.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *DomainMintFactoryPaused) (event.Subscription, error) {

	logs, sub, err := _DomainMintFactory.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainMintFactoryPaused)
				if err := _DomainMintFactory.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) ParsePaused(log types.Log) (*DomainMintFactoryPaused, error) {
	event := new(DomainMintFactoryPaused)
	if err := _DomainMintFactory.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainMintFactoryUnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the DomainMintFactory contract.
type DomainMintFactoryUnpausedIterator struct {
	Event *DomainMintFactoryUnpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainMintFactoryUnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainMintFactoryUnpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainMintFactoryUnpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainMintFactoryUnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainMintFactoryUnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainMintFactoryUnpaused represents a Unpaused event raised by the DomainMintFactory contract.
type DomainMintFactoryUnpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) FilterUnpaused(opts *bind.FilterOpts) (*DomainMintFactoryUnpausedIterator, error) {

	logs, sub, err := _DomainMintFactory.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &DomainMintFactoryUnpausedIterator{contract: _DomainMintFactory.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *DomainMintFactoryUnpaused) (event.Subscription, error) {

	logs, sub, err := _DomainMintFactory.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainMintFactoryUnpaused)
				if err := _DomainMintFactory.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainMintFactory *DomainMintFactoryFilterer) ParseUnpaused(log types.Log) (*DomainMintFactoryUnpaused, error) {
	event := new(DomainMintFactoryUnpaused)
	if err := _DomainMintFactory.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
