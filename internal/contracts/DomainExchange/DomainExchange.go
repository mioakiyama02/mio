// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package DomainExchange

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// DomainExchangeDomainERC20PreSaleInfo is an auto generated low-level Go binding around an user-defined struct.
type DomainExchangeDomainERC20PreSaleInfo struct {
	Curator        common.Address
	Amount         *big.Int
	OriginalAmount *big.Int
	Price          *big.Int
	StartTime      *big.Int
	EndTime        *big.Int
}

// DomainExchangeDomainERC721Offer is an auto generated low-level Go binding around an user-defined struct.
type DomainExchangeDomainERC721Offer struct {
	Bidder        common.Address
	Price         *big.Int
	OfferEndTime  *big.Int
	OfferDuration *big.Int
}

// DomainExchangeDomainERC721PriceInfo is an auto generated low-level Go binding around an user-defined struct.
type DomainExchangeDomainERC721PriceInfo struct {
	Erc721     common.Address
	TokenId    *big.Int
	Domain     [32]byte
	Curator    common.Address
	FixedPrice *big.Int
}

// DomainExchangeMetaData contains all meta data concerning the DomainExchange contract.
var DomainExchangeMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"FixedPriceBuyForERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"FixedPriceCancelForERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"FixedPriceOrderForERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"offerDuration\",\"type\":\"uint256\"}],\"name\":\"MakeOfferForERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"OfferToSellForERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"PreSaleBuyForERC20\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"}],\"name\":\"PreSaleOrderForERC20\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"PreSaleWithdrawForERC20\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"}],\"name\":\"WithdrawOfferFundForERC721\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"domainERC721\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"domainSettings\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wrapperNativeToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"stateMutability\":\"payable\",\"type\":\"receive\",\"payable\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_domainERC721\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_domainSettings\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_wrapperNativeToken\",\"type\":\"address\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"offerDuration\",\"type\":\"uint256\"}],\"name\":\"makeOfferForERC721\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"withdrawOfferFundForERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"offerToSellForERC721WithPermit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"offerToSellForERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIds\",\"type\":\"uint256[]\"},{\"internalType\":\"uint256\",\"name\":\"fixedPrice\",\"type\":\"uint256\"}],\"name\":\"batchFixedPriceOrderForERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"fixedPrice\",\"type\":\"uint256\"}],\"name\":\"fixedPriceOrderForERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"fixedPriceCancelForERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"fixedPriceCancelForDomainERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"fixedPriceBuyForERC721\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amountWithDecimals\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"preSaleOrderForERC20WithPermit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"}],\"name\":\"preSaleOrderForERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"preSaleBuyForERC20\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"}],\"name\":\"preSaleWithdrawForERC20\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC721_\",\"type\":\"address\"}],\"name\":\"setDomainERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainSettings_\",\"type\":\"address\"}],\"name\":\"setDomainSettings\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId_\",\"type\":\"uint256\"}],\"name\":\"getFixedPriceInfoForERC721\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"erc721\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"fixedPrice\",\"type\":\"uint256\"}],\"internalType\":\"structDomainExchange.DomainERC721PriceInfo\",\"name\":\"info\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256[]\",\"name\":\"tokenIds\",\"type\":\"uint256[]\"}],\"name\":\"getFixedPriceListForER721\",\"outputs\":[{\"internalType\":\"uint256[]\",\"name\":\"fixedPriceList\",\"type\":\"uint256[]\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"getDomainERC20ListForPreSale\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"domainERC20List\",\"type\":\"address[]\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20\",\"type\":\"address\"}],\"name\":\"getPreSaleInfoByDomainERC20\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"curator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"originalAmount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"startTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"endTime\",\"type\":\"uint256\"}],\"internalType\":\"structDomainExchange.DomainERC20PreSaleInfo\",\"name\":\"erc20PreSaleInfo\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId_\",\"type\":\"uint256\"}],\"name\":\"getOfferForERC721ByTokenId\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"price\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"offerEndTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"offerDuration\",\"type\":\"uint256\"}],\"internalType\":\"structDomainExchange.DomainERC721Offer\",\"name\":\"offer\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true}]",
}

// DomainExchangeABI is the input ABI used to generate the binding from.
// Deprecated: Use DomainExchangeMetaData.ABI instead.
var DomainExchangeABI = DomainExchangeMetaData.ABI

// DomainExchange is an auto generated Go binding around an Ethereum contract.
type DomainExchange struct {
	DomainExchangeCaller     // Read-only binding to the contract
	DomainExchangeTransactor // Write-only binding to the contract
	DomainExchangeFilterer   // Log filterer for contract events
}

// DomainExchangeCaller is an auto generated read-only Go binding around an Ethereum contract.
type DomainExchangeCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainExchangeTransactor is an auto generated write-only Go binding around an Ethereum contract.
type DomainExchangeTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainExchangeFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type DomainExchangeFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainExchangeSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type DomainExchangeSession struct {
	Contract     *DomainExchange   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// DomainExchangeCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type DomainExchangeCallerSession struct {
	Contract *DomainExchangeCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// DomainExchangeTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type DomainExchangeTransactorSession struct {
	Contract     *DomainExchangeTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// DomainExchangeRaw is an auto generated low-level Go binding around an Ethereum contract.
type DomainExchangeRaw struct {
	Contract *DomainExchange // Generic contract binding to access the raw methods on
}

// DomainExchangeCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type DomainExchangeCallerRaw struct {
	Contract *DomainExchangeCaller // Generic read-only contract binding to access the raw methods on
}

// DomainExchangeTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type DomainExchangeTransactorRaw struct {
	Contract *DomainExchangeTransactor // Generic write-only contract binding to access the raw methods on
}

// NewDomainExchange creates a new instance of DomainExchange, bound to a specific deployed contract.
func NewDomainExchange(address common.Address, backend bind.ContractBackend) (*DomainExchange, error) {
	contract, err := bindDomainExchange(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &DomainExchange{DomainExchangeCaller: DomainExchangeCaller{contract: contract}, DomainExchangeTransactor: DomainExchangeTransactor{contract: contract}, DomainExchangeFilterer: DomainExchangeFilterer{contract: contract}}, nil
}

// NewDomainExchangeCaller creates a new read-only instance of DomainExchange, bound to a specific deployed contract.
func NewDomainExchangeCaller(address common.Address, caller bind.ContractCaller) (*DomainExchangeCaller, error) {
	contract, err := bindDomainExchange(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeCaller{contract: contract}, nil
}

// NewDomainExchangeTransactor creates a new write-only instance of DomainExchange, bound to a specific deployed contract.
func NewDomainExchangeTransactor(address common.Address, transactor bind.ContractTransactor) (*DomainExchangeTransactor, error) {
	contract, err := bindDomainExchange(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeTransactor{contract: contract}, nil
}

// NewDomainExchangeFilterer creates a new log filterer instance of DomainExchange, bound to a specific deployed contract.
func NewDomainExchangeFilterer(address common.Address, filterer bind.ContractFilterer) (*DomainExchangeFilterer, error) {
	contract, err := bindDomainExchange(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeFilterer{contract: contract}, nil
}

// bindDomainExchange binds a generic wrapper to an already deployed contract.
func bindDomainExchange(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(DomainExchangeABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainExchange *DomainExchangeRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainExchange.Contract.DomainExchangeCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainExchange *DomainExchangeRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.Contract.DomainExchangeTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainExchange *DomainExchangeRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainExchange.Contract.DomainExchangeTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainExchange *DomainExchangeCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainExchange.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainExchange *DomainExchangeTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainExchange *DomainExchangeTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainExchange.Contract.contract.Transact(opts, method, params...)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainExchange *DomainExchangeCaller) DomainERC721(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "domainERC721")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainExchange *DomainExchangeSession) DomainERC721() (common.Address, error) {
	return _DomainExchange.Contract.DomainERC721(&_DomainExchange.CallOpts)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainExchange *DomainExchangeCallerSession) DomainERC721() (common.Address, error) {
	return _DomainExchange.Contract.DomainERC721(&_DomainExchange.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainExchange *DomainExchangeCaller) DomainSettings(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "domainSettings")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainExchange *DomainExchangeSession) DomainSettings() (common.Address, error) {
	return _DomainExchange.Contract.DomainSettings(&_DomainExchange.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainExchange *DomainExchangeCallerSession) DomainSettings() (common.Address, error) {
	return _DomainExchange.Contract.DomainSettings(&_DomainExchange.CallOpts)
}

// GetDomainERC20ListForPreSale is a free data retrieval call binding the contract method 0x45c4319f.
//
// Solidity: function getDomainERC20ListForPreSale() view returns(address[] domainERC20List)
func (_DomainExchange *DomainExchangeCaller) GetDomainERC20ListForPreSale(opts *bind.CallOpts) ([]common.Address, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "getDomainERC20ListForPreSale")

	if err != nil {
		return *new([]common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)

	return out0, err

}

// GetDomainERC20ListForPreSale is a free data retrieval call binding the contract method 0x45c4319f.
//
// Solidity: function getDomainERC20ListForPreSale() view returns(address[] domainERC20List)
func (_DomainExchange *DomainExchangeSession) GetDomainERC20ListForPreSale() ([]common.Address, error) {
	return _DomainExchange.Contract.GetDomainERC20ListForPreSale(&_DomainExchange.CallOpts)
}

// GetDomainERC20ListForPreSale is a free data retrieval call binding the contract method 0x45c4319f.
//
// Solidity: function getDomainERC20ListForPreSale() view returns(address[] domainERC20List)
func (_DomainExchange *DomainExchangeCallerSession) GetDomainERC20ListForPreSale() ([]common.Address, error) {
	return _DomainExchange.Contract.GetDomainERC20ListForPreSale(&_DomainExchange.CallOpts)
}

// GetFixedPriceInfoForERC721 is a free data retrieval call binding the contract method 0xf8fcb5cd.
//
// Solidity: function getFixedPriceInfoForERC721(uint256 tokenId_) view returns((address,uint256,bytes32,address,uint256) info)
func (_DomainExchange *DomainExchangeCaller) GetFixedPriceInfoForERC721(opts *bind.CallOpts, tokenId_ *big.Int) (DomainExchangeDomainERC721PriceInfo, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "getFixedPriceInfoForERC721", tokenId_)

	if err != nil {
		return *new(DomainExchangeDomainERC721PriceInfo), err
	}

	out0 := *abi.ConvertType(out[0], new(DomainExchangeDomainERC721PriceInfo)).(*DomainExchangeDomainERC721PriceInfo)

	return out0, err

}

// GetFixedPriceInfoForERC721 is a free data retrieval call binding the contract method 0xf8fcb5cd.
//
// Solidity: function getFixedPriceInfoForERC721(uint256 tokenId_) view returns((address,uint256,bytes32,address,uint256) info)
func (_DomainExchange *DomainExchangeSession) GetFixedPriceInfoForERC721(tokenId_ *big.Int) (DomainExchangeDomainERC721PriceInfo, error) {
	return _DomainExchange.Contract.GetFixedPriceInfoForERC721(&_DomainExchange.CallOpts, tokenId_)
}

// GetFixedPriceInfoForERC721 is a free data retrieval call binding the contract method 0xf8fcb5cd.
//
// Solidity: function getFixedPriceInfoForERC721(uint256 tokenId_) view returns((address,uint256,bytes32,address,uint256) info)
func (_DomainExchange *DomainExchangeCallerSession) GetFixedPriceInfoForERC721(tokenId_ *big.Int) (DomainExchangeDomainERC721PriceInfo, error) {
	return _DomainExchange.Contract.GetFixedPriceInfoForERC721(&_DomainExchange.CallOpts, tokenId_)
}

// GetFixedPriceListForER721 is a free data retrieval call binding the contract method 0xcac31779.
//
// Solidity: function getFixedPriceListForER721(uint256[] tokenIds) view returns(uint256[] fixedPriceList)
func (_DomainExchange *DomainExchangeCaller) GetFixedPriceListForER721(opts *bind.CallOpts, tokenIds []*big.Int) ([]*big.Int, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "getFixedPriceListForER721", tokenIds)

	if err != nil {
		return *new([]*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([]*big.Int)).(*[]*big.Int)

	return out0, err

}

// GetFixedPriceListForER721 is a free data retrieval call binding the contract method 0xcac31779.
//
// Solidity: function getFixedPriceListForER721(uint256[] tokenIds) view returns(uint256[] fixedPriceList)
func (_DomainExchange *DomainExchangeSession) GetFixedPriceListForER721(tokenIds []*big.Int) ([]*big.Int, error) {
	return _DomainExchange.Contract.GetFixedPriceListForER721(&_DomainExchange.CallOpts, tokenIds)
}

// GetFixedPriceListForER721 is a free data retrieval call binding the contract method 0xcac31779.
//
// Solidity: function getFixedPriceListForER721(uint256[] tokenIds) view returns(uint256[] fixedPriceList)
func (_DomainExchange *DomainExchangeCallerSession) GetFixedPriceListForER721(tokenIds []*big.Int) ([]*big.Int, error) {
	return _DomainExchange.Contract.GetFixedPriceListForER721(&_DomainExchange.CallOpts, tokenIds)
}

// GetOfferForERC721ByTokenId is a free data retrieval call binding the contract method 0x474452a0.
//
// Solidity: function getOfferForERC721ByTokenId(uint256 tokenId_) view returns((address,uint256,uint256,uint256) offer)
func (_DomainExchange *DomainExchangeCaller) GetOfferForERC721ByTokenId(opts *bind.CallOpts, tokenId_ *big.Int) (DomainExchangeDomainERC721Offer, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "getOfferForERC721ByTokenId", tokenId_)

	if err != nil {
		return *new(DomainExchangeDomainERC721Offer), err
	}

	out0 := *abi.ConvertType(out[0], new(DomainExchangeDomainERC721Offer)).(*DomainExchangeDomainERC721Offer)

	return out0, err

}

// GetOfferForERC721ByTokenId is a free data retrieval call binding the contract method 0x474452a0.
//
// Solidity: function getOfferForERC721ByTokenId(uint256 tokenId_) view returns((address,uint256,uint256,uint256) offer)
func (_DomainExchange *DomainExchangeSession) GetOfferForERC721ByTokenId(tokenId_ *big.Int) (DomainExchangeDomainERC721Offer, error) {
	return _DomainExchange.Contract.GetOfferForERC721ByTokenId(&_DomainExchange.CallOpts, tokenId_)
}

// GetOfferForERC721ByTokenId is a free data retrieval call binding the contract method 0x474452a0.
//
// Solidity: function getOfferForERC721ByTokenId(uint256 tokenId_) view returns((address,uint256,uint256,uint256) offer)
func (_DomainExchange *DomainExchangeCallerSession) GetOfferForERC721ByTokenId(tokenId_ *big.Int) (DomainExchangeDomainERC721Offer, error) {
	return _DomainExchange.Contract.GetOfferForERC721ByTokenId(&_DomainExchange.CallOpts, tokenId_)
}

// GetPreSaleInfoByDomainERC20 is a free data retrieval call binding the contract method 0x8fc6e4a7.
//
// Solidity: function getPreSaleInfoByDomainERC20(address domainERC20) view returns((address,uint256,uint256,uint256,uint256,uint256) erc20PreSaleInfo)
func (_DomainExchange *DomainExchangeCaller) GetPreSaleInfoByDomainERC20(opts *bind.CallOpts, domainERC20 common.Address) (DomainExchangeDomainERC20PreSaleInfo, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "getPreSaleInfoByDomainERC20", domainERC20)

	if err != nil {
		return *new(DomainExchangeDomainERC20PreSaleInfo), err
	}

	out0 := *abi.ConvertType(out[0], new(DomainExchangeDomainERC20PreSaleInfo)).(*DomainExchangeDomainERC20PreSaleInfo)

	return out0, err

}

// GetPreSaleInfoByDomainERC20 is a free data retrieval call binding the contract method 0x8fc6e4a7.
//
// Solidity: function getPreSaleInfoByDomainERC20(address domainERC20) view returns((address,uint256,uint256,uint256,uint256,uint256) erc20PreSaleInfo)
func (_DomainExchange *DomainExchangeSession) GetPreSaleInfoByDomainERC20(domainERC20 common.Address) (DomainExchangeDomainERC20PreSaleInfo, error) {
	return _DomainExchange.Contract.GetPreSaleInfoByDomainERC20(&_DomainExchange.CallOpts, domainERC20)
}

// GetPreSaleInfoByDomainERC20 is a free data retrieval call binding the contract method 0x8fc6e4a7.
//
// Solidity: function getPreSaleInfoByDomainERC20(address domainERC20) view returns((address,uint256,uint256,uint256,uint256,uint256) erc20PreSaleInfo)
func (_DomainExchange *DomainExchangeCallerSession) GetPreSaleInfoByDomainERC20(domainERC20 common.Address) (DomainExchangeDomainERC20PreSaleInfo, error) {
	return _DomainExchange.Contract.GetPreSaleInfoByDomainERC20(&_DomainExchange.CallOpts, domainERC20)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainExchange *DomainExchangeCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainExchange *DomainExchangeSession) Owner() (common.Address, error) {
	return _DomainExchange.Contract.Owner(&_DomainExchange.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainExchange *DomainExchangeCallerSession) Owner() (common.Address, error) {
	return _DomainExchange.Contract.Owner(&_DomainExchange.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainExchange *DomainExchangeCaller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainExchange *DomainExchangeSession) Paused() (bool, error) {
	return _DomainExchange.Contract.Paused(&_DomainExchange.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainExchange *DomainExchangeCallerSession) Paused() (bool, error) {
	return _DomainExchange.Contract.Paused(&_DomainExchange.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainExchange *DomainExchangeCaller) WrapperNativeToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainExchange.contract.Call(opts, &out, "wrapperNativeToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainExchange *DomainExchangeSession) WrapperNativeToken() (common.Address, error) {
	return _DomainExchange.Contract.WrapperNativeToken(&_DomainExchange.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainExchange *DomainExchangeCallerSession) WrapperNativeToken() (common.Address, error) {
	return _DomainExchange.Contract.WrapperNativeToken(&_DomainExchange.CallOpts)
}

// BatchFixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x39778776.
//
// Solidity: function batchFixedPriceOrderForERC721(uint256[] tokenIds, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeTransactor) BatchFixedPriceOrderForERC721(opts *bind.TransactOpts, tokenIds []*big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "batchFixedPriceOrderForERC721", tokenIds, fixedPrice)
}

// BatchFixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x39778776.
//
// Solidity: function batchFixedPriceOrderForERC721(uint256[] tokenIds, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeSession) BatchFixedPriceOrderForERC721(tokenIds []*big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.BatchFixedPriceOrderForERC721(&_DomainExchange.TransactOpts, tokenIds, fixedPrice)
}

// BatchFixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x39778776.
//
// Solidity: function batchFixedPriceOrderForERC721(uint256[] tokenIds, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeTransactorSession) BatchFixedPriceOrderForERC721(tokenIds []*big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.BatchFixedPriceOrderForERC721(&_DomainExchange.TransactOpts, tokenIds, fixedPrice)
}

// FixedPriceBuyForERC721 is a paid mutator transaction binding the contract method 0xbc06b967.
//
// Solidity: function fixedPriceBuyForERC721(uint256 tokenId) payable returns()
func (_DomainExchange *DomainExchangeTransactor) FixedPriceBuyForERC721(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "fixedPriceBuyForERC721", tokenId)
}

// FixedPriceBuyForERC721 is a paid mutator transaction binding the contract method 0xbc06b967.
//
// Solidity: function fixedPriceBuyForERC721(uint256 tokenId) payable returns()
func (_DomainExchange *DomainExchangeSession) FixedPriceBuyForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceBuyForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceBuyForERC721 is a paid mutator transaction binding the contract method 0xbc06b967.
//
// Solidity: function fixedPriceBuyForERC721(uint256 tokenId) payable returns()
func (_DomainExchange *DomainExchangeTransactorSession) FixedPriceBuyForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceBuyForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceCancelForDomainERC721 is a paid mutator transaction binding the contract method 0x68e2574a.
//
// Solidity: function fixedPriceCancelForDomainERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactor) FixedPriceCancelForDomainERC721(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "fixedPriceCancelForDomainERC721", tokenId)
}

// FixedPriceCancelForDomainERC721 is a paid mutator transaction binding the contract method 0x68e2574a.
//
// Solidity: function fixedPriceCancelForDomainERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeSession) FixedPriceCancelForDomainERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceCancelForDomainERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceCancelForDomainERC721 is a paid mutator transaction binding the contract method 0x68e2574a.
//
// Solidity: function fixedPriceCancelForDomainERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactorSession) FixedPriceCancelForDomainERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceCancelForDomainERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceCancelForERC721 is a paid mutator transaction binding the contract method 0x1d123312.
//
// Solidity: function fixedPriceCancelForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactor) FixedPriceCancelForERC721(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "fixedPriceCancelForERC721", tokenId)
}

// FixedPriceCancelForERC721 is a paid mutator transaction binding the contract method 0x1d123312.
//
// Solidity: function fixedPriceCancelForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeSession) FixedPriceCancelForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceCancelForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceCancelForERC721 is a paid mutator transaction binding the contract method 0x1d123312.
//
// Solidity: function fixedPriceCancelForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactorSession) FixedPriceCancelForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceCancelForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// FixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x7315c1d3.
//
// Solidity: function fixedPriceOrderForERC721(uint256 tokenId, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeTransactor) FixedPriceOrderForERC721(opts *bind.TransactOpts, tokenId *big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "fixedPriceOrderForERC721", tokenId, fixedPrice)
}

// FixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x7315c1d3.
//
// Solidity: function fixedPriceOrderForERC721(uint256 tokenId, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeSession) FixedPriceOrderForERC721(tokenId *big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceOrderForERC721(&_DomainExchange.TransactOpts, tokenId, fixedPrice)
}

// FixedPriceOrderForERC721 is a paid mutator transaction binding the contract method 0x7315c1d3.
//
// Solidity: function fixedPriceOrderForERC721(uint256 tokenId, uint256 fixedPrice) returns()
func (_DomainExchange *DomainExchangeTransactorSession) FixedPriceOrderForERC721(tokenId *big.Int, fixedPrice *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.FixedPriceOrderForERC721(&_DomainExchange.TransactOpts, tokenId, fixedPrice)
}

// Initialize is a paid mutator transaction binding the contract method 0xc0c53b8b.
//
// Solidity: function initialize(address _domainERC721, address _domainSettings, address _wrapperNativeToken) returns()
func (_DomainExchange *DomainExchangeTransactor) Initialize(opts *bind.TransactOpts, _domainERC721 common.Address, _domainSettings common.Address, _wrapperNativeToken common.Address) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "initialize", _domainERC721, _domainSettings, _wrapperNativeToken)
}

// Initialize is a paid mutator transaction binding the contract method 0xc0c53b8b.
//
// Solidity: function initialize(address _domainERC721, address _domainSettings, address _wrapperNativeToken) returns()
func (_DomainExchange *DomainExchangeSession) Initialize(_domainERC721 common.Address, _domainSettings common.Address, _wrapperNativeToken common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.Initialize(&_DomainExchange.TransactOpts, _domainERC721, _domainSettings, _wrapperNativeToken)
}

// Initialize is a paid mutator transaction binding the contract method 0xc0c53b8b.
//
// Solidity: function initialize(address _domainERC721, address _domainSettings, address _wrapperNativeToken) returns()
func (_DomainExchange *DomainExchangeTransactorSession) Initialize(_domainERC721 common.Address, _domainSettings common.Address, _wrapperNativeToken common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.Initialize(&_DomainExchange.TransactOpts, _domainERC721, _domainSettings, _wrapperNativeToken)
}

// MakeOfferForERC721 is a paid mutator transaction binding the contract method 0xe31c18a1.
//
// Solidity: function makeOfferForERC721(uint256 tokenId, uint256 offerDuration) payable returns()
func (_DomainExchange *DomainExchangeTransactor) MakeOfferForERC721(opts *bind.TransactOpts, tokenId *big.Int, offerDuration *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "makeOfferForERC721", tokenId, offerDuration)
}

// MakeOfferForERC721 is a paid mutator transaction binding the contract method 0xe31c18a1.
//
// Solidity: function makeOfferForERC721(uint256 tokenId, uint256 offerDuration) payable returns()
func (_DomainExchange *DomainExchangeSession) MakeOfferForERC721(tokenId *big.Int, offerDuration *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.MakeOfferForERC721(&_DomainExchange.TransactOpts, tokenId, offerDuration)
}

// MakeOfferForERC721 is a paid mutator transaction binding the contract method 0xe31c18a1.
//
// Solidity: function makeOfferForERC721(uint256 tokenId, uint256 offerDuration) payable returns()
func (_DomainExchange *DomainExchangeTransactorSession) MakeOfferForERC721(tokenId *big.Int, offerDuration *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.MakeOfferForERC721(&_DomainExchange.TransactOpts, tokenId, offerDuration)
}

// OfferToSellForERC721 is a paid mutator transaction binding the contract method 0x8b3dee8f.
//
// Solidity: function offerToSellForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactor) OfferToSellForERC721(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "offerToSellForERC721", tokenId)
}

// OfferToSellForERC721 is a paid mutator transaction binding the contract method 0x8b3dee8f.
//
// Solidity: function offerToSellForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeSession) OfferToSellForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.OfferToSellForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// OfferToSellForERC721 is a paid mutator transaction binding the contract method 0x8b3dee8f.
//
// Solidity: function offerToSellForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactorSession) OfferToSellForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.OfferToSellForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// OfferToSellForERC721WithPermit is a paid mutator transaction binding the contract method 0x1967df85.
//
// Solidity: function offerToSellForERC721WithPermit(uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeTransactor) OfferToSellForERC721WithPermit(opts *bind.TransactOpts, tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "offerToSellForERC721WithPermit", tokenId, deadline, v, r, s)
}

// OfferToSellForERC721WithPermit is a paid mutator transaction binding the contract method 0x1967df85.
//
// Solidity: function offerToSellForERC721WithPermit(uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeSession) OfferToSellForERC721WithPermit(tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.OfferToSellForERC721WithPermit(&_DomainExchange.TransactOpts, tokenId, deadline, v, r, s)
}

// OfferToSellForERC721WithPermit is a paid mutator transaction binding the contract method 0x1967df85.
//
// Solidity: function offerToSellForERC721WithPermit(uint256 tokenId, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeTransactorSession) OfferToSellForERC721WithPermit(tokenId *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.OfferToSellForERC721WithPermit(&_DomainExchange.TransactOpts, tokenId, deadline, v, r, s)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainExchange *DomainExchangeTransactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainExchange *DomainExchangeSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.OnERC721Received(&_DomainExchange.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainExchange *DomainExchangeTransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.OnERC721Received(&_DomainExchange.TransactOpts, arg0, arg1, arg2, arg3)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainExchange *DomainExchangeTransactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainExchange *DomainExchangeSession) Pause() (*types.Transaction, error) {
	return _DomainExchange.Contract.Pause(&_DomainExchange.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainExchange *DomainExchangeTransactorSession) Pause() (*types.Transaction, error) {
	return _DomainExchange.Contract.Pause(&_DomainExchange.TransactOpts)
}

// PreSaleBuyForERC20 is a paid mutator transaction binding the contract method 0x6e5da5f9.
//
// Solidity: function preSaleBuyForERC20(address domainERC20, uint256 amount) payable returns()
func (_DomainExchange *DomainExchangeTransactor) PreSaleBuyForERC20(opts *bind.TransactOpts, domainERC20 common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "preSaleBuyForERC20", domainERC20, amount)
}

// PreSaleBuyForERC20 is a paid mutator transaction binding the contract method 0x6e5da5f9.
//
// Solidity: function preSaleBuyForERC20(address domainERC20, uint256 amount) payable returns()
func (_DomainExchange *DomainExchangeSession) PreSaleBuyForERC20(domainERC20 common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleBuyForERC20(&_DomainExchange.TransactOpts, domainERC20, amount)
}

// PreSaleBuyForERC20 is a paid mutator transaction binding the contract method 0x6e5da5f9.
//
// Solidity: function preSaleBuyForERC20(address domainERC20, uint256 amount) payable returns()
func (_DomainExchange *DomainExchangeTransactorSession) PreSaleBuyForERC20(domainERC20 common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleBuyForERC20(&_DomainExchange.TransactOpts, domainERC20, amount)
}

// PreSaleOrderForERC20 is a paid mutator transaction binding the contract method 0x9b463467.
//
// Solidity: function preSaleOrderForERC20(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime) returns()
func (_DomainExchange *DomainExchangeTransactor) PreSaleOrderForERC20(opts *bind.TransactOpts, domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "preSaleOrderForERC20", domainERC20, amount, price, startTime, endTime)
}

// PreSaleOrderForERC20 is a paid mutator transaction binding the contract method 0x9b463467.
//
// Solidity: function preSaleOrderForERC20(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime) returns()
func (_DomainExchange *DomainExchangeSession) PreSaleOrderForERC20(domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleOrderForERC20(&_DomainExchange.TransactOpts, domainERC20, amount, price, startTime, endTime)
}

// PreSaleOrderForERC20 is a paid mutator transaction binding the contract method 0x9b463467.
//
// Solidity: function preSaleOrderForERC20(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime) returns()
func (_DomainExchange *DomainExchangeTransactorSession) PreSaleOrderForERC20(domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleOrderForERC20(&_DomainExchange.TransactOpts, domainERC20, amount, price, startTime, endTime)
}

// PreSaleOrderForERC20WithPermit is a paid mutator transaction binding the contract method 0xda89f25c.
//
// Solidity: function preSaleOrderForERC20WithPermit(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime, uint256 amountWithDecimals, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeTransactor) PreSaleOrderForERC20WithPermit(opts *bind.TransactOpts, domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int, amountWithDecimals *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "preSaleOrderForERC20WithPermit", domainERC20, amount, price, startTime, endTime, amountWithDecimals, deadline, v, r, s)
}

// PreSaleOrderForERC20WithPermit is a paid mutator transaction binding the contract method 0xda89f25c.
//
// Solidity: function preSaleOrderForERC20WithPermit(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime, uint256 amountWithDecimals, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeSession) PreSaleOrderForERC20WithPermit(domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int, amountWithDecimals *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleOrderForERC20WithPermit(&_DomainExchange.TransactOpts, domainERC20, amount, price, startTime, endTime, amountWithDecimals, deadline, v, r, s)
}

// PreSaleOrderForERC20WithPermit is a paid mutator transaction binding the contract method 0xda89f25c.
//
// Solidity: function preSaleOrderForERC20WithPermit(address domainERC20, uint256 amount, uint256 price, uint256 startTime, uint256 endTime, uint256 amountWithDecimals, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainExchange *DomainExchangeTransactorSession) PreSaleOrderForERC20WithPermit(domainERC20 common.Address, amount *big.Int, price *big.Int, startTime *big.Int, endTime *big.Int, amountWithDecimals *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleOrderForERC20WithPermit(&_DomainExchange.TransactOpts, domainERC20, amount, price, startTime, endTime, amountWithDecimals, deadline, v, r, s)
}

// PreSaleWithdrawForERC20 is a paid mutator transaction binding the contract method 0x4d172b6a.
//
// Solidity: function preSaleWithdrawForERC20(address domainERC20) payable returns()
func (_DomainExchange *DomainExchangeTransactor) PreSaleWithdrawForERC20(opts *bind.TransactOpts, domainERC20 common.Address) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "preSaleWithdrawForERC20", domainERC20)
}

// PreSaleWithdrawForERC20 is a paid mutator transaction binding the contract method 0x4d172b6a.
//
// Solidity: function preSaleWithdrawForERC20(address domainERC20) payable returns()
func (_DomainExchange *DomainExchangeSession) PreSaleWithdrawForERC20(domainERC20 common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleWithdrawForERC20(&_DomainExchange.TransactOpts, domainERC20)
}

// PreSaleWithdrawForERC20 is a paid mutator transaction binding the contract method 0x4d172b6a.
//
// Solidity: function preSaleWithdrawForERC20(address domainERC20) payable returns()
func (_DomainExchange *DomainExchangeTransactorSession) PreSaleWithdrawForERC20(domainERC20 common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.PreSaleWithdrawForERC20(&_DomainExchange.TransactOpts, domainERC20)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainExchange *DomainExchangeTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainExchange *DomainExchangeSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainExchange.Contract.RenounceOwnership(&_DomainExchange.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainExchange *DomainExchangeTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainExchange.Contract.RenounceOwnership(&_DomainExchange.TransactOpts)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainExchange *DomainExchangeTransactor) SetDomainERC721(opts *bind.TransactOpts, domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "setDomainERC721", domainERC721_)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainExchange *DomainExchangeSession) SetDomainERC721(domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.SetDomainERC721(&_DomainExchange.TransactOpts, domainERC721_)
}

// SetDomainERC721 is a paid mutator transaction binding the contract method 0xe85a0c1e.
//
// Solidity: function setDomainERC721(address domainERC721_) returns()
func (_DomainExchange *DomainExchangeTransactorSession) SetDomainERC721(domainERC721_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.SetDomainERC721(&_DomainExchange.TransactOpts, domainERC721_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainExchange *DomainExchangeTransactor) SetDomainSettings(opts *bind.TransactOpts, domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "setDomainSettings", domainSettings_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainExchange *DomainExchangeSession) SetDomainSettings(domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.SetDomainSettings(&_DomainExchange.TransactOpts, domainSettings_)
}

// SetDomainSettings is a paid mutator transaction binding the contract method 0xfa419aaa.
//
// Solidity: function setDomainSettings(address domainSettings_) returns()
func (_DomainExchange *DomainExchangeTransactorSession) SetDomainSettings(domainSettings_ common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.SetDomainSettings(&_DomainExchange.TransactOpts, domainSettings_)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainExchange *DomainExchangeTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainExchange *DomainExchangeSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.TransferOwnership(&_DomainExchange.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainExchange *DomainExchangeTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainExchange.Contract.TransferOwnership(&_DomainExchange.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainExchange *DomainExchangeTransactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainExchange *DomainExchangeSession) Unpause() (*types.Transaction, error) {
	return _DomainExchange.Contract.Unpause(&_DomainExchange.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainExchange *DomainExchangeTransactorSession) Unpause() (*types.Transaction, error) {
	return _DomainExchange.Contract.Unpause(&_DomainExchange.TransactOpts)
}

// WithdrawOfferFundForERC721 is a paid mutator transaction binding the contract method 0x7e43e8d8.
//
// Solidity: function withdrawOfferFundForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactor) WithdrawOfferFundForERC721(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.contract.Transact(opts, "withdrawOfferFundForERC721", tokenId)
}

// WithdrawOfferFundForERC721 is a paid mutator transaction binding the contract method 0x7e43e8d8.
//
// Solidity: function withdrawOfferFundForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeSession) WithdrawOfferFundForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.WithdrawOfferFundForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// WithdrawOfferFundForERC721 is a paid mutator transaction binding the contract method 0x7e43e8d8.
//
// Solidity: function withdrawOfferFundForERC721(uint256 tokenId) returns()
func (_DomainExchange *DomainExchangeTransactorSession) WithdrawOfferFundForERC721(tokenId *big.Int) (*types.Transaction, error) {
	return _DomainExchange.Contract.WithdrawOfferFundForERC721(&_DomainExchange.TransactOpts, tokenId)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainExchange *DomainExchangeTransactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainExchange.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainExchange *DomainExchangeSession) Receive() (*types.Transaction, error) {
	return _DomainExchange.Contract.Receive(&_DomainExchange.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainExchange *DomainExchangeTransactorSession) Receive() (*types.Transaction, error) {
	return _DomainExchange.Contract.Receive(&_DomainExchange.TransactOpts)
}

// DomainExchangeFixedPriceBuyForERC721Iterator is returned from FilterFixedPriceBuyForERC721 and is used to iterate over the raw logs and unpacked data for FixedPriceBuyForERC721 events raised by the DomainExchange contract.
type DomainExchangeFixedPriceBuyForERC721Iterator struct {
	Event *DomainExchangeFixedPriceBuyForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeFixedPriceBuyForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeFixedPriceBuyForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeFixedPriceBuyForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeFixedPriceBuyForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeFixedPriceBuyForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeFixedPriceBuyForERC721 represents a FixedPriceBuyForERC721 event raised by the DomainExchange contract.
type DomainExchangeFixedPriceBuyForERC721 struct {
	Buyer   common.Address
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterFixedPriceBuyForERC721 is a free log retrieval operation binding the contract event 0x136f4960bea84bfd52ef1a10488a32720bb02d45794328fe6798144ad2d0289c.
//
// Solidity: event FixedPriceBuyForERC721(address indexed buyer, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) FilterFixedPriceBuyForERC721(opts *bind.FilterOpts, buyer []common.Address, tokenId []*big.Int) (*DomainExchangeFixedPriceBuyForERC721Iterator, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "FixedPriceBuyForERC721", buyerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeFixedPriceBuyForERC721Iterator{contract: _DomainExchange.contract, event: "FixedPriceBuyForERC721", logs: logs, sub: sub}, nil
}

// WatchFixedPriceBuyForERC721 is a free log subscription operation binding the contract event 0x136f4960bea84bfd52ef1a10488a32720bb02d45794328fe6798144ad2d0289c.
//
// Solidity: event FixedPriceBuyForERC721(address indexed buyer, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) WatchFixedPriceBuyForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeFixedPriceBuyForERC721, buyer []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "FixedPriceBuyForERC721", buyerRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeFixedPriceBuyForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceBuyForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseFixedPriceBuyForERC721 is a log parse operation binding the contract event 0x136f4960bea84bfd52ef1a10488a32720bb02d45794328fe6798144ad2d0289c.
//
// Solidity: event FixedPriceBuyForERC721(address indexed buyer, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) ParseFixedPriceBuyForERC721(log types.Log) (*DomainExchangeFixedPriceBuyForERC721, error) {
	event := new(DomainExchangeFixedPriceBuyForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceBuyForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeFixedPriceCancelForERC721Iterator is returned from FilterFixedPriceCancelForERC721 and is used to iterate over the raw logs and unpacked data for FixedPriceCancelForERC721 events raised by the DomainExchange contract.
type DomainExchangeFixedPriceCancelForERC721Iterator struct {
	Event *DomainExchangeFixedPriceCancelForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeFixedPriceCancelForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeFixedPriceCancelForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeFixedPriceCancelForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeFixedPriceCancelForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeFixedPriceCancelForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeFixedPriceCancelForERC721 represents a FixedPriceCancelForERC721 event raised by the DomainExchange contract.
type DomainExchangeFixedPriceCancelForERC721 struct {
	Curator common.Address
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterFixedPriceCancelForERC721 is a free log retrieval operation binding the contract event 0x2b1ded6943e65e92548aa93bf3529ae3d9934bd2e7fe2a9911182fa32bfe0b32.
//
// Solidity: event FixedPriceCancelForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) FilterFixedPriceCancelForERC721(opts *bind.FilterOpts, curator []common.Address, tokenId []*big.Int) (*DomainExchangeFixedPriceCancelForERC721Iterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "FixedPriceCancelForERC721", curatorRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeFixedPriceCancelForERC721Iterator{contract: _DomainExchange.contract, event: "FixedPriceCancelForERC721", logs: logs, sub: sub}, nil
}

// WatchFixedPriceCancelForERC721 is a free log subscription operation binding the contract event 0x2b1ded6943e65e92548aa93bf3529ae3d9934bd2e7fe2a9911182fa32bfe0b32.
//
// Solidity: event FixedPriceCancelForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) WatchFixedPriceCancelForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeFixedPriceCancelForERC721, curator []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "FixedPriceCancelForERC721", curatorRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeFixedPriceCancelForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceCancelForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseFixedPriceCancelForERC721 is a log parse operation binding the contract event 0x2b1ded6943e65e92548aa93bf3529ae3d9934bd2e7fe2a9911182fa32bfe0b32.
//
// Solidity: event FixedPriceCancelForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) ParseFixedPriceCancelForERC721(log types.Log) (*DomainExchangeFixedPriceCancelForERC721, error) {
	event := new(DomainExchangeFixedPriceCancelForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceCancelForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeFixedPriceOrderForERC721Iterator is returned from FilterFixedPriceOrderForERC721 and is used to iterate over the raw logs and unpacked data for FixedPriceOrderForERC721 events raised by the DomainExchange contract.
type DomainExchangeFixedPriceOrderForERC721Iterator struct {
	Event *DomainExchangeFixedPriceOrderForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeFixedPriceOrderForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeFixedPriceOrderForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeFixedPriceOrderForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeFixedPriceOrderForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeFixedPriceOrderForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeFixedPriceOrderForERC721 represents a FixedPriceOrderForERC721 event raised by the DomainExchange contract.
type DomainExchangeFixedPriceOrderForERC721 struct {
	Curator common.Address
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterFixedPriceOrderForERC721 is a free log retrieval operation binding the contract event 0xa2d606d1ed7344efff588848a3a0941ba6d9b8c57826751028e3c90fe354da8a.
//
// Solidity: event FixedPriceOrderForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) FilterFixedPriceOrderForERC721(opts *bind.FilterOpts, curator []common.Address, tokenId []*big.Int) (*DomainExchangeFixedPriceOrderForERC721Iterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "FixedPriceOrderForERC721", curatorRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeFixedPriceOrderForERC721Iterator{contract: _DomainExchange.contract, event: "FixedPriceOrderForERC721", logs: logs, sub: sub}, nil
}

// WatchFixedPriceOrderForERC721 is a free log subscription operation binding the contract event 0xa2d606d1ed7344efff588848a3a0941ba6d9b8c57826751028e3c90fe354da8a.
//
// Solidity: event FixedPriceOrderForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) WatchFixedPriceOrderForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeFixedPriceOrderForERC721, curator []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "FixedPriceOrderForERC721", curatorRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeFixedPriceOrderForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceOrderForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseFixedPriceOrderForERC721 is a log parse operation binding the contract event 0xa2d606d1ed7344efff588848a3a0941ba6d9b8c57826751028e3c90fe354da8a.
//
// Solidity: event FixedPriceOrderForERC721(address indexed curator, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) ParseFixedPriceOrderForERC721(log types.Log) (*DomainExchangeFixedPriceOrderForERC721, error) {
	event := new(DomainExchangeFixedPriceOrderForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "FixedPriceOrderForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeMakeOfferForERC721Iterator is returned from FilterMakeOfferForERC721 and is used to iterate over the raw logs and unpacked data for MakeOfferForERC721 events raised by the DomainExchange contract.
type DomainExchangeMakeOfferForERC721Iterator struct {
	Event *DomainExchangeMakeOfferForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeMakeOfferForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeMakeOfferForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeMakeOfferForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeMakeOfferForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeMakeOfferForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeMakeOfferForERC721 represents a MakeOfferForERC721 event raised by the DomainExchange contract.
type DomainExchangeMakeOfferForERC721 struct {
	Bidder        common.Address
	TokenId       *big.Int
	Price         *big.Int
	OfferDuration *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterMakeOfferForERC721 is a free log retrieval operation binding the contract event 0x053745e7e5bc6657adc21948f873094aa2ddc11f5b85c1010c1f8614195c08d3.
//
// Solidity: event MakeOfferForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price, uint256 offerDuration)
func (_DomainExchange *DomainExchangeFilterer) FilterMakeOfferForERC721(opts *bind.FilterOpts, bidder []common.Address, tokenId []*big.Int) (*DomainExchangeMakeOfferForERC721Iterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "MakeOfferForERC721", bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeMakeOfferForERC721Iterator{contract: _DomainExchange.contract, event: "MakeOfferForERC721", logs: logs, sub: sub}, nil
}

// WatchMakeOfferForERC721 is a free log subscription operation binding the contract event 0x053745e7e5bc6657adc21948f873094aa2ddc11f5b85c1010c1f8614195c08d3.
//
// Solidity: event MakeOfferForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price, uint256 offerDuration)
func (_DomainExchange *DomainExchangeFilterer) WatchMakeOfferForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeMakeOfferForERC721, bidder []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "MakeOfferForERC721", bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeMakeOfferForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "MakeOfferForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMakeOfferForERC721 is a log parse operation binding the contract event 0x053745e7e5bc6657adc21948f873094aa2ddc11f5b85c1010c1f8614195c08d3.
//
// Solidity: event MakeOfferForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price, uint256 offerDuration)
func (_DomainExchange *DomainExchangeFilterer) ParseMakeOfferForERC721(log types.Log) (*DomainExchangeMakeOfferForERC721, error) {
	event := new(DomainExchangeMakeOfferForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "MakeOfferForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeOfferToSellForERC721Iterator is returned from FilterOfferToSellForERC721 and is used to iterate over the raw logs and unpacked data for OfferToSellForERC721 events raised by the DomainExchange contract.
type DomainExchangeOfferToSellForERC721Iterator struct {
	Event *DomainExchangeOfferToSellForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeOfferToSellForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeOfferToSellForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeOfferToSellForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeOfferToSellForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeOfferToSellForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeOfferToSellForERC721 represents a OfferToSellForERC721 event raised by the DomainExchange contract.
type DomainExchangeOfferToSellForERC721 struct {
	Curator common.Address
	Bidder  common.Address
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterOfferToSellForERC721 is a free log retrieval operation binding the contract event 0x7fe0a2a90cc886749c6af26922861d02fddece8aa69005bbdc2c1bdbb0018eaa.
//
// Solidity: event OfferToSellForERC721(address indexed curator, address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) FilterOfferToSellForERC721(opts *bind.FilterOpts, curator []common.Address, bidder []common.Address, tokenId []*big.Int) (*DomainExchangeOfferToSellForERC721Iterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "OfferToSellForERC721", curatorRule, bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeOfferToSellForERC721Iterator{contract: _DomainExchange.contract, event: "OfferToSellForERC721", logs: logs, sub: sub}, nil
}

// WatchOfferToSellForERC721 is a free log subscription operation binding the contract event 0x7fe0a2a90cc886749c6af26922861d02fddece8aa69005bbdc2c1bdbb0018eaa.
//
// Solidity: event OfferToSellForERC721(address indexed curator, address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) WatchOfferToSellForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeOfferToSellForERC721, curator []common.Address, bidder []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}
	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "OfferToSellForERC721", curatorRule, bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeOfferToSellForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "OfferToSellForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOfferToSellForERC721 is a log parse operation binding the contract event 0x7fe0a2a90cc886749c6af26922861d02fddece8aa69005bbdc2c1bdbb0018eaa.
//
// Solidity: event OfferToSellForERC721(address indexed curator, address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) ParseOfferToSellForERC721(log types.Log) (*DomainExchangeOfferToSellForERC721, error) {
	event := new(DomainExchangeOfferToSellForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "OfferToSellForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the DomainExchange contract.
type DomainExchangeOwnershipTransferredIterator struct {
	Event *DomainExchangeOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeOwnershipTransferred represents a OwnershipTransferred event raised by the DomainExchange contract.
type DomainExchangeOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainExchange *DomainExchangeFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*DomainExchangeOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeOwnershipTransferredIterator{contract: _DomainExchange.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainExchange *DomainExchangeFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *DomainExchangeOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeOwnershipTransferred)
				if err := _DomainExchange.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainExchange *DomainExchangeFilterer) ParseOwnershipTransferred(log types.Log) (*DomainExchangeOwnershipTransferred, error) {
	event := new(DomainExchangeOwnershipTransferred)
	if err := _DomainExchange.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangePausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the DomainExchange contract.
type DomainExchangePausedIterator struct {
	Event *DomainExchangePaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangePausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangePaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangePaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangePausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangePausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangePaused represents a Paused event raised by the DomainExchange contract.
type DomainExchangePaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainExchange *DomainExchangeFilterer) FilterPaused(opts *bind.FilterOpts) (*DomainExchangePausedIterator, error) {

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &DomainExchangePausedIterator{contract: _DomainExchange.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainExchange *DomainExchangeFilterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *DomainExchangePaused) (event.Subscription, error) {

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangePaused)
				if err := _DomainExchange.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainExchange *DomainExchangeFilterer) ParsePaused(log types.Log) (*DomainExchangePaused, error) {
	event := new(DomainExchangePaused)
	if err := _DomainExchange.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangePreSaleBuyForERC20Iterator is returned from FilterPreSaleBuyForERC20 and is used to iterate over the raw logs and unpacked data for PreSaleBuyForERC20 events raised by the DomainExchange contract.
type DomainExchangePreSaleBuyForERC20Iterator struct {
	Event *DomainExchangePreSaleBuyForERC20 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangePreSaleBuyForERC20Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangePreSaleBuyForERC20)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangePreSaleBuyForERC20)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangePreSaleBuyForERC20Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangePreSaleBuyForERC20Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangePreSaleBuyForERC20 represents a PreSaleBuyForERC20 event raised by the DomainExchange contract.
type DomainExchangePreSaleBuyForERC20 struct {
	Buyer       common.Address
	DomainERC20 common.Address
	Amount      *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPreSaleBuyForERC20 is a free log retrieval operation binding the contract event 0x07a09d5c869b8fea6138608be2281af980ee484514085421f6c2c953868b78cf.
//
// Solidity: event PreSaleBuyForERC20(address indexed buyer, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) FilterPreSaleBuyForERC20(opts *bind.FilterOpts, buyer []common.Address) (*DomainExchangePreSaleBuyForERC20Iterator, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "PreSaleBuyForERC20", buyerRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangePreSaleBuyForERC20Iterator{contract: _DomainExchange.contract, event: "PreSaleBuyForERC20", logs: logs, sub: sub}, nil
}

// WatchPreSaleBuyForERC20 is a free log subscription operation binding the contract event 0x07a09d5c869b8fea6138608be2281af980ee484514085421f6c2c953868b78cf.
//
// Solidity: event PreSaleBuyForERC20(address indexed buyer, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) WatchPreSaleBuyForERC20(opts *bind.WatchOpts, sink chan<- *DomainExchangePreSaleBuyForERC20, buyer []common.Address) (event.Subscription, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "PreSaleBuyForERC20", buyerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangePreSaleBuyForERC20)
				if err := _DomainExchange.contract.UnpackLog(event, "PreSaleBuyForERC20", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePreSaleBuyForERC20 is a log parse operation binding the contract event 0x07a09d5c869b8fea6138608be2281af980ee484514085421f6c2c953868b78cf.
//
// Solidity: event PreSaleBuyForERC20(address indexed buyer, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) ParsePreSaleBuyForERC20(log types.Log) (*DomainExchangePreSaleBuyForERC20, error) {
	event := new(DomainExchangePreSaleBuyForERC20)
	if err := _DomainExchange.contract.UnpackLog(event, "PreSaleBuyForERC20", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangePreSaleOrderForERC20Iterator is returned from FilterPreSaleOrderForERC20 and is used to iterate over the raw logs and unpacked data for PreSaleOrderForERC20 events raised by the DomainExchange contract.
type DomainExchangePreSaleOrderForERC20Iterator struct {
	Event *DomainExchangePreSaleOrderForERC20 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangePreSaleOrderForERC20Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangePreSaleOrderForERC20)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangePreSaleOrderForERC20)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangePreSaleOrderForERC20Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangePreSaleOrderForERC20Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangePreSaleOrderForERC20 represents a PreSaleOrderForERC20 event raised by the DomainExchange contract.
type DomainExchangePreSaleOrderForERC20 struct {
	Curator     common.Address
	DomainERC20 common.Address
	Amount      *big.Int
	Price       *big.Int
	StartTime   *big.Int
	EndTime     *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPreSaleOrderForERC20 is a free log retrieval operation binding the contract event 0xf3da7c6a5c9039cf5c6e58a05d2448dee3cc6f06b737ef878b082e949ffa20d4.
//
// Solidity: event PreSaleOrderForERC20(address indexed curator, address domainERC20, uint256 amount, uint256 price, uint256 indexed startTime, uint256 indexed endTime)
func (_DomainExchange *DomainExchangeFilterer) FilterPreSaleOrderForERC20(opts *bind.FilterOpts, curator []common.Address, startTime []*big.Int, endTime []*big.Int) (*DomainExchangePreSaleOrderForERC20Iterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	var startTimeRule []interface{}
	for _, startTimeItem := range startTime {
		startTimeRule = append(startTimeRule, startTimeItem)
	}
	var endTimeRule []interface{}
	for _, endTimeItem := range endTime {
		endTimeRule = append(endTimeRule, endTimeItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "PreSaleOrderForERC20", curatorRule, startTimeRule, endTimeRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangePreSaleOrderForERC20Iterator{contract: _DomainExchange.contract, event: "PreSaleOrderForERC20", logs: logs, sub: sub}, nil
}

// WatchPreSaleOrderForERC20 is a free log subscription operation binding the contract event 0xf3da7c6a5c9039cf5c6e58a05d2448dee3cc6f06b737ef878b082e949ffa20d4.
//
// Solidity: event PreSaleOrderForERC20(address indexed curator, address domainERC20, uint256 amount, uint256 price, uint256 indexed startTime, uint256 indexed endTime)
func (_DomainExchange *DomainExchangeFilterer) WatchPreSaleOrderForERC20(opts *bind.WatchOpts, sink chan<- *DomainExchangePreSaleOrderForERC20, curator []common.Address, startTime []*big.Int, endTime []*big.Int) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	var startTimeRule []interface{}
	for _, startTimeItem := range startTime {
		startTimeRule = append(startTimeRule, startTimeItem)
	}
	var endTimeRule []interface{}
	for _, endTimeItem := range endTime {
		endTimeRule = append(endTimeRule, endTimeItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "PreSaleOrderForERC20", curatorRule, startTimeRule, endTimeRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangePreSaleOrderForERC20)
				if err := _DomainExchange.contract.UnpackLog(event, "PreSaleOrderForERC20", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePreSaleOrderForERC20 is a log parse operation binding the contract event 0xf3da7c6a5c9039cf5c6e58a05d2448dee3cc6f06b737ef878b082e949ffa20d4.
//
// Solidity: event PreSaleOrderForERC20(address indexed curator, address domainERC20, uint256 amount, uint256 price, uint256 indexed startTime, uint256 indexed endTime)
func (_DomainExchange *DomainExchangeFilterer) ParsePreSaleOrderForERC20(log types.Log) (*DomainExchangePreSaleOrderForERC20, error) {
	event := new(DomainExchangePreSaleOrderForERC20)
	if err := _DomainExchange.contract.UnpackLog(event, "PreSaleOrderForERC20", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangePreSaleWithdrawForERC20Iterator is returned from FilterPreSaleWithdrawForERC20 and is used to iterate over the raw logs and unpacked data for PreSaleWithdrawForERC20 events raised by the DomainExchange contract.
type DomainExchangePreSaleWithdrawForERC20Iterator struct {
	Event *DomainExchangePreSaleWithdrawForERC20 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangePreSaleWithdrawForERC20Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangePreSaleWithdrawForERC20)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangePreSaleWithdrawForERC20)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangePreSaleWithdrawForERC20Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangePreSaleWithdrawForERC20Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangePreSaleWithdrawForERC20 represents a PreSaleWithdrawForERC20 event raised by the DomainExchange contract.
type DomainExchangePreSaleWithdrawForERC20 struct {
	Curator     common.Address
	DomainERC20 common.Address
	Amount      *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPreSaleWithdrawForERC20 is a free log retrieval operation binding the contract event 0xda24867139c432099170dbe42f63e6788be16ec26cf5004fb32cf800d43f67e8.
//
// Solidity: event PreSaleWithdrawForERC20(address indexed curator, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) FilterPreSaleWithdrawForERC20(opts *bind.FilterOpts, curator []common.Address) (*DomainExchangePreSaleWithdrawForERC20Iterator, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "PreSaleWithdrawForERC20", curatorRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangePreSaleWithdrawForERC20Iterator{contract: _DomainExchange.contract, event: "PreSaleWithdrawForERC20", logs: logs, sub: sub}, nil
}

// WatchPreSaleWithdrawForERC20 is a free log subscription operation binding the contract event 0xda24867139c432099170dbe42f63e6788be16ec26cf5004fb32cf800d43f67e8.
//
// Solidity: event PreSaleWithdrawForERC20(address indexed curator, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) WatchPreSaleWithdrawForERC20(opts *bind.WatchOpts, sink chan<- *DomainExchangePreSaleWithdrawForERC20, curator []common.Address) (event.Subscription, error) {

	var curatorRule []interface{}
	for _, curatorItem := range curator {
		curatorRule = append(curatorRule, curatorItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "PreSaleWithdrawForERC20", curatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangePreSaleWithdrawForERC20)
				if err := _DomainExchange.contract.UnpackLog(event, "PreSaleWithdrawForERC20", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePreSaleWithdrawForERC20 is a log parse operation binding the contract event 0xda24867139c432099170dbe42f63e6788be16ec26cf5004fb32cf800d43f67e8.
//
// Solidity: event PreSaleWithdrawForERC20(address indexed curator, address domainERC20, uint256 amount)
func (_DomainExchange *DomainExchangeFilterer) ParsePreSaleWithdrawForERC20(log types.Log) (*DomainExchangePreSaleWithdrawForERC20, error) {
	event := new(DomainExchangePreSaleWithdrawForERC20)
	if err := _DomainExchange.contract.UnpackLog(event, "PreSaleWithdrawForERC20", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeUnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the DomainExchange contract.
type DomainExchangeUnpausedIterator struct {
	Event *DomainExchangeUnpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeUnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeUnpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeUnpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeUnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeUnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeUnpaused represents a Unpaused event raised by the DomainExchange contract.
type DomainExchangeUnpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainExchange *DomainExchangeFilterer) FilterUnpaused(opts *bind.FilterOpts) (*DomainExchangeUnpausedIterator, error) {

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &DomainExchangeUnpausedIterator{contract: _DomainExchange.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainExchange *DomainExchangeFilterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *DomainExchangeUnpaused) (event.Subscription, error) {

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeUnpaused)
				if err := _DomainExchange.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainExchange *DomainExchangeFilterer) ParseUnpaused(log types.Log) (*DomainExchangeUnpaused, error) {
	event := new(DomainExchangeUnpaused)
	if err := _DomainExchange.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// DomainExchangeWithdrawOfferFundForERC721Iterator is returned from FilterWithdrawOfferFundForERC721 and is used to iterate over the raw logs and unpacked data for WithdrawOfferFundForERC721 events raised by the DomainExchange contract.
type DomainExchangeWithdrawOfferFundForERC721Iterator struct {
	Event *DomainExchangeWithdrawOfferFundForERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainExchangeWithdrawOfferFundForERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainExchangeWithdrawOfferFundForERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainExchangeWithdrawOfferFundForERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainExchangeWithdrawOfferFundForERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainExchangeWithdrawOfferFundForERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainExchangeWithdrawOfferFundForERC721 represents a WithdrawOfferFundForERC721 event raised by the DomainExchange contract.
type DomainExchangeWithdrawOfferFundForERC721 struct {
	Bidder  common.Address
	TokenId *big.Int
	Price   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterWithdrawOfferFundForERC721 is a free log retrieval operation binding the contract event 0xd273f709f986c96b8cf648e8679c76e689dd497f960d9edc8e923b17b4e01221.
//
// Solidity: event WithdrawOfferFundForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) FilterWithdrawOfferFundForERC721(opts *bind.FilterOpts, bidder []common.Address, tokenId []*big.Int) (*DomainExchangeWithdrawOfferFundForERC721Iterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.FilterLogs(opts, "WithdrawOfferFundForERC721", bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &DomainExchangeWithdrawOfferFundForERC721Iterator{contract: _DomainExchange.contract, event: "WithdrawOfferFundForERC721", logs: logs, sub: sub}, nil
}

// WatchWithdrawOfferFundForERC721 is a free log subscription operation binding the contract event 0xd273f709f986c96b8cf648e8679c76e689dd497f960d9edc8e923b17b4e01221.
//
// Solidity: event WithdrawOfferFundForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) WatchWithdrawOfferFundForERC721(opts *bind.WatchOpts, sink chan<- *DomainExchangeWithdrawOfferFundForERC721, bidder []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _DomainExchange.contract.WatchLogs(opts, "WithdrawOfferFundForERC721", bidderRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainExchangeWithdrawOfferFundForERC721)
				if err := _DomainExchange.contract.UnpackLog(event, "WithdrawOfferFundForERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWithdrawOfferFundForERC721 is a log parse operation binding the contract event 0xd273f709f986c96b8cf648e8679c76e689dd497f960d9edc8e923b17b4e01221.
//
// Solidity: event WithdrawOfferFundForERC721(address indexed bidder, uint256 indexed tokenId, uint256 price)
func (_DomainExchange *DomainExchangeFilterer) ParseWithdrawOfferFundForERC721(log types.Log) (*DomainExchangeWithdrawOfferFundForERC721, error) {
	event := new(DomainExchangeWithdrawOfferFundForERC721)
	if err := _DomainExchange.contract.UnpackLog(event, "WithdrawOfferFundForERC721", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
