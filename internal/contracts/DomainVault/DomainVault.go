// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package DomainVault

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// IDomainVaultInfo is an auto generated low-level Go binding around an user-defined struct.
type IDomainVaultInfo struct {
	WrapperNativeToken   common.Address
	DomainSettings       common.Address
	Erc20                common.Address
	Erc20InitTotalSupply *big.Int
	Erc721               common.Address
	Erc721TokenId        *big.Int
	ReservePrice         *big.Int
	VoteDuration         *big.Int
	VoteEndTime          *big.Int
	Votes                *big.Int
	VotePercentage       *big.Int
	AuctionDuration      *big.Int
	AuctionEndTime       *big.Int
	BidPrice             *big.Int
	Bidder               common.Address
	Domain               [32]byte
	Erc20Name            string
	Erc20Symbol          string
	Erc20Decimals        uint8
	Erc20TotalSupply     *big.Int
	Erc20BalanceOf       *big.Int
	IsVotePass           bool
	Voted                bool
	TokenIdOwner         common.Address
}

// DomainVaultABI is the input ABI used to generate the binding from.
const DomainVaultABI = "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"bidPrice\",\"type\":\"uint256\"}],\"name\":\"Bid\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"bidPrice\",\"type\":\"uint256\"}],\"name\":\"MakeOfferBid\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"Redeem\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"voter\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"votes\",\"type\":\"uint256\"}],\"name\":\"Vote\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"bidPrice\",\"type\":\"uint256\"}],\"name\":\"WithdrawBidFund\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"bidPrice\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"WithdrawERC721\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"erc20Owner\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"withdrawERC20Amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"fundsShareAmount\",\"type\":\"uint256\"}],\"name\":\"WithdrawFund\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"auctionDuration\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"auctionEndTime\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"bidPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"bidder\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainERC20\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainERC20InitTotalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainERC721\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainERC721TokenId\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"domainSettings\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"reservePrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"voteDuration\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"voteEndTime\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"votePercentage\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"votes\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wrapperNativeToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"domainERC20_\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"domainERC20TotalSupply_\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"domainERC721_\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"domainERC721TokenId_\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"domainSettings_\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"reservePrice_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"voteDuration_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"votePercentage_\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"auctionDuration_\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"wrapperNativeToken_\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"afterTokenTransferForDomainERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"redeemWithPermit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"redeem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"makeOfferBid\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"vote\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"bid\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdrawBidFund\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdrawERC721\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"withdrawERC20Amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"withdrawFundWithPermit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"withdrawERC20Amount\",\"type\":\"uint256\"}],\"name\":\"withdrawFund\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getInfo\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"wrapperNativeToken\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"domainSettings\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"erc20\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"erc20InitTotalSupply\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"erc721\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"erc721TokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"reservePrice\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"voteDuration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"voteEndTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"votes\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"votePercentage\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"auctionDuration\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"auctionEndTime\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"bidPrice\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"bytes32\",\"name\":\"domain\",\"type\":\"bytes32\"},{\"internalType\":\"string\",\"name\":\"erc20Name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"erc20Symbol\",\"type\":\"string\"},{\"internalType\":\"uint8\",\"name\":\"erc20Decimals\",\"type\":\"uint8\"},{\"internalType\":\"uint256\",\"name\":\"erc20TotalSupply\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"erc20BalanceOf\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"isVotePass\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"voted\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"tokenIdOwner\",\"type\":\"address\"}],\"internalType\":\"structIDomainVault.Info\",\"name\":\"info\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getVoterList\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"voterList\",\"type\":\"address[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

// DomainVault is an auto generated Go binding around an Ethereum contract.
type DomainVault struct {
	DomainVaultCaller     // Read-only binding to the contract
	DomainVaultTransactor // Write-only binding to the contract
	DomainVaultFilterer   // Log filterer for contract events
}

// DomainVaultCaller is an auto generated read-only Go binding around an Ethereum contract.
type DomainVaultCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainVaultTransactor is an auto generated write-only Go binding around an Ethereum contract.
type DomainVaultTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainVaultFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type DomainVaultFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DomainVaultSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type DomainVaultSession struct {
	Contract     *DomainVault      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// DomainVaultCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type DomainVaultCallerSession struct {
	Contract *DomainVaultCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// DomainVaultTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type DomainVaultTransactorSession struct {
	Contract     *DomainVaultTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// DomainVaultRaw is an auto generated low-level Go binding around an Ethereum contract.
type DomainVaultRaw struct {
	Contract *DomainVault // Generic contract binding to access the raw methods on
}

// DomainVaultCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type DomainVaultCallerRaw struct {
	Contract *DomainVaultCaller // Generic read-only contract binding to access the raw methods on
}

// DomainVaultTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type DomainVaultTransactorRaw struct {
	Contract *DomainVaultTransactor // Generic write-only contract binding to access the raw methods on
}

// NewDomainVault creates a new instance of DomainVault, bound to a specific deployed contract.
func NewDomainVault(address common.Address, backend bind.ContractBackend) (*DomainVault, error) {
	contract, err := bindDomainVault(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &DomainVault{DomainVaultCaller: DomainVaultCaller{contract: contract}, DomainVaultTransactor: DomainVaultTransactor{contract: contract}, DomainVaultFilterer: DomainVaultFilterer{contract: contract}}, nil
}

// NewDomainVaultCaller creates a new read-only instance of DomainVault, bound to a specific deployed contract.
func NewDomainVaultCaller(address common.Address, caller bind.ContractCaller) (*DomainVaultCaller, error) {
	contract, err := bindDomainVault(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &DomainVaultCaller{contract: contract}, nil
}

// NewDomainVaultTransactor creates a new write-only instance of DomainVault, bound to a specific deployed contract.
func NewDomainVaultTransactor(address common.Address, transactor bind.ContractTransactor) (*DomainVaultTransactor, error) {
	contract, err := bindDomainVault(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &DomainVaultTransactor{contract: contract}, nil
}

// NewDomainVaultFilterer creates a new log filterer instance of DomainVault, bound to a specific deployed contract.
func NewDomainVaultFilterer(address common.Address, filterer bind.ContractFilterer) (*DomainVaultFilterer, error) {
	contract, err := bindDomainVault(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &DomainVaultFilterer{contract: contract}, nil
}

// bindDomainVault binds a generic wrapper to an already deployed contract.
func bindDomainVault(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(DomainVaultABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainVault *DomainVaultRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainVault.Contract.DomainVaultCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainVault *DomainVaultRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.Contract.DomainVaultTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainVault *DomainVaultRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainVault.Contract.DomainVaultTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DomainVault *DomainVaultCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _DomainVault.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DomainVault *DomainVaultTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DomainVault *DomainVaultTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DomainVault.Contract.contract.Transact(opts, method, params...)
}

// AuctionDuration is a free data retrieval call binding the contract method 0x0cbf54c8.
//
// Solidity: function auctionDuration() view returns(uint256)
func (_DomainVault *DomainVaultCaller) AuctionDuration(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "auctionDuration")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AuctionDuration is a free data retrieval call binding the contract method 0x0cbf54c8.
//
// Solidity: function auctionDuration() view returns(uint256)
func (_DomainVault *DomainVaultSession) AuctionDuration() (*big.Int, error) {
	return _DomainVault.Contract.AuctionDuration(&_DomainVault.CallOpts)
}

// AuctionDuration is a free data retrieval call binding the contract method 0x0cbf54c8.
//
// Solidity: function auctionDuration() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) AuctionDuration() (*big.Int, error) {
	return _DomainVault.Contract.AuctionDuration(&_DomainVault.CallOpts)
}

// AuctionEndTime is a free data retrieval call binding the contract method 0x4b449cba.
//
// Solidity: function auctionEndTime() view returns(uint256)
func (_DomainVault *DomainVaultCaller) AuctionEndTime(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "auctionEndTime")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AuctionEndTime is a free data retrieval call binding the contract method 0x4b449cba.
//
// Solidity: function auctionEndTime() view returns(uint256)
func (_DomainVault *DomainVaultSession) AuctionEndTime() (*big.Int, error) {
	return _DomainVault.Contract.AuctionEndTime(&_DomainVault.CallOpts)
}

// AuctionEndTime is a free data retrieval call binding the contract method 0x4b449cba.
//
// Solidity: function auctionEndTime() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) AuctionEndTime() (*big.Int, error) {
	return _DomainVault.Contract.AuctionEndTime(&_DomainVault.CallOpts)
}

// BidPrice is a free data retrieval call binding the contract method 0x19afe473.
//
// Solidity: function bidPrice() view returns(uint256)
func (_DomainVault *DomainVaultCaller) BidPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "bidPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BidPrice is a free data retrieval call binding the contract method 0x19afe473.
//
// Solidity: function bidPrice() view returns(uint256)
func (_DomainVault *DomainVaultSession) BidPrice() (*big.Int, error) {
	return _DomainVault.Contract.BidPrice(&_DomainVault.CallOpts)
}

// BidPrice is a free data retrieval call binding the contract method 0x19afe473.
//
// Solidity: function bidPrice() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) BidPrice() (*big.Int, error) {
	return _DomainVault.Contract.BidPrice(&_DomainVault.CallOpts)
}

// Bidder is a free data retrieval call binding the contract method 0xf496d882.
//
// Solidity: function bidder() view returns(address)
func (_DomainVault *DomainVaultCaller) Bidder(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "bidder")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Bidder is a free data retrieval call binding the contract method 0xf496d882.
//
// Solidity: function bidder() view returns(address)
func (_DomainVault *DomainVaultSession) Bidder() (common.Address, error) {
	return _DomainVault.Contract.Bidder(&_DomainVault.CallOpts)
}

// Bidder is a free data retrieval call binding the contract method 0xf496d882.
//
// Solidity: function bidder() view returns(address)
func (_DomainVault *DomainVaultCallerSession) Bidder() (common.Address, error) {
	return _DomainVault.Contract.Bidder(&_DomainVault.CallOpts)
}

// DomainERC20 is a free data retrieval call binding the contract method 0xd3dfc450.
//
// Solidity: function domainERC20() view returns(address)
func (_DomainVault *DomainVaultCaller) DomainERC20(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "domainERC20")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainERC20 is a free data retrieval call binding the contract method 0xd3dfc450.
//
// Solidity: function domainERC20() view returns(address)
func (_DomainVault *DomainVaultSession) DomainERC20() (common.Address, error) {
	return _DomainVault.Contract.DomainERC20(&_DomainVault.CallOpts)
}

// DomainERC20 is a free data retrieval call binding the contract method 0xd3dfc450.
//
// Solidity: function domainERC20() view returns(address)
func (_DomainVault *DomainVaultCallerSession) DomainERC20() (common.Address, error) {
	return _DomainVault.Contract.DomainERC20(&_DomainVault.CallOpts)
}

// DomainERC20InitTotalSupply is a free data retrieval call binding the contract method 0xa23fa80a.
//
// Solidity: function domainERC20InitTotalSupply() view returns(uint256)
func (_DomainVault *DomainVaultCaller) DomainERC20InitTotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "domainERC20InitTotalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DomainERC20InitTotalSupply is a free data retrieval call binding the contract method 0xa23fa80a.
//
// Solidity: function domainERC20InitTotalSupply() view returns(uint256)
func (_DomainVault *DomainVaultSession) DomainERC20InitTotalSupply() (*big.Int, error) {
	return _DomainVault.Contract.DomainERC20InitTotalSupply(&_DomainVault.CallOpts)
}

// DomainERC20InitTotalSupply is a free data retrieval call binding the contract method 0xa23fa80a.
//
// Solidity: function domainERC20InitTotalSupply() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) DomainERC20InitTotalSupply() (*big.Int, error) {
	return _DomainVault.Contract.DomainERC20InitTotalSupply(&_DomainVault.CallOpts)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainVault *DomainVaultCaller) DomainERC721(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "domainERC721")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainVault *DomainVaultSession) DomainERC721() (common.Address, error) {
	return _DomainVault.Contract.DomainERC721(&_DomainVault.CallOpts)
}

// DomainERC721 is a free data retrieval call binding the contract method 0x4bfa457d.
//
// Solidity: function domainERC721() view returns(address)
func (_DomainVault *DomainVaultCallerSession) DomainERC721() (common.Address, error) {
	return _DomainVault.Contract.DomainERC721(&_DomainVault.CallOpts)
}

// DomainERC721TokenId is a free data retrieval call binding the contract method 0xc2ff45c5.
//
// Solidity: function domainERC721TokenId() view returns(uint256)
func (_DomainVault *DomainVaultCaller) DomainERC721TokenId(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "domainERC721TokenId")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DomainERC721TokenId is a free data retrieval call binding the contract method 0xc2ff45c5.
//
// Solidity: function domainERC721TokenId() view returns(uint256)
func (_DomainVault *DomainVaultSession) DomainERC721TokenId() (*big.Int, error) {
	return _DomainVault.Contract.DomainERC721TokenId(&_DomainVault.CallOpts)
}

// DomainERC721TokenId is a free data retrieval call binding the contract method 0xc2ff45c5.
//
// Solidity: function domainERC721TokenId() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) DomainERC721TokenId() (*big.Int, error) {
	return _DomainVault.Contract.DomainERC721TokenId(&_DomainVault.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainVault *DomainVaultCaller) DomainSettings(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "domainSettings")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainVault *DomainVaultSession) DomainSettings() (common.Address, error) {
	return _DomainVault.Contract.DomainSettings(&_DomainVault.CallOpts)
}

// DomainSettings is a free data retrieval call binding the contract method 0xf9394522.
//
// Solidity: function domainSettings() view returns(address)
func (_DomainVault *DomainVaultCallerSession) DomainSettings() (common.Address, error) {
	return _DomainVault.Contract.DomainSettings(&_DomainVault.CallOpts)
}

// GetInfo is a free data retrieval call binding the contract method 0xffdd5cf1.
//
// Solidity: function getInfo(address account) view returns((address,address,address,uint256,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address,bytes32,string,string,uint8,uint256,uint256,bool,bool,address) info)
func (_DomainVault *DomainVaultCaller) GetInfo(opts *bind.CallOpts, account common.Address) (IDomainVaultInfo, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "getInfo", account)

	if err != nil {
		return *new(IDomainVaultInfo), err
	}

	out0 := *abi.ConvertType(out[0], new(IDomainVaultInfo)).(*IDomainVaultInfo)

	return out0, err

}

// GetInfo is a free data retrieval call binding the contract method 0xffdd5cf1.
//
// Solidity: function getInfo(address account) view returns((address,address,address,uint256,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address,bytes32,string,string,uint8,uint256,uint256,bool,bool,address) info)
func (_DomainVault *DomainVaultSession) GetInfo(account common.Address) (IDomainVaultInfo, error) {
	return _DomainVault.Contract.GetInfo(&_DomainVault.CallOpts, account)
}

// GetInfo is a free data retrieval call binding the contract method 0xffdd5cf1.
//
// Solidity: function getInfo(address account) view returns((address,address,address,uint256,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256,address,bytes32,string,string,uint8,uint256,uint256,bool,bool,address) info)
func (_DomainVault *DomainVaultCallerSession) GetInfo(account common.Address) (IDomainVaultInfo, error) {
	return _DomainVault.Contract.GetInfo(&_DomainVault.CallOpts, account)
}

// GetVoterList is a free data retrieval call binding the contract method 0xb28d92ae.
//
// Solidity: function getVoterList() view returns(address[] voterList)
func (_DomainVault *DomainVaultCaller) GetVoterList(opts *bind.CallOpts) ([]common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "getVoterList")

	if err != nil {
		return *new([]common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)

	return out0, err

}

// GetVoterList is a free data retrieval call binding the contract method 0xb28d92ae.
//
// Solidity: function getVoterList() view returns(address[] voterList)
func (_DomainVault *DomainVaultSession) GetVoterList() ([]common.Address, error) {
	return _DomainVault.Contract.GetVoterList(&_DomainVault.CallOpts)
}

// GetVoterList is a free data retrieval call binding the contract method 0xb28d92ae.
//
// Solidity: function getVoterList() view returns(address[] voterList)
func (_DomainVault *DomainVaultCallerSession) GetVoterList() ([]common.Address, error) {
	return _DomainVault.Contract.GetVoterList(&_DomainVault.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainVault *DomainVaultCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainVault *DomainVaultSession) Owner() (common.Address, error) {
	return _DomainVault.Contract.Owner(&_DomainVault.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_DomainVault *DomainVaultCallerSession) Owner() (common.Address, error) {
	return _DomainVault.Contract.Owner(&_DomainVault.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainVault *DomainVaultCaller) Paused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "paused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainVault *DomainVaultSession) Paused() (bool, error) {
	return _DomainVault.Contract.Paused(&_DomainVault.CallOpts)
}

// Paused is a free data retrieval call binding the contract method 0x5c975abb.
//
// Solidity: function paused() view returns(bool)
func (_DomainVault *DomainVaultCallerSession) Paused() (bool, error) {
	return _DomainVault.Contract.Paused(&_DomainVault.CallOpts)
}

// ReservePrice is a free data retrieval call binding the contract method 0xdb2e1eed.
//
// Solidity: function reservePrice() view returns(uint256)
func (_DomainVault *DomainVaultCaller) ReservePrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "reservePrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ReservePrice is a free data retrieval call binding the contract method 0xdb2e1eed.
//
// Solidity: function reservePrice() view returns(uint256)
func (_DomainVault *DomainVaultSession) ReservePrice() (*big.Int, error) {
	return _DomainVault.Contract.ReservePrice(&_DomainVault.CallOpts)
}

// ReservePrice is a free data retrieval call binding the contract method 0xdb2e1eed.
//
// Solidity: function reservePrice() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) ReservePrice() (*big.Int, error) {
	return _DomainVault.Contract.ReservePrice(&_DomainVault.CallOpts)
}

// VoteDuration is a free data retrieval call binding the contract method 0x56364499.
//
// Solidity: function voteDuration() view returns(uint256)
func (_DomainVault *DomainVaultCaller) VoteDuration(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "voteDuration")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// VoteDuration is a free data retrieval call binding the contract method 0x56364499.
//
// Solidity: function voteDuration() view returns(uint256)
func (_DomainVault *DomainVaultSession) VoteDuration() (*big.Int, error) {
	return _DomainVault.Contract.VoteDuration(&_DomainVault.CallOpts)
}

// VoteDuration is a free data retrieval call binding the contract method 0x56364499.
//
// Solidity: function voteDuration() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) VoteDuration() (*big.Int, error) {
	return _DomainVault.Contract.VoteDuration(&_DomainVault.CallOpts)
}

// VoteEndTime is a free data retrieval call binding the contract method 0x9e6cb42b.
//
// Solidity: function voteEndTime() view returns(uint256)
func (_DomainVault *DomainVaultCaller) VoteEndTime(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "voteEndTime")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// VoteEndTime is a free data retrieval call binding the contract method 0x9e6cb42b.
//
// Solidity: function voteEndTime() view returns(uint256)
func (_DomainVault *DomainVaultSession) VoteEndTime() (*big.Int, error) {
	return _DomainVault.Contract.VoteEndTime(&_DomainVault.CallOpts)
}

// VoteEndTime is a free data retrieval call binding the contract method 0x9e6cb42b.
//
// Solidity: function voteEndTime() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) VoteEndTime() (*big.Int, error) {
	return _DomainVault.Contract.VoteEndTime(&_DomainVault.CallOpts)
}

// VotePercentage is a free data retrieval call binding the contract method 0x34b502c0.
//
// Solidity: function votePercentage() view returns(uint256)
func (_DomainVault *DomainVaultCaller) VotePercentage(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "votePercentage")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// VotePercentage is a free data retrieval call binding the contract method 0x34b502c0.
//
// Solidity: function votePercentage() view returns(uint256)
func (_DomainVault *DomainVaultSession) VotePercentage() (*big.Int, error) {
	return _DomainVault.Contract.VotePercentage(&_DomainVault.CallOpts)
}

// VotePercentage is a free data retrieval call binding the contract method 0x34b502c0.
//
// Solidity: function votePercentage() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) VotePercentage() (*big.Int, error) {
	return _DomainVault.Contract.VotePercentage(&_DomainVault.CallOpts)
}

// Votes is a free data retrieval call binding the contract method 0xe168c3ec.
//
// Solidity: function votes() view returns(uint256)
func (_DomainVault *DomainVaultCaller) Votes(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "votes")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Votes is a free data retrieval call binding the contract method 0xe168c3ec.
//
// Solidity: function votes() view returns(uint256)
func (_DomainVault *DomainVaultSession) Votes() (*big.Int, error) {
	return _DomainVault.Contract.Votes(&_DomainVault.CallOpts)
}

// Votes is a free data retrieval call binding the contract method 0xe168c3ec.
//
// Solidity: function votes() view returns(uint256)
func (_DomainVault *DomainVaultCallerSession) Votes() (*big.Int, error) {
	return _DomainVault.Contract.Votes(&_DomainVault.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainVault *DomainVaultCaller) WrapperNativeToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _DomainVault.contract.Call(opts, &out, "wrapperNativeToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainVault *DomainVaultSession) WrapperNativeToken() (common.Address, error) {
	return _DomainVault.Contract.WrapperNativeToken(&_DomainVault.CallOpts)
}

// WrapperNativeToken is a free data retrieval call binding the contract method 0xccd90b00.
//
// Solidity: function wrapperNativeToken() view returns(address)
func (_DomainVault *DomainVaultCallerSession) WrapperNativeToken() (common.Address, error) {
	return _DomainVault.Contract.WrapperNativeToken(&_DomainVault.CallOpts)
}

// AfterTokenTransferForDomainERC20 is a paid mutator transaction binding the contract method 0x97f7b776.
//
// Solidity: function afterTokenTransferForDomainERC20(address from, address to, uint256 amount) returns()
func (_DomainVault *DomainVaultTransactor) AfterTokenTransferForDomainERC20(opts *bind.TransactOpts, from common.Address, to common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "afterTokenTransferForDomainERC20", from, to, amount)
}

// AfterTokenTransferForDomainERC20 is a paid mutator transaction binding the contract method 0x97f7b776.
//
// Solidity: function afterTokenTransferForDomainERC20(address from, address to, uint256 amount) returns()
func (_DomainVault *DomainVaultSession) AfterTokenTransferForDomainERC20(from common.Address, to common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.Contract.AfterTokenTransferForDomainERC20(&_DomainVault.TransactOpts, from, to, amount)
}

// AfterTokenTransferForDomainERC20 is a paid mutator transaction binding the contract method 0x97f7b776.
//
// Solidity: function afterTokenTransferForDomainERC20(address from, address to, uint256 amount) returns()
func (_DomainVault *DomainVaultTransactorSession) AfterTokenTransferForDomainERC20(from common.Address, to common.Address, amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.Contract.AfterTokenTransferForDomainERC20(&_DomainVault.TransactOpts, from, to, amount)
}

// Bid is a paid mutator transaction binding the contract method 0x1998aeef.
//
// Solidity: function bid() payable returns()
func (_DomainVault *DomainVaultTransactor) Bid(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "bid")
}

// Bid is a paid mutator transaction binding the contract method 0x1998aeef.
//
// Solidity: function bid() payable returns()
func (_DomainVault *DomainVaultSession) Bid() (*types.Transaction, error) {
	return _DomainVault.Contract.Bid(&_DomainVault.TransactOpts)
}

// Bid is a paid mutator transaction binding the contract method 0x1998aeef.
//
// Solidity: function bid() payable returns()
func (_DomainVault *DomainVaultTransactorSession) Bid() (*types.Transaction, error) {
	return _DomainVault.Contract.Bid(&_DomainVault.TransactOpts)
}

// Initialize is a paid mutator transaction binding the contract method 0xb7093fc4.
//
// Solidity: function initialize(address domainERC20_, uint256 domainERC20TotalSupply_, address domainERC721_, uint256 domainERC721TokenId_, address domainSettings_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, address wrapperNativeToken_, address owner) returns()
func (_DomainVault *DomainVaultTransactor) Initialize(opts *bind.TransactOpts, domainERC20_ common.Address, domainERC20TotalSupply_ *big.Int, domainERC721_ common.Address, domainERC721TokenId_ *big.Int, domainSettings_ common.Address, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, wrapperNativeToken_ common.Address, owner common.Address) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "initialize", domainERC20_, domainERC20TotalSupply_, domainERC721_, domainERC721TokenId_, domainSettings_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, wrapperNativeToken_, owner)
}

// Initialize is a paid mutator transaction binding the contract method 0xb7093fc4.
//
// Solidity: function initialize(address domainERC20_, uint256 domainERC20TotalSupply_, address domainERC721_, uint256 domainERC721TokenId_, address domainSettings_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, address wrapperNativeToken_, address owner) returns()
func (_DomainVault *DomainVaultSession) Initialize(domainERC20_ common.Address, domainERC20TotalSupply_ *big.Int, domainERC721_ common.Address, domainERC721TokenId_ *big.Int, domainSettings_ common.Address, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, wrapperNativeToken_ common.Address, owner common.Address) (*types.Transaction, error) {
	return _DomainVault.Contract.Initialize(&_DomainVault.TransactOpts, domainERC20_, domainERC20TotalSupply_, domainERC721_, domainERC721TokenId_, domainSettings_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, wrapperNativeToken_, owner)
}

// Initialize is a paid mutator transaction binding the contract method 0xb7093fc4.
//
// Solidity: function initialize(address domainERC20_, uint256 domainERC20TotalSupply_, address domainERC721_, uint256 domainERC721TokenId_, address domainSettings_, uint256 reservePrice_, uint256 voteDuration_, uint256 votePercentage_, uint256 auctionDuration_, address wrapperNativeToken_, address owner) returns()
func (_DomainVault *DomainVaultTransactorSession) Initialize(domainERC20_ common.Address, domainERC20TotalSupply_ *big.Int, domainERC721_ common.Address, domainERC721TokenId_ *big.Int, domainSettings_ common.Address, reservePrice_ *big.Int, voteDuration_ *big.Int, votePercentage_ *big.Int, auctionDuration_ *big.Int, wrapperNativeToken_ common.Address, owner common.Address) (*types.Transaction, error) {
	return _DomainVault.Contract.Initialize(&_DomainVault.TransactOpts, domainERC20_, domainERC20TotalSupply_, domainERC721_, domainERC721TokenId_, domainSettings_, reservePrice_, voteDuration_, votePercentage_, auctionDuration_, wrapperNativeToken_, owner)
}

// MakeOfferBid is a paid mutator transaction binding the contract method 0x16ed7108.
//
// Solidity: function makeOfferBid() payable returns()
func (_DomainVault *DomainVaultTransactor) MakeOfferBid(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "makeOfferBid")
}

// MakeOfferBid is a paid mutator transaction binding the contract method 0x16ed7108.
//
// Solidity: function makeOfferBid() payable returns()
func (_DomainVault *DomainVaultSession) MakeOfferBid() (*types.Transaction, error) {
	return _DomainVault.Contract.MakeOfferBid(&_DomainVault.TransactOpts)
}

// MakeOfferBid is a paid mutator transaction binding the contract method 0x16ed7108.
//
// Solidity: function makeOfferBid() payable returns()
func (_DomainVault *DomainVaultTransactorSession) MakeOfferBid() (*types.Transaction, error) {
	return _DomainVault.Contract.MakeOfferBid(&_DomainVault.TransactOpts)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainVault *DomainVaultTransactor) OnERC721Received(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "onERC721Received", arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainVault *DomainVaultSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainVault.Contract.OnERC721Received(&_DomainVault.TransactOpts, arg0, arg1, arg2, arg3)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address , address , uint256 , bytes ) returns(bytes4)
func (_DomainVault *DomainVaultTransactorSession) OnERC721Received(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 []byte) (*types.Transaction, error) {
	return _DomainVault.Contract.OnERC721Received(&_DomainVault.TransactOpts, arg0, arg1, arg2, arg3)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainVault *DomainVaultTransactor) Pause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "pause")
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainVault *DomainVaultSession) Pause() (*types.Transaction, error) {
	return _DomainVault.Contract.Pause(&_DomainVault.TransactOpts)
}

// Pause is a paid mutator transaction binding the contract method 0x8456cb59.
//
// Solidity: function pause() returns()
func (_DomainVault *DomainVaultTransactorSession) Pause() (*types.Transaction, error) {
	return _DomainVault.Contract.Pause(&_DomainVault.TransactOpts)
}

// Redeem is a paid mutator transaction binding the contract method 0xbe040fb0.
//
// Solidity: function redeem() returns()
func (_DomainVault *DomainVaultTransactor) Redeem(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "redeem")
}

// Redeem is a paid mutator transaction binding the contract method 0xbe040fb0.
//
// Solidity: function redeem() returns()
func (_DomainVault *DomainVaultSession) Redeem() (*types.Transaction, error) {
	return _DomainVault.Contract.Redeem(&_DomainVault.TransactOpts)
}

// Redeem is a paid mutator transaction binding the contract method 0xbe040fb0.
//
// Solidity: function redeem() returns()
func (_DomainVault *DomainVaultTransactorSession) Redeem() (*types.Transaction, error) {
	return _DomainVault.Contract.Redeem(&_DomainVault.TransactOpts)
}

// RedeemWithPermit is a paid mutator transaction binding the contract method 0xff0ef5f8.
//
// Solidity: function redeemWithPermit(uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultTransactor) RedeemWithPermit(opts *bind.TransactOpts, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "redeemWithPermit", deadline, v, r, s)
}

// RedeemWithPermit is a paid mutator transaction binding the contract method 0xff0ef5f8.
//
// Solidity: function redeemWithPermit(uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultSession) RedeemWithPermit(deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.Contract.RedeemWithPermit(&_DomainVault.TransactOpts, deadline, v, r, s)
}

// RedeemWithPermit is a paid mutator transaction binding the contract method 0xff0ef5f8.
//
// Solidity: function redeemWithPermit(uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultTransactorSession) RedeemWithPermit(deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.Contract.RedeemWithPermit(&_DomainVault.TransactOpts, deadline, v, r, s)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainVault *DomainVaultTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainVault *DomainVaultSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainVault.Contract.RenounceOwnership(&_DomainVault.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_DomainVault *DomainVaultTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _DomainVault.Contract.RenounceOwnership(&_DomainVault.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainVault *DomainVaultTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainVault *DomainVaultSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainVault.Contract.TransferOwnership(&_DomainVault.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_DomainVault *DomainVaultTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _DomainVault.Contract.TransferOwnership(&_DomainVault.TransactOpts, newOwner)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainVault *DomainVaultTransactor) Unpause(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "unpause")
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainVault *DomainVaultSession) Unpause() (*types.Transaction, error) {
	return _DomainVault.Contract.Unpause(&_DomainVault.TransactOpts)
}

// Unpause is a paid mutator transaction binding the contract method 0x3f4ba83a.
//
// Solidity: function unpause() returns()
func (_DomainVault *DomainVaultTransactorSession) Unpause() (*types.Transaction, error) {
	return _DomainVault.Contract.Unpause(&_DomainVault.TransactOpts)
}

// Vote is a paid mutator transaction binding the contract method 0x632a9a52.
//
// Solidity: function vote() returns()
func (_DomainVault *DomainVaultTransactor) Vote(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "vote")
}

// Vote is a paid mutator transaction binding the contract method 0x632a9a52.
//
// Solidity: function vote() returns()
func (_DomainVault *DomainVaultSession) Vote() (*types.Transaction, error) {
	return _DomainVault.Contract.Vote(&_DomainVault.TransactOpts)
}

// Vote is a paid mutator transaction binding the contract method 0x632a9a52.
//
// Solidity: function vote() returns()
func (_DomainVault *DomainVaultTransactorSession) Vote() (*types.Transaction, error) {
	return _DomainVault.Contract.Vote(&_DomainVault.TransactOpts)
}

// WithdrawBidFund is a paid mutator transaction binding the contract method 0x466c22c7.
//
// Solidity: function withdrawBidFund() payable returns()
func (_DomainVault *DomainVaultTransactor) WithdrawBidFund(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "withdrawBidFund")
}

// WithdrawBidFund is a paid mutator transaction binding the contract method 0x466c22c7.
//
// Solidity: function withdrawBidFund() payable returns()
func (_DomainVault *DomainVaultSession) WithdrawBidFund() (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawBidFund(&_DomainVault.TransactOpts)
}

// WithdrawBidFund is a paid mutator transaction binding the contract method 0x466c22c7.
//
// Solidity: function withdrawBidFund() payable returns()
func (_DomainVault *DomainVaultTransactorSession) WithdrawBidFund() (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawBidFund(&_DomainVault.TransactOpts)
}

// WithdrawERC721 is a paid mutator transaction binding the contract method 0xa1446817.
//
// Solidity: function withdrawERC721() returns()
func (_DomainVault *DomainVaultTransactor) WithdrawERC721(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "withdrawERC721")
}

// WithdrawERC721 is a paid mutator transaction binding the contract method 0xa1446817.
//
// Solidity: function withdrawERC721() returns()
func (_DomainVault *DomainVaultSession) WithdrawERC721() (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawERC721(&_DomainVault.TransactOpts)
}

// WithdrawERC721 is a paid mutator transaction binding the contract method 0xa1446817.
//
// Solidity: function withdrawERC721() returns()
func (_DomainVault *DomainVaultTransactorSession) WithdrawERC721() (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawERC721(&_DomainVault.TransactOpts)
}

// WithdrawFund is a paid mutator transaction binding the contract method 0x0cee1725.
//
// Solidity: function withdrawFund(uint256 withdrawERC20Amount) returns()
func (_DomainVault *DomainVaultTransactor) WithdrawFund(opts *bind.TransactOpts, withdrawERC20Amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "withdrawFund", withdrawERC20Amount)
}

// WithdrawFund is a paid mutator transaction binding the contract method 0x0cee1725.
//
// Solidity: function withdrawFund(uint256 withdrawERC20Amount) returns()
func (_DomainVault *DomainVaultSession) WithdrawFund(withdrawERC20Amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawFund(&_DomainVault.TransactOpts, withdrawERC20Amount)
}

// WithdrawFund is a paid mutator transaction binding the contract method 0x0cee1725.
//
// Solidity: function withdrawFund(uint256 withdrawERC20Amount) returns()
func (_DomainVault *DomainVaultTransactorSession) WithdrawFund(withdrawERC20Amount *big.Int) (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawFund(&_DomainVault.TransactOpts, withdrawERC20Amount)
}

// WithdrawFundWithPermit is a paid mutator transaction binding the contract method 0x0475bb94.
//
// Solidity: function withdrawFundWithPermit(uint256 withdrawERC20Amount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultTransactor) WithdrawFundWithPermit(opts *bind.TransactOpts, withdrawERC20Amount *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.contract.Transact(opts, "withdrawFundWithPermit", withdrawERC20Amount, deadline, v, r, s)
}

// WithdrawFundWithPermit is a paid mutator transaction binding the contract method 0x0475bb94.
//
// Solidity: function withdrawFundWithPermit(uint256 withdrawERC20Amount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultSession) WithdrawFundWithPermit(withdrawERC20Amount *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawFundWithPermit(&_DomainVault.TransactOpts, withdrawERC20Amount, deadline, v, r, s)
}

// WithdrawFundWithPermit is a paid mutator transaction binding the contract method 0x0475bb94.
//
// Solidity: function withdrawFundWithPermit(uint256 withdrawERC20Amount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) returns()
func (_DomainVault *DomainVaultTransactorSession) WithdrawFundWithPermit(withdrawERC20Amount *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _DomainVault.Contract.WithdrawFundWithPermit(&_DomainVault.TransactOpts, withdrawERC20Amount, deadline, v, r, s)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainVault *DomainVaultTransactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DomainVault.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainVault *DomainVaultSession) Receive() (*types.Transaction, error) {
	return _DomainVault.Contract.Receive(&_DomainVault.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_DomainVault *DomainVaultTransactorSession) Receive() (*types.Transaction, error) {
	return _DomainVault.Contract.Receive(&_DomainVault.TransactOpts)
}

// DomainVaultBidIterator is returned from FilterBid and is used to iterate over the raw logs and unpacked data for Bid events raised by the DomainVault contract.
type DomainVaultBidIterator struct {
	Event *DomainVaultBid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultBidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultBid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultBid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultBidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultBidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultBid represents a Bid event raised by the DomainVault contract.
type DomainVaultBid struct {
	Bidder   common.Address
	BidPrice *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterBid is a free log retrieval operation binding the contract event 0xe684a55f31b79eca403df938249029212a5925ec6be8012e099b45bc1019e5d2.
//
// Solidity: event Bid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) FilterBid(opts *bind.FilterOpts, bidder []common.Address) (*DomainVaultBidIterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "Bid", bidderRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultBidIterator{contract: _DomainVault.contract, event: "Bid", logs: logs, sub: sub}, nil
}

// WatchBid is a free log subscription operation binding the contract event 0xe684a55f31b79eca403df938249029212a5925ec6be8012e099b45bc1019e5d2.
//
// Solidity: event Bid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) WatchBid(opts *bind.WatchOpts, sink chan<- *DomainVaultBid, bidder []common.Address) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "Bid", bidderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultBid)
				if err := _DomainVault.contract.UnpackLog(event, "Bid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBid is a log parse operation binding the contract event 0xe684a55f31b79eca403df938249029212a5925ec6be8012e099b45bc1019e5d2.
//
// Solidity: event Bid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) ParseBid(log types.Log) (*DomainVaultBid, error) {
	event := new(DomainVaultBid)
	if err := _DomainVault.contract.UnpackLog(event, "Bid", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultMakeOfferBidIterator is returned from FilterMakeOfferBid and is used to iterate over the raw logs and unpacked data for MakeOfferBid events raised by the DomainVault contract.
type DomainVaultMakeOfferBidIterator struct {
	Event *DomainVaultMakeOfferBid // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultMakeOfferBidIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultMakeOfferBid)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultMakeOfferBid)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultMakeOfferBidIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultMakeOfferBidIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultMakeOfferBid represents a MakeOfferBid event raised by the DomainVault contract.
type DomainVaultMakeOfferBid struct {
	Bidder   common.Address
	BidPrice *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterMakeOfferBid is a free log retrieval operation binding the contract event 0x009d878c75bc73c4b7a7c4a5b35906c6cfd5c91cb737fa07789e2dcc5ce738df.
//
// Solidity: event MakeOfferBid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) FilterMakeOfferBid(opts *bind.FilterOpts, bidder []common.Address) (*DomainVaultMakeOfferBidIterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "MakeOfferBid", bidderRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultMakeOfferBidIterator{contract: _DomainVault.contract, event: "MakeOfferBid", logs: logs, sub: sub}, nil
}

// WatchMakeOfferBid is a free log subscription operation binding the contract event 0x009d878c75bc73c4b7a7c4a5b35906c6cfd5c91cb737fa07789e2dcc5ce738df.
//
// Solidity: event MakeOfferBid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) WatchMakeOfferBid(opts *bind.WatchOpts, sink chan<- *DomainVaultMakeOfferBid, bidder []common.Address) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "MakeOfferBid", bidderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultMakeOfferBid)
				if err := _DomainVault.contract.UnpackLog(event, "MakeOfferBid", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseMakeOfferBid is a log parse operation binding the contract event 0x009d878c75bc73c4b7a7c4a5b35906c6cfd5c91cb737fa07789e2dcc5ce738df.
//
// Solidity: event MakeOfferBid(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) ParseMakeOfferBid(log types.Log) (*DomainVaultMakeOfferBid, error) {
	event := new(DomainVaultMakeOfferBid)
	if err := _DomainVault.contract.UnpackLog(event, "MakeOfferBid", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the DomainVault contract.
type DomainVaultOwnershipTransferredIterator struct {
	Event *DomainVaultOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultOwnershipTransferred represents a OwnershipTransferred event raised by the DomainVault contract.
type DomainVaultOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainVault *DomainVaultFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*DomainVaultOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultOwnershipTransferredIterator{contract: _DomainVault.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainVault *DomainVaultFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *DomainVaultOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultOwnershipTransferred)
				if err := _DomainVault.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_DomainVault *DomainVaultFilterer) ParseOwnershipTransferred(log types.Log) (*DomainVaultOwnershipTransferred, error) {
	event := new(DomainVaultOwnershipTransferred)
	if err := _DomainVault.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultPausedIterator is returned from FilterPaused and is used to iterate over the raw logs and unpacked data for Paused events raised by the DomainVault contract.
type DomainVaultPausedIterator struct {
	Event *DomainVaultPaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultPausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultPaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultPaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultPausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultPausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultPaused represents a Paused event raised by the DomainVault contract.
type DomainVaultPaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterPaused is a free log retrieval operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainVault *DomainVaultFilterer) FilterPaused(opts *bind.FilterOpts) (*DomainVaultPausedIterator, error) {

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return &DomainVaultPausedIterator{contract: _DomainVault.contract, event: "Paused", logs: logs, sub: sub}, nil
}

// WatchPaused is a free log subscription operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainVault *DomainVaultFilterer) WatchPaused(opts *bind.WatchOpts, sink chan<- *DomainVaultPaused) (event.Subscription, error) {

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "Paused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultPaused)
				if err := _DomainVault.contract.UnpackLog(event, "Paused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePaused is a log parse operation binding the contract event 0x62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a258.
//
// Solidity: event Paused(address account)
func (_DomainVault *DomainVaultFilterer) ParsePaused(log types.Log) (*DomainVaultPaused, error) {
	event := new(DomainVaultPaused)
	if err := _DomainVault.contract.UnpackLog(event, "Paused", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultRedeemIterator is returned from FilterRedeem and is used to iterate over the raw logs and unpacked data for Redeem events raised by the DomainVault contract.
type DomainVaultRedeemIterator struct {
	Event *DomainVaultRedeem // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultRedeemIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultRedeem)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultRedeem)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultRedeemIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultRedeemIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultRedeem represents a Redeem event raised by the DomainVault contract.
type DomainVaultRedeem struct {
	Owner common.Address
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterRedeem is a free log retrieval operation binding the contract event 0xd1b5ea7fe0f1c2fa09d49c2aa9b2200664ba57a734f1d95481d95b7f99af991c.
//
// Solidity: event Redeem(address indexed owner)
func (_DomainVault *DomainVaultFilterer) FilterRedeem(opts *bind.FilterOpts, owner []common.Address) (*DomainVaultRedeemIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "Redeem", ownerRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultRedeemIterator{contract: _DomainVault.contract, event: "Redeem", logs: logs, sub: sub}, nil
}

// WatchRedeem is a free log subscription operation binding the contract event 0xd1b5ea7fe0f1c2fa09d49c2aa9b2200664ba57a734f1d95481d95b7f99af991c.
//
// Solidity: event Redeem(address indexed owner)
func (_DomainVault *DomainVaultFilterer) WatchRedeem(opts *bind.WatchOpts, sink chan<- *DomainVaultRedeem, owner []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "Redeem", ownerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultRedeem)
				if err := _DomainVault.contract.UnpackLog(event, "Redeem", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRedeem is a log parse operation binding the contract event 0xd1b5ea7fe0f1c2fa09d49c2aa9b2200664ba57a734f1d95481d95b7f99af991c.
//
// Solidity: event Redeem(address indexed owner)
func (_DomainVault *DomainVaultFilterer) ParseRedeem(log types.Log) (*DomainVaultRedeem, error) {
	event := new(DomainVaultRedeem)
	if err := _DomainVault.contract.UnpackLog(event, "Redeem", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultUnpausedIterator is returned from FilterUnpaused and is used to iterate over the raw logs and unpacked data for Unpaused events raised by the DomainVault contract.
type DomainVaultUnpausedIterator struct {
	Event *DomainVaultUnpaused // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultUnpausedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultUnpaused)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultUnpaused)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultUnpausedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultUnpausedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultUnpaused represents a Unpaused event raised by the DomainVault contract.
type DomainVaultUnpaused struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterUnpaused is a free log retrieval operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainVault *DomainVaultFilterer) FilterUnpaused(opts *bind.FilterOpts) (*DomainVaultUnpausedIterator, error) {

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return &DomainVaultUnpausedIterator{contract: _DomainVault.contract, event: "Unpaused", logs: logs, sub: sub}, nil
}

// WatchUnpaused is a free log subscription operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainVault *DomainVaultFilterer) WatchUnpaused(opts *bind.WatchOpts, sink chan<- *DomainVaultUnpaused) (event.Subscription, error) {

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "Unpaused")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultUnpaused)
				if err := _DomainVault.contract.UnpackLog(event, "Unpaused", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnpaused is a log parse operation binding the contract event 0x5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa.
//
// Solidity: event Unpaused(address account)
func (_DomainVault *DomainVaultFilterer) ParseUnpaused(log types.Log) (*DomainVaultUnpaused, error) {
	event := new(DomainVaultUnpaused)
	if err := _DomainVault.contract.UnpackLog(event, "Unpaused", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultVoteIterator is returned from FilterVote and is used to iterate over the raw logs and unpacked data for Vote events raised by the DomainVault contract.
type DomainVaultVoteIterator struct {
	Event *DomainVaultVote // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultVoteIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultVote)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultVote)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultVoteIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultVoteIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultVote represents a Vote event raised by the DomainVault contract.
type DomainVaultVote struct {
	Voter common.Address
	Votes *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterVote is a free log retrieval operation binding the contract event 0xf668ead05c744b9178e571d2edb452e72baf6529c8d72160e64e59b50d865bd0.
//
// Solidity: event Vote(address indexed voter, uint256 votes)
func (_DomainVault *DomainVaultFilterer) FilterVote(opts *bind.FilterOpts, voter []common.Address) (*DomainVaultVoteIterator, error) {

	var voterRule []interface{}
	for _, voterItem := range voter {
		voterRule = append(voterRule, voterItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "Vote", voterRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultVoteIterator{contract: _DomainVault.contract, event: "Vote", logs: logs, sub: sub}, nil
}

// WatchVote is a free log subscription operation binding the contract event 0xf668ead05c744b9178e571d2edb452e72baf6529c8d72160e64e59b50d865bd0.
//
// Solidity: event Vote(address indexed voter, uint256 votes)
func (_DomainVault *DomainVaultFilterer) WatchVote(opts *bind.WatchOpts, sink chan<- *DomainVaultVote, voter []common.Address) (event.Subscription, error) {

	var voterRule []interface{}
	for _, voterItem := range voter {
		voterRule = append(voterRule, voterItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "Vote", voterRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultVote)
				if err := _DomainVault.contract.UnpackLog(event, "Vote", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseVote is a log parse operation binding the contract event 0xf668ead05c744b9178e571d2edb452e72baf6529c8d72160e64e59b50d865bd0.
//
// Solidity: event Vote(address indexed voter, uint256 votes)
func (_DomainVault *DomainVaultFilterer) ParseVote(log types.Log) (*DomainVaultVote, error) {
	event := new(DomainVaultVote)
	if err := _DomainVault.contract.UnpackLog(event, "Vote", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultWithdrawBidFundIterator is returned from FilterWithdrawBidFund and is used to iterate over the raw logs and unpacked data for WithdrawBidFund events raised by the DomainVault contract.
type DomainVaultWithdrawBidFundIterator struct {
	Event *DomainVaultWithdrawBidFund // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultWithdrawBidFundIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultWithdrawBidFund)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultWithdrawBidFund)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultWithdrawBidFundIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultWithdrawBidFundIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultWithdrawBidFund represents a WithdrawBidFund event raised by the DomainVault contract.
type DomainVaultWithdrawBidFund struct {
	Bidder   common.Address
	BidPrice *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterWithdrawBidFund is a free log retrieval operation binding the contract event 0xebe8082ddb1a7250c6922d8d3188b31905726c5d4a028d71a400e2bb0f8177f9.
//
// Solidity: event WithdrawBidFund(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) FilterWithdrawBidFund(opts *bind.FilterOpts, bidder []common.Address) (*DomainVaultWithdrawBidFundIterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "WithdrawBidFund", bidderRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultWithdrawBidFundIterator{contract: _DomainVault.contract, event: "WithdrawBidFund", logs: logs, sub: sub}, nil
}

// WatchWithdrawBidFund is a free log subscription operation binding the contract event 0xebe8082ddb1a7250c6922d8d3188b31905726c5d4a028d71a400e2bb0f8177f9.
//
// Solidity: event WithdrawBidFund(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) WatchWithdrawBidFund(opts *bind.WatchOpts, sink chan<- *DomainVaultWithdrawBidFund, bidder []common.Address) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "WithdrawBidFund", bidderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultWithdrawBidFund)
				if err := _DomainVault.contract.UnpackLog(event, "WithdrawBidFund", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWithdrawBidFund is a log parse operation binding the contract event 0xebe8082ddb1a7250c6922d8d3188b31905726c5d4a028d71a400e2bb0f8177f9.
//
// Solidity: event WithdrawBidFund(address indexed bidder, uint256 bidPrice)
func (_DomainVault *DomainVaultFilterer) ParseWithdrawBidFund(log types.Log) (*DomainVaultWithdrawBidFund, error) {
	event := new(DomainVaultWithdrawBidFund)
	if err := _DomainVault.contract.UnpackLog(event, "WithdrawBidFund", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultWithdrawERC721Iterator is returned from FilterWithdrawERC721 and is used to iterate over the raw logs and unpacked data for WithdrawERC721 events raised by the DomainVault contract.
type DomainVaultWithdrawERC721Iterator struct {
	Event *DomainVaultWithdrawERC721 // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultWithdrawERC721Iterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultWithdrawERC721)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultWithdrawERC721)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultWithdrawERC721Iterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultWithdrawERC721Iterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultWithdrawERC721 represents a WithdrawERC721 event raised by the DomainVault contract.
type DomainVaultWithdrawERC721 struct {
	Bidder   common.Address
	BidPrice *big.Int
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterWithdrawERC721 is a free log retrieval operation binding the contract event 0x3bef01a072a1ecbad87afff2d6e4629df7a37805ddc4572aad8f62c7c2ad52f3.
//
// Solidity: event WithdrawERC721(address indexed bidder, uint256 bidPrice, uint256 tokenId)
func (_DomainVault *DomainVaultFilterer) FilterWithdrawERC721(opts *bind.FilterOpts, bidder []common.Address) (*DomainVaultWithdrawERC721Iterator, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "WithdrawERC721", bidderRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultWithdrawERC721Iterator{contract: _DomainVault.contract, event: "WithdrawERC721", logs: logs, sub: sub}, nil
}

// WatchWithdrawERC721 is a free log subscription operation binding the contract event 0x3bef01a072a1ecbad87afff2d6e4629df7a37805ddc4572aad8f62c7c2ad52f3.
//
// Solidity: event WithdrawERC721(address indexed bidder, uint256 bidPrice, uint256 tokenId)
func (_DomainVault *DomainVaultFilterer) WatchWithdrawERC721(opts *bind.WatchOpts, sink chan<- *DomainVaultWithdrawERC721, bidder []common.Address) (event.Subscription, error) {

	var bidderRule []interface{}
	for _, bidderItem := range bidder {
		bidderRule = append(bidderRule, bidderItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "WithdrawERC721", bidderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultWithdrawERC721)
				if err := _DomainVault.contract.UnpackLog(event, "WithdrawERC721", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWithdrawERC721 is a log parse operation binding the contract event 0x3bef01a072a1ecbad87afff2d6e4629df7a37805ddc4572aad8f62c7c2ad52f3.
//
// Solidity: event WithdrawERC721(address indexed bidder, uint256 bidPrice, uint256 tokenId)
func (_DomainVault *DomainVaultFilterer) ParseWithdrawERC721(log types.Log) (*DomainVaultWithdrawERC721, error) {
	event := new(DomainVaultWithdrawERC721)
	if err := _DomainVault.contract.UnpackLog(event, "WithdrawERC721", log); err != nil {
		return nil, err
	}
	return event, nil
}

// DomainVaultWithdrawFundIterator is returned from FilterWithdrawFund and is used to iterate over the raw logs and unpacked data for WithdrawFund events raised by the DomainVault contract.
type DomainVaultWithdrawFundIterator struct {
	Event *DomainVaultWithdrawFund // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *DomainVaultWithdrawFundIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(DomainVaultWithdrawFund)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(DomainVaultWithdrawFund)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *DomainVaultWithdrawFundIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *DomainVaultWithdrawFundIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// DomainVaultWithdrawFund represents a WithdrawFund event raised by the DomainVault contract.
type DomainVaultWithdrawFund struct {
	Erc20Owner          common.Address
	WithdrawERC20Amount *big.Int
	FundsShareAmount    *big.Int
	Raw                 types.Log // Blockchain specific contextual infos
}

// FilterWithdrawFund is a free log retrieval operation binding the contract event 0x25c3183b4fe0218880477406c1f1ba98d3e25acb8efa68ef4a900982a8cea38a.
//
// Solidity: event WithdrawFund(address indexed erc20Owner, uint256 withdrawERC20Amount, uint256 fundsShareAmount)
func (_DomainVault *DomainVaultFilterer) FilterWithdrawFund(opts *bind.FilterOpts, erc20Owner []common.Address) (*DomainVaultWithdrawFundIterator, error) {

	var erc20OwnerRule []interface{}
	for _, erc20OwnerItem := range erc20Owner {
		erc20OwnerRule = append(erc20OwnerRule, erc20OwnerItem)
	}

	logs, sub, err := _DomainVault.contract.FilterLogs(opts, "WithdrawFund", erc20OwnerRule)
	if err != nil {
		return nil, err
	}
	return &DomainVaultWithdrawFundIterator{contract: _DomainVault.contract, event: "WithdrawFund", logs: logs, sub: sub}, nil
}

// WatchWithdrawFund is a free log subscription operation binding the contract event 0x25c3183b4fe0218880477406c1f1ba98d3e25acb8efa68ef4a900982a8cea38a.
//
// Solidity: event WithdrawFund(address indexed erc20Owner, uint256 withdrawERC20Amount, uint256 fundsShareAmount)
func (_DomainVault *DomainVaultFilterer) WatchWithdrawFund(opts *bind.WatchOpts, sink chan<- *DomainVaultWithdrawFund, erc20Owner []common.Address) (event.Subscription, error) {

	var erc20OwnerRule []interface{}
	for _, erc20OwnerItem := range erc20Owner {
		erc20OwnerRule = append(erc20OwnerRule, erc20OwnerItem)
	}

	logs, sub, err := _DomainVault.contract.WatchLogs(opts, "WithdrawFund", erc20OwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(DomainVaultWithdrawFund)
				if err := _DomainVault.contract.UnpackLog(event, "WithdrawFund", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseWithdrawFund is a log parse operation binding the contract event 0x25c3183b4fe0218880477406c1f1ba98d3e25acb8efa68ef4a900982a8cea38a.
//
// Solidity: event WithdrawFund(address indexed erc20Owner, uint256 withdrawERC20Amount, uint256 fundsShareAmount)
func (_DomainVault *DomainVaultFilterer) ParseWithdrawFund(log types.Log) (*DomainVaultWithdrawFund, error) {
	event := new(DomainVaultWithdrawFund)
	if err := _DomainVault.contract.UnpackLog(event, "WithdrawFund", log); err != nil {
		return nil, err
	}
	return event, nil
}
