package conf

// Config 配置
type Config struct {
	Enabled bool `yaml:"enabled"` //是否启用
}

var DefaultConfig = Config{
	Enabled: true,
}
