package store

import (
	"context"
	"github.com/jordan-wright/email"
	"net/smtp"
	"time"
)

type MailEventBO struct {
	TxHash  string `db:"tx_hash"`
	Domain  string `db:"domain"`
	TokenId int    `db:"token_id"`
	Event   string `db:"type"`
}

type MailTextBO struct {
	MailId      int    `db:"mail_id"`
	NftId       int    `db:"nft_id"`
	Event       string `db:"event"`
	FromAddress string `db:"from_address"`
	ToAddress   string `db:"to_address"`
	TxHash      string `db:"tx_hash"`
}

type MailLogBO struct {
	NftId                int    `db:"nft_id"`
	Event                string `db:"event"`
	SendToAccountAddress string `db:"send_to_account_address"`
	SendToMailAddress    string `db:"send_to_mail_address"`
}

const (
	QueryMailEventListSQL = `
		SELECT 
			COALESCE(tx_hash,'') AS tx_hash,
			COALESCE(domain,'') AS domain,
			COALESCE(token_id,'') AS token_id,
			COALESCE(type,'') AS type
		FROM 
			t_transactions
		WHERE 
			mail_send_status = 0`
	QueryMailSQL         = `SELECT email FROM t_account WHERE account_address=?`
	QueryMailTextSQL     = `SELECT text FROM t_mail WHERE mail_type=?`
	QueryAccountAddress  = `SELECT curator_address FROM t_nft WHERE token_id=?`
	InsertMailLogSQL     = `INSERT INTO t_mail_log(domain,token_id, event,tx_hash, send_to_mail_address, text, date) VALUES (:domain,:token_id,:event,:tx_hash,:send_to_mail_address,:text,:date)`
	UpdateMailSendStatus = `UPDATE t_transactions SET mail_send_status=1 WHERE token_id=:token_id AND tx_hash=:tx_hash`
)

func (s *store) QueryMailEventList(ctx context.Context) ([]MailEventBO, error) {
	var (
		MailEventList []MailEventBO
		err1          error
	)
	err0 := s.DataBase.Dao.Select(&MailEventList, QueryMailEventListSQL)
	if err0 == nil && err1 == nil {
		if MailEventList != nil && len(MailEventList) > 0 {
			return MailEventList, nil
		} else {
			return MailEventList, nil
		}
	} else {
		return MailEventList, err0
	}
	return MailEventList, nil
}

func (s *store) QueryMailAddress(tokenId int, txHash string) (string, error) {
	var (
		mailAddress    string
		accountAddress string
		err0           = s.DataBase.Dao.QueryRow(QueryAccountAddress, tokenId).Scan(&accountAddress)
		err1           = s.DataBase.Dao.QueryRow(QueryMailSQL, accountAddress).Scan(&mailAddress)
	)
	if err1 != nil || err0 != nil {
		return "", err1
	}
	return mailAddress, nil
}

func (s *store) SendMailToMailAddress(mailType string, mailAddress string, list MailEventBO, text string) error {
	var (
		e = email.NewEmail()
	)
	e.From = "NFT.IO <shiina96@163.com>"
	e.To = []string{mailAddress}
	e.Subject = mailType
	e.Text = []byte(text)
	err := e.Send("smtp.163.com:25", smtp.PlainAuth("", "shiina96@163.com", "ZJGSKHWCUQHFAQVU", "smtp.163.com"))
	if err != nil {
		log.Fatal(err)
	} else {
		_, err = s.DataBase.Dao.NamedExec(InsertMailLogSQL, map[string]interface{}{
			"domain":               list.Domain,
			"token_id":             list.TokenId,
			"event":                list.Event,
			"tx_hash":              list.TxHash,
			"send_to_mail_address": mailAddress,
			"text":                 text,
			"date":                 time.Now(),
		})
	}
	if err == nil {
		_, err = s.DataBase.Dao.NamedExec(UpdateMailSendStatus, map[string]interface{}{
			"token_id": list.TokenId,
			"tx_hash":  list.TxHash,
		})
	} else {
		return err
	}
	return nil
}
