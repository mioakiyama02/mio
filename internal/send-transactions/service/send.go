package service

import (
	"context"
	"domain/internal/send-transactions/store"
	"fmt"
)

type SendService struct {
	Service
}

func NewSendService(srv Service) *SendService {
	return &SendService{
		Service: srv,
	}
}

func (srv *SendService) SendMail(ctx context.Context) error {
	var (
		list, err = srv.QueryMailEventList(ctx)
	)
	if err != nil {
		return err
	}
	for i := 0; i < len(list); i++ {
		switch list[i].Event {
		case "MintNFT":
		case "ListNFT":
		case "CancelListNFT":
		case "BuyNFT":
		case "OfferNFT":
			mailAddress, err0 := srv.store.QueryMailAddress(list[i].TokenId, list[i].TxHash)
			if err0 == nil {
				text := fmt.Sprintf("有人对你的NFT出价了。\nNFT_ID:%d， \n域名:%s。", list[i].TokenId, list[i].Domain)
				srv.store.SendMailToMailAddress("MakeOfferForERC721", mailAddress, list[i], text)
			}
		case "OfferToSellNFT":
		case "TransferNFT":
		case "ListPreSaleFragment":
		case "WithdrawPreSaleFragment":
		case "TT10":
		case "MintVault":
		case "MakeOffer":
		case "Vote":
		case "Bid":
		case "Redeem":
		}
	}
	return nil
}

func (srv *SendService) QueryMailEventList(ctx context.Context) ([]store.MailEventBO, error) {
	return srv.store.QueryMailEventList(ctx)
}
