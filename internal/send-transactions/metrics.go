package sendTransactions

import (
	"domain/internal/metrics"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
	"time"
)

// Metric descriptors.
var (
	durations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:  "domain",
			Subsystem:  "trace",
			Name:       "durations_seconds",
			Help:       "trace latency distributions.",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"method"},
	)
)

func init() {
	prometheus.MustRegister(durations)
}

func withMetric(h http.HandlerFunc) http.HandlerFunc {
	if metrics.Enabled {
		return func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			h(w, r)
			durations.WithLabelValues(r.URL.Path).Observe(time.Since(start).Seconds())
		}
	} else {
		return h
	}
}
