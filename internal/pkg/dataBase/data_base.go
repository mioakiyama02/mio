package dataBase

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"strings"
	"time"
)

type Config struct {
	DataSource            string        `yaml:"data-source"` //数据库链接字符串，eg: pool:pool123@tcp(172.16.2.154)/pool
	MaxIdleConnections    int           `yaml:"maxIdleConnections"`
	MaxOpenConnections    int           `yaml:"maxOpenConnections"`
	MaxConnectionLifeTime time.Duration `yaml:"maxConnectionLifeTime"`
	BatchSize             int           `yaml:"batchSize"` //批量操作的批次大小
}

var DefaultConfig = Config{
	DataSource:            "root:root@tcp(127.0.0.1)/domain",
	MaxIdleConnections:    50,
	MaxOpenConnections:    100,
	MaxConnectionLifeTime: time.Duration(10) * time.Second,

	BatchSize: 500,
}

type DataBase struct {
	Config *Config
	Dao    *sqlx.DB
}

func New(cfg *Config) *DataBase {
	db := sqlx.MustConnect("mysql", cfg.DataSource)
	// SetMaxOpenConns sets the maximum number of open connections to the database.
	db.SetMaxOpenConns(cfg.MaxOpenConnections)
	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	db.SetConnMaxLifetime(cfg.MaxConnectionLifeTime)
	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	db.SetMaxIdleConns(cfg.MaxIdleConnections)

	err := db.Ping()
	if err != nil {
		log.Panic("Can't establish connection to mysql: ", err)
	} else {
		log.Infof("Ping to mysql[%s] ok!", cfg.DataSource)
	}

	for _, schema := range schemaSQL {
		sqls := strings.Split(schema, ";")
		for _, q := range sqls {
			q = strings.TrimSpace(q)
			if len(q) == 0 {
				continue
			}
			_, err := db.Exec(q)
			log.Info("SQL:", q)
			if err != nil {
				log.Warn(err)
				break
			}
		}

	}

	log.Info("init schema success!")

	return &DataBase{
		Config: cfg,
		Dao:    db,
	}

}

func (client *DataBase) Close() error {
	return client.Dao.Close()
}

func (client DataBase) BindBatchInsertOrUpdate(insertSQL, onDuplicateKeyUpdateSql string, arg interface{}) (string, []interface{}, error) {
	q, args, err := sqlx.Named(insertSQL, arg)
	return strings.Join([]string{q, onDuplicateKeyUpdateSql}, " "), args, err
}

func (client *DataBase) ExecBatchTx(tx *sqlx.Tx, insertSQL, onDuplicateKeyUpdateSql string, arg interface{}) (int64, error) {
	q, args, err := client.BindBatchInsertOrUpdate(insertSQL, onDuplicateKeyUpdateSql, arg)
	if err != nil {
		return 0, err
	}

	result, err := tx.Exec(q, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}

func (client *DataBase) ExecBatch(insertSQL, onDuplicateKeyUpdateSql string, arg interface{}) (int64, error) {
	q, args, err := client.BindBatchInsertOrUpdate(insertSQL, onDuplicateKeyUpdateSql, arg)
	if err != nil {
		return 0, err
	}

	result, err := client.Dao.Exec(q, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}

func (client *DataBase) WithTx(f func(tx *sqlx.Tx) (int64, error)) (int64, error) {
	tx, err := client.Dao.Beginx()
	if err != nil {
		return 0, errors.WithMessage(err, "!Begin transaction")
	}
	defer func(tx *sqlx.Tx) {
		err := tx.Rollback()
		if err != nil && !errors.Is(err, sql.ErrTxDone) {
			log.Error(err)
		}
	}(tx)

	rows, err := f(tx)
	if err != nil {
		return 0, err
	}

	err = tx.Commit()
	if err != nil {
		return 0, errors.WithMessage(err, "!Commit transaction")
	}

	return rows, nil
}
