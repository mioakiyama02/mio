package dataBase

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"strings"
	"testing"
)

var schema = `
CREATE  TABLE if not exists person (
    first_name text,
    last_name text,
    email text
);
`

type Person struct {
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
	Email     string
}

type Place struct {
	Country string
	City    sql.NullString
	TelCode int
}

func Test_SQLX(t *testing.T) {
	// this Pings the database trying to connect
	// use sqlx.Open() for sql.Open() semantics
	db, err := sqlx.Connect("mysql", "pool:pool123@tcp(172.16.2.154)/pool")
	if err != nil {
		log.Fatal(err)
	}

	// exec the schema or fail; multi-statement Exec behavior varies between
	// database drivers;  pq will exec them all, sqlite3 won't, ymmv
	db.MustExec(schema)

	// batch insert

	// batch insert with structs
	var personStructs []Person
	personStructs = append(personStructs, Person{FirstName: "Ardie", LastName: "Savea", Email: "asavea@ab.co.nz"})
	personStructs = append(personStructs, Person{FirstName: "Sonny Bill", LastName: "Williams", Email: "sbw@ab.co.nz"})
	personStructs = append(personStructs, Person{FirstName: "Ngani", LastName: "Laumape", Email: "nlaumape@ab.co.nz"})
	//personStructs := []Person{
	//	{FirstName: "Ardie", LastName: "Savea", Email: "asavea@ab.co.nz"},
	//	{FirstName: "Sonny Bill", LastName: "Williams", Email: "sbw@ab.co.nz"},
	//	{FirstName: "Ngani", LastName: "Laumape", Email: "nlaumape@ab.co.nz"},
	//}

	_, err = db.NamedExec(`INSERT INTO person (first_name, last_name, email)
        VALUES (:first_name, :last_name, :email)`, personStructs)

	if err != nil {
		t.Error("Failed to write share stats to mysql:", err)
		return
	}

	// batch insert with maps
	personMaps := []map[string]interface{}{
		{"first_name": "Ardie", "last_name": "Savea", "email": "asavea@ab.co.nz"},
		{"first_name": "Sonny Bill", "last_name": "Williams", "email": "sbw@ab.co.nz"},
		{"first_name": "Ngani", "last_name": "Laumape", "email": "nlaumape@ab.co.nz"},
	}

	_, err = db.NamedExec(`INSERT INTO person (first_name, last_name, email)
        VALUES (:first_name, :last_name, :email)`, personMaps)
}

func Test_sqlx_In(t *testing.T) {
	type tr struct {
		q    string
		args []interface{}
		c    int
	}
	tests := []tr{
		{"SELECT * FROM foo WHERE x = ? AND v in (?) AND y = ?",
			[]interface{}{"foo", []int{0, 5, 7, 2, 9}, "bar"},
			7},
		{"SELECT * FROM foo WHERE x in (?)",
			[]interface{}{[]int{1, 2, 3, 4, 5, 6, 7, 8}},
			8},
	}
	for _, test := range tests {
		q, a, err := sqlx.In(test.q, test.args...)
		if err != nil {
			t.Error(err)
		}
		if len(a) != test.c {
			t.Errorf("Expected %d args, but got %d (%+v)", test.c, len(a), a)
		}
		if strings.Count(q, "?") != test.c {
			t.Errorf("Expected %d bindVars, got %d", test.c, strings.Count(q, "?"))
		}
	}
}
