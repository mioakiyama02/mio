package cache

import (
	"context"
	"domain/app/server"
	"github.com/go-redis/redis/v8"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	// redis keys
	ServerKey = "server" //redis 前缀

	TokenExpireTime = time.Hour * 24
	//TokenExpireTime    = time.Second * 10
)

type Config struct {
	Addr      string `yaml:"endpoint"` //Redis 访问地址 ip:port
	Username  string `yaml:"username"` //访问密码
	Password  string `yaml:"password"` //访问密码
	Database  int    `yaml:"database"` //数据库序号
	PoolSize  int    `yaml:"poolSize"` //连接池大小
	Prefix    string `yaml:"prefix"`   //默认前缀
	ScanCount int64  `yaml:"scanSize"` //分页扫描页的大小
}

// DefaultConfig 默认配置
var DefaultConfig = Config{
	Addr:      "localhost:6379",
	Password:  "",
	Database:  0,
	PoolSize:  1,
	Prefix:    "domain",
	ScanCount: 1000,
}

type Redis struct {
	Cfg    *Config
	Client *redis.Client
	prefix string
}

func New(cfg *Config) *Redis {
	client := redis.NewClient(
		&redis.Options{
			Addr: cfg.Addr,
			//Username: cfg.Username,
			Password: cfg.Password,
			DB:       cfg.Database,
			PoolSize: cfg.PoolSize,
		},
	)

	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		log.Panic("Can't establish connection to redis: ", err)
	} else {
		log.Infof("Redis[%s/%d] check reply: %s", cfg.Addr, cfg.Database, pong)
	}

	return &Redis{Cfg: cfg, Client: client, prefix: cfg.Prefix}
}

func (r *Redis) Close() error {
	return r.Client.Close()
}

type registerServer struct {
	name string
	rds  *Redis
	t    *time.Timer
}

func (s registerServer) Start(ctx context.Context) error {
	interval := time.Second * 10
	srv := s.name
	s.t = time.NewTimer(interval)

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()

		case <-s.t.C:
			if _, err := s.rds.registerServer(ctx, srv, time.Now(), interval*2); err != nil {
				log.Errorf("register %s fail! %v", srv, err)
			}
			s.t.Reset(interval)
		}
	}
}

func (s registerServer) Stop(ctx context.Context) error {
	if s.t != nil {
		s.t.Stop()
	}
	return nil
}

// RegisterServer 注册服务
func (r *Redis) RegisterServer(srv string) server.Server {
	return &registerServer{
		name: srv,
		rds:  r,
	}
}

func (r *Redis) registerServer(ctx context.Context, srv string, ts time.Time, timeout time.Duration) (bool, error) {
	hostname, err := os.Hostname()
	if err == nil {
		srv = join(srv, strings.ToLower(strings.TrimSpace(hostname)))
	}
	return r.Client.SetNX(ctx, join(ServerKey, srv), ts.Format(time.RFC3339Nano), timeout).Result()
}

func (r *Redis) FormatKey(args ...string) string {
	return join(r.prefix, join(args...))
}

func (r *Redis) FormatKey2(s string, i uint64) string {
	return join(r.prefix, s, formatUint(i))
}

func (r *Redis) FormatKey3(s1, s2, s3 string, i int64) string {
	return join(r.prefix, s1, s2, s3, formatInt(i))
}

func (r *Redis) FormatKey4(s1, s2, s3, s4 string, i int64) string {
	return join(r.prefix, s1, s2, s3, s4, formatInt(i))
}

func FormatStringKey(args ...string) string {
	return join(args...)
}

func JoinShareHashKey(miner, worker string, height, shareDiff int64) string {
	return join(
		miner,
		worker,
		formatInt(height),
		formatInt(shareDiff),
	)
}

func JoinHashrateValue(hashrate uint64, miner, worker string, unixNano int64) string {
	return join(
		formatUint(hashrate),
		miner,
		worker,
		formatInt(unixNano),
	)
}

func JoinBlockHashrateValue(height uint64, miner, worker string, timestamp int64, blockDiff, shareDiff int64, pow string) string {
	return join(
		formatUint(height),
		miner,
		worker,
		formatInt(timestamp),
		formatInt(blockDiff),
		pow,
		formatInt(shareDiff),
	)
}

func formatUint(i uint64) string {
	return strconv.FormatUint(i, 10)
}

func formatInt(i int64) string {
	return strconv.FormatInt(i, 10)
}

func join(s ...string) string {
	return strings.Join(s, ":")

	//s := make([]string, len(args))
	//for i, v := range args {
	//	s[i] = v.(string)
	//	switch v.(type) {
	//	case string:
	//		s[i] = v.(string)
	//	case int64:
	//		s[i] = strconv.FormatInt(v.(int64), 10)
	//	case uint64:
	//		s[i] = strconv.FormatUint(v.(uint64), 10)
	//	case float64:
	//		s[i] = strconv.FormatFloat(v.(float64), 'f', 0, 64)
	//	case bool:
	//		if v.(bool) {
	//			s[i] = "1"
	//		} else {
	//			s[i] = "0"
	//		}
	//	case *big.Int:
	//		n := v.(*big.Int)
	//		if n != nil {
	//			s[i] = n.String()
	//		} else {
	//			s[i] = "0"
	//		}
	//	default:
	//		panic("Invalid type specified for conversion")
	//	}
	//}
	//return strings.Join(s, ":")
}
