package cache

import (
	"domain/internal/metrics"
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

// 监控指标
var (
	// redis 调用消耗的时间
	durations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:  "domain",
			Subsystem:  "redis",
			Name:       "durations_seconds",
			Help:       "storage latency distributions.",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"method"},
	)
)

func init() {
	prometheus.MustRegister(durations)
}

func Observe(start time.Time, lvs ...string) {
	if metrics.Enabled {
		durations.WithLabelValues(lvs...).Observe(time.Since(start).Seconds())
	}
}
