package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
	"strconv"
	"testing"
	"time"
)

var (
	r   *Redis
	ctx = context.Background()
)

func withSetBenchmark(b *testing.B, key string, init int, f func(client *redis.Client, k, v string)) {
	client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		DB:   2,
	})
	defer client.Close()

	client.Del(ctx, key)
	for i := 0; i < init; i++ {
		f(client, key, join(strconv.FormatInt(int64(i), 10), strconv.FormatInt(time.Now().UnixNano(), 10)))
	}
	b.ResetTimer()

	for i := init; i < b.N+init; i++ {
		member := strconv.Itoa(i)
		b.StartTimer()
		f(client, key, member)
		b.StopTimer()
	}
}

func withZSetBenchmark(b *testing.B, key string, init int, f func(client *redis.Client, k string, v *redis.Z)) {
	client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		DB:   2,
	})
	defer client.Close()

	client.Del(ctx, key)
	for i := 0; i < init; i++ {
		f(client, key, &redis.Z{Score: float64(i), Member: strconv.Itoa(i)})
	}
	b.ResetTimer()

	for i := init; i < b.N+init; i++ {
		member := &redis.Z{Score: float64(i), Member: strconv.Itoa(i)}
		b.StartTimer()
		f(client, key, member)
		b.StopTimer()
	}
}

func BenchmarkZSet(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet", 0, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})

}

func BenchmarkZSet_10_000(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_10_000", 10_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})

}

func BenchmarkZSet_100_000(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_100_000", 100_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})
}

func BenchmarkZSet_1_000_000(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_1_000_000", 1_000_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})
}

func BenchmarkZSet_10_000_rand(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_10_000_rand", 10_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})
}

func BenchmarkZSet_100_000_rand(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_10_000_rand", 100_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})
}

func BenchmarkZSet_1_000_000_rand(b *testing.B) {
	withZSetBenchmark(b, "BenchmarkZSet_1_000_000_rand", 1_000_000, func(client *redis.Client, k string, v *redis.Z) {
		client.ZAdd(ctx, k, v)
	})
}

func BenchmarkSet(b *testing.B) {
	withSetBenchmark(b, "BenchmarkSet", 0, func(client *redis.Client, k, v string) {
		client.SAdd(ctx, k, v)
	})
}

func BenchmarkSet_10_000(b *testing.B) {
	withSetBenchmark(b, "BenchmarkSet_100_000", 10_000, func(client *redis.Client, k, v string) {
		client.SAdd(ctx, k, v)
	})
}

func BenchmarkSet_100_000(b *testing.B) {
	withSetBenchmark(b, "BenchmarkSet_100_000", 100_000, func(client *redis.Client, k, v string) {
		client.SAdd(ctx, k, v)
	})
}

func BenchmarkSet_1_000_000(b *testing.B) {
	withSetBenchmark(b, "BenchmarkSet_1_000_000", 1_000_000, func(client *redis.Client, k, v string) {
		client.SAdd(ctx, k, v)
	})
}

func TestHostname(t *testing.T) {
	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("Hostname: %s\n", hostname)
}
