package util

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/shopspring/decimal"
	"math/big"
	"reflect"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var (
	Gwei  = decimal.NewFromBigInt(math.BigPow(10, 9), 0)
	Ether = decimal.NewFromBigInt(math.BigPow(10, 18), 0)
	// two256 is a big integer representing 2^256
	two256         = new(big.Int).Exp(big.NewInt(2), big.NewInt(256), big.NewInt(0))
	addressPattern = regexp.MustCompile("^0x[0-9a-fA-F]{40}$")
	zero           = big.NewInt(0)
)

func IsValidHexAddress(s string) bool {
	if IsZeroHash(s) || !addressPattern.MatchString(s) {
		return false
	}
	return true
}

func FormatHexAddress(s string) string {
	s = strings.ToLower(s)
	l := len(s)
	if l < 40 {
		return s
	} else if !strings.HasPrefix(s, "0x") {
		return "0x" + s
	} else {
		return s
	}
}

func IsZeroHash(s string) bool {
	return new(big.Int).SetBytes(common.HexToAddress(s).Bytes()).Cmp(zero) == 0
}

// DecodeBig decodes a hex string with 0x prefix as a quantity.
// Numbers larger than 256 bits are not accepted.
func DecodeBig(input string) (*big.Int, error) {
	return hexutil.DecodeBig(input)
}

// MustDecodeBig decodes a hex string with 0x prefix as a quantity.
// It panics for invalid input.
func MustDecodeBig(input string) *big.Int {
	return hexutil.MustDecodeBig(input)
}

func FromTargetHex(targetHex string) *big.Int {
	target := common.HexToHash(targetHex)
	if target.Big().Cmp(zero) == 0 {
		return zero
	}

	return new(big.Int).Div(two256, target.Big())
}

func ToTargetHex(difficulty *big.Int) string {
	if difficulty.Cmp(zero) == 0 {
		return common.BytesToHash(zero.Bytes()).Hex()
	}
	return common.BytesToHash(new(big.Int).Div(two256, difficulty).Bytes()).Hex()
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// FormatAddressHex
func FormatAddressHex(s string) string {
	return "0x" + s[26:]
}

// FormatUintHex 格式化为 0x....16进制格式
func FormatUintHex(i uint64) string {
	return "0x" + strconv.FormatUint(i, 16)
}

// FormatIntHex 格式化为 0x....16进制格式
func FormatIntHex(i int) string {
	return "0x" + strconv.FormatInt(int64(i), 16)
}

// ParseUintHex 解析 0x....格式的 16 进制字符串为 uint64
func ParseUintHex(s string) (uint64, error) {
	return strconv.ParseUint(strings.Replace(s, "0x", "", -1), 16, 64)
}

// FormatUint64 格式化为 10 进制字符串
func FormatUint64(i uint64) string {
	return strconv.FormatUint(i, 10)
}

// FormatInt64 格式化为 10 进制字符串
func FormatInt64(i int64) string {
	return strconv.FormatInt(i, 10)
}

// ParseUint64 解析 10 进制字符串为 uint64
func ParseUint64(s string) (uint64, error) {
	return strconv.ParseUint(s, 10, 64)
}

// ParseInt64 解析 10 进制字符串为 uint64
func ParseInt64(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

func ToEther(wei string) decimal.Decimal {
	weid, _ := decimal.NewFromString(wei)
	return weid.Div(Ether)
}

func GWeiToEther(gwei string) decimal.Decimal {
	weid, _ := decimal.NewFromString(gwei)
	return weid.Div(Gwei)
}

var hashesUnits = []string{"MH", "GH", "TH", "PH", "EH", "ZH", "YH"}

func NiceHashes(n int64) string {
	if n == 0 {
		return strconv.FormatInt(n, 10)
	}
	l := int64(0)
	f := float64(n / 1000_000)
	for f >= 1000 {
		f = f / 1000
		l++
	}
	return fmt.Sprintf("%.2f %s", f, hashesUnits[l])
}

func Package() string {
	pc, _, _, _ := runtime.Caller(1)
	parts := strings.Split(runtime.FuncForPC(pc).Name(), ".")
	pl := len(parts)
	pkage := ""
	funcName := parts[pl-1]
	if parts[pl-2][0] == '(' {
		funcName = parts[pl-2] + "." + funcName
		pkage = strings.Join(parts[0:pl-2], ".")
	} else {
		pkage = strings.Join(parts[0:pl-1], ".")
	}
	return pkage
}

func AtStartMinuteOf(now time.Time, tWindow time.Duration) int64 {
	internal := int(tWindow.Minutes())
	offset := now.Minute() % internal
	return time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute()-offset, 0, 0, now.Location()).Unix()
}

func StructAssign(binding interface{}, value interface{}) {
	bVal := reflect.ValueOf(binding).Elem() //获取reflect.Type类型
	vVal := reflect.ValueOf(value).Elem()   //获取reflect.Type类型
	vTypeOfT := vVal.Type()
	for i := 0; i < vVal.NumField(); i++ {
		// 在要修改的结构体中查询有数据结构体中相同属性的字段，有则修改其值
		name := vTypeOfT.Field(i).Name
		if ok := bVal.FieldByName(name).IsValid(); ok {
			bVal.FieldByName(name).Set(reflect.ValueOf(vVal.Field(i).Interface()))
		}
	}
}
