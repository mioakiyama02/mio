package util

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"log"
)

const (
	signFormat = "\x19Ethereum Signed Message:\n%d%s"
)

//生成web3签名
func SignWithEthWeb3(unsignData string, privateByte []byte) (signature string, err error) {
	// 需要先做一次Hash
	unsignDataHashByte := crypto.Keccak256Hash(common.FromHex(unsignData)).Bytes()
	return SignWithEth(unsignDataHashByte, privateByte)
}

//生成签名
func SignWithEth(unsignData, privateKeyByte []byte) (signature string, err error) {
	unsignDataHash := SaltHash(unsignData)
	key, err := crypto.ToECDSA(privateKeyByte)
	if err != nil {
		log.Println("sign ToECDSA err:", err.Error())
		return "", err
	}
	signatureByte, err := crypto.Sign(unsignDataHash, key)
	if err != nil {
		log.Println("sign Sign err:", err.Error())
		return "", err
	}
	// web逻辑
	signatureByte[64] += 27
	return hexutil.Encode(signatureByte), nil
}

//校验签名
func VerifySig(from, sigHex string, msg []byte) bool {
	fromAddr := common.HexToAddress(from)
	sig := hexutil.MustDecode(sigHex)

	// https://github.com/ethereum/go-ethereum/blob/55599ee95d4151a2502465e0afc7c47bd1acba77/internal/ethapi/api.go#L442
	if sig[64] != 27 && sig[64] != 28 {
		return false
	}
	sig[64] -= 27

	pubKey, err := crypto.SigToPub(SaltHash(msg), sig)
	if err != nil {
		return false
	}
	recoveredAddr := crypto.PubkeyToAddress(*pubKey)
	return fromAddr == recoveredAddr
}

// https://github.com/ethereum/go-ethereum/blob/55599ee95d4151a2502465e0afc7c47bd1acba77/internal/ethapi/api.go#L404
// signHash is a helper function that calculates a hash for the given message that can be
// safely used to calculate a signature from.
//
// The hash is calculated as
//   keccak256("\x19Ethereum Signed Message:\n"${message length}${message}).
//
// This gives context to the signed message and prevents signing of transactions.
func SaltHash(data []byte) []byte {
	//加盐hash
	msg := fmt.Sprintf(signFormat, len(data), data)
	return crypto.Keccak256([]byte(msg))
}
