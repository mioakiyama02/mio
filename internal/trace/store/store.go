package store

import (
	"context"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
	"time"
)

type MailStore struct {
	Cache    *cache.Redis
	DataBase *dataBase.DataBase
}

//store Store 具体实现
type store struct {
	Cache    *cache.Redis
	DataBase *dataBase.DataBase
}

// GetBatchSize 获取 MySQL 每批次的大小
func (s store) GetBatchSize() int {
	return s.DataBase.Config.BatchSize
}

func New(cache *cache.Redis, mysql *dataBase.DataBase) Store {
	return &store{
		Cache:    cache,
		DataBase: mysql,
	}
}

// 域名NFT日志表
type DomainNFTLog struct {
	Id          int64     `db:"id" json:"id"`
	NftId       int64     `db:"nft_id" json:"nft_id"`
	Event       string    `db:"event" json:"event"`
	FromAddress string    `db:"from_address" json:"fromAddress"`
	ToAddress   string    `db:"to_address" json:"toAddress"`
	Price       float64   `db:"price" json:"price"`
	TxHash      string    `db:"tx_hash" json:"txHash"`
	Date        time.Time `db:"date" json:"date"`
}

//抵押库地址表
type DomainVault struct {
	Id           int64  `db:"id" json:"id"`
	ChainId      uint64 `db:"chain_id" json:"chainId"`
	Domain       string `db:"domain" json:"domain"`
	TokenId      uint64 `db:"token_id" json:"tokenId"`
	NftAddress   string `db:"nft_address" json:"nftAddress"`
	VaultAddress string `db:"vault_address" json:"vaultAddress"`
	Newest       bool   `db:"newest" json:"newest"`
	CreateTs     string `db:"create_ts" json:"createTs"`
	UpdateTs     string `db:"update_ts" json:"updateTs"`
}

// 域名NFT出价记录表
type DomainOffers struct {
	Id            int64     `db:"id" json:"id"`
	ChainId       uint64    `db:"chain_id" json:"chainId"`
	BlockNum      uint64    `db:"block_num" json:"blockNum"`
	TxHash        string    `db:"tx_hash" json:"txHash"`
	Domain        string    `db:"domain" json:"domain"`
	TokenId       uint64    `db:"token_id" json:"tokenId"`
	NftAddress    string    `db:"nft_address" json:"nftAddress"`
	BidderAddress string    `db:"bidder_address" json:"bidderAddress"`
	Price         float64   `db:"price" json:"price"`
	BlockTs       time.Time `db:"block_ts" json:"blockTs"`
	CreateTs      time.Time `db:"create_ts" json:"createTs"`
}

// 域名NFT碎片信息表
type DomainFragments struct {
	Id           int64     `db:"id" json:"id"`
	ChainId      uint64    `db:"chain_id" json:"chainId"`
	Domain       string    `db:"domain" json:"domain"`
	TokenId      uint64    `db:"token_id" json:"tokenId"`
	NftAddress   string    `db:"nft_address" json:"nftAddress"`
	Erc20Address string    `db:"erc20_address" json:"erc20Address"`
	Owner        string    `db:"owner" json:"owner"`
	TotalSupply  uint64    `db:"total_supply" json:"totalSupply"`
	Price        float64   `db:"price" json:"price"`
	Status       int8      `db:"status" json:"status"`
	CreateTs     time.Time `db:"create_ts" json:"createTs"`
	UpdateTs     time.Time `db:"update_ts" json:"updateTs"`
}

// Store 存储接口
type Store interface {

	//获取最后抓取区块高度
	GetLastBlockNum(ctx context.Context, chainId uint64) (uint64, error)

	//域名NFT铸币
	MintDomain(ctx context.Context, chainId uint64, txHash string, blockNum uint64, domain string, nftAddress string, toAddress string, tokenId uint64) (int64, error)

	//固定价订单
	FixedPriceOrderForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, curatorAddress string, tokenId uint64, price float64) (int64, error)

	//固定价订单取消
	FixedPriceCancelForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, curatorAddress string, tokenId uint64, price float64) (int64, error)

	//固定价购买
	FixedPriceBuyForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, buyerAddress string, tokenId uint64, price float64) (int64, error)

	//出价
	MakeOfferForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, bidderAddress string, tokenId uint64, price float64, duration uint64) (int64, error)

	//按出价卖出
	OfferToSellForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, curatorAddress string, bidderAddress string, tokenId uint64, price float64) (int64, error)

	//碎片化
	MintVault(ctx context.Context, chainId uint64, txHash string, blockNum uint64, curatorAddress string, nftAddress string, tokenId uint64, erc20Address string, erc20TotalSupply uint64, vaultAddress string, reservePrice float64) (int64, error)

	//查询抵押库
	QueryVaultAll(ctx context.Context, chainId uint64) ([]DomainVault, error)
}
