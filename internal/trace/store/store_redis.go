package store

import (
	"context"
	"encoding/json"
	"time"
)

// 域名NFT信息VO
type DomainNFT struct {
	Id             int64     `db:"id" json:"id"`
	ChainId        uint64    `db:"chain_id" json:"chainId"`
	TokenId        uint64    `db:"token_id" json:"tokenId"`
	Domain         string    `db:"domain" json:"domain"`
	NftAddress     string    `db:"nft_address" json:"nftAddress"`
	CuratorAddress string    `db:"curator_address" json:"curatorAddress"`
	Price          float64   `db:"price" json:"price"`
	Status         int8      `db:"status" json:"status"`
	CreateTime     time.Time `db:"create_time" json:"createTime"`
	UpdateTime     time.Time `db:"update_time" json:"updateTime"`
}

//保存域名缓存
func (s *store) saveDomainCache(ctx context.Context, vo DomainNFT) bool {
	if vo.Domain == "" {
		return false
	}
	key := s.Cache.FormatKey("domains")
	if data, err := json.Marshal(vo); err == nil {
		s.Cache.Client.HSet(ctx, key, vo.Domain, data)
		return true
	}
	return false
}

//更新域名缓存
func (s *store) updateDomainCache(ctx context.Context, domain string, owner string, price float64, status int8, totalSupply uint64) bool {
	if domain == "" {
		return false
	}
	key := s.Cache.FormatKey("domains")
	value := s.Cache.Client.HGet(ctx, key, domain)
	if value == nil || value.Val() == "" {
		return false
	}
	vo := DomainNFT{}
	err := json.Unmarshal([]byte(value.Val()), &vo)
	if err != nil {
		return false
	}
	if owner != "" {
		vo.CuratorAddress = owner
	}
	vo.Price = price
	vo.Status = status
	if data, err := json.Marshal(vo); err == nil {
		s.Cache.Client.HSet(ctx, key, vo.Domain, data)
		return true
	}
	return false
}
