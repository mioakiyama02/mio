package store

import (
	"context"
	"domain/internal/pkg/dataBase"
	"domain/internal/pkg/util"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"time"
)

//domain_block
const (
	getLastBlockNumSQL    = `SELECT block_num FROM domain_block WHERE chain_id = ?`
	updateLastBlockNumSQL = `REPLACE INTO domain_block(chain_id, block_num, update_ts) VALUES (:chain_id, :block_num, :update_ts)`
)

//domain_nfts
const (
	updateNftStatusSQL = `UPDATE t_nft SET curator_address=:curator_address, price=:price, update_time=:update_time, status=:status `
	countNftSQL        = `SELECT COUNT(*) FROM t_nft WHERE chain_id=? AND token_id=? AND domain=? AND nft_address=?`
	insertNewNftSQL    = `INSERT IGNORE INTO t_nft(chain_id, token_id, domain, nft_address, curator_address, price, status, create_time, update_time)
						   VALUES (:chain_id, :token_id, :domain, :nft_address, :curator_address, :price, :status, :create_time, :update_time)`

	domainNftsSelectSQL = `SELECT id, domain, curator_address FROM t_nft WHERE chain_id = ? AND token_id = ? AND nft_address = ?`
	updatePriceSQL      = `UPDATE t_nft SET price = :price, status = :status, update_time = :update_time WHERE id = :id`
	updateOwnerPriceSQL = `UPDATE t_nft SET curator_address = :owner, price = :price, status = :status, update_time = :update_time WHERE id = :id`
)

//domain_nfts_log
const (
	countNftLogSQL       = `SELECT COUNT(*) FROM t_nft_event_log WHERE nft_id = ? AND event=? AND tx_hash=?`
	queryNftIdSQL        = `SELECT id FROM t_nft WHERE nft_address = ?`
	insertNftEventLogSQL = `INSERT INTO t_nft_event_log(nft_id, event, from_address, to_address, tx_hash, mail_send_status, date)
						  	  VALUES (:nft_id, :event, :from_address, :to_address, :tx_hash, :mail_send_status, :date)`
)

//domain_vault
const (
	domainVaultInsertSQL = `INSERT INTO t_vault(chain_id, domain, token_id, nft_address, vault_address, create_ts, newest)
								   VALUES (:chain_id, :domain, :token_id, :nft_address, :vault_address, now(), 1)`
	domainVaultUpdateSQL         = `UPDATE t_vault SET vault_address= :vault_address, newest=1 WHERE chain_id=:chain_id and domain=:domain and token_id=:token_id  and nft_address=:nft_address`
	domainVaultUpdateByDomainSQL = `UPDATE t_vault SET newest = 0 WHERE domain = :domain`
	domainVaultSelectSQL         = `SELECT COUNT(*) FROM t_vault WHERE chain_id=? AND domain=? AND token_id=? AND nft_address=?`

	domainVaultSelectAllSQL = `SELECT id,
		   COALESCE(chain_id, "") AS chain_id,
		   COALESCE(domain, "") AS domain,
		   COALESCE(token_id, "") AS token_id,
		   COALESCE(nft_address, "") AS nft_address,
		   COALESCE(vault_address, "") AS vault_address,
		   COALESCE(newest, "") AS newest
       FROM t_vault WHERE chain_id = ? and newest = 1`
)

//domain_offers
const (
	countNftOfferSQL = `SELECT COUNT(*) FROM t_nft_offers WHERE chain_id=? AND domain=? AND tx_hash=?`
	insertOffersSQL  = `INSERT INTO t_nft_offers(chain_id, block_num, tx_hash, domain, token_id, nft_address, bidder_address, price, offer_duration, block_ts, create_ts)
						  	  VALUES (:chain_id, :block_num, :tx_hash, :domain, :token_id, :nft_address, :bidder_address, :price, :offer_duration, :block_ts, :create_ts)`

	//domainOffersCountByTxHashSQL = `select count(1) from t_offers do where do.tx_hash = ?`
)

//domain_fragments
const (
	domainFragmentsInsertSQL = `INSERT INTO t_fragments(chain_id, domain, token_id, nft_address, erc20_address, owner, total_supply, price, status, create_ts, update_ts)
						  	    VALUES (:chain_id, :domain, :token_id, :nft_address, :erc20_address, :owner, :total_supply, :price, :status, :create_ts, :update_ts)`
	domainFragmentsSelectSQL       = `SELECT COUNT(*) FROM t_fragments WHERE chain_id=? AND domain=? AND token_id=? AND nft_address=?`
	domainFragmentsStatusUpdateSQL = `UPDATE t_fragments SET status = 3 WHERE chain_id=:chain_id AND domain=:domain AND token_id=:token_id AND nft_address=:nft_address`
)

//获取最后抓取区块高度
func (s *store) GetLastBlockNum(ctx context.Context, chainId uint64) (uint64, error) {
	var records []uint64
	err := s.DataBase.Dao.Select(&records, getLastBlockNumSQL, chainId)
	if err != nil {
		return 0, errors.Errorf("GetLastBlockNum error: ", err)
	}
	return records[0], nil
}

// 域名NFT铸币
func (s *store) MintDomain(ctx context.Context, chainId uint64, txHash string, blockNum uint64, domain string, nftAddress string, toAddress string, tokenId uint64) (int64, error) {
	var (
		err       error
		count     int
		nftId     int64
		domainNft = DomainNFT{
			ChainId:        chainId,
			Domain:         domain,
			TokenId:        tokenId,
			NftAddress:     nftAddress,
			CuratorAddress: toAddress,
			Price:          0,
			Status:         0, //0-已铸币
			CreateTime:     time.Now(),
			UpdateTime:     time.Now(),
		}
	)
	defer dataBase.Observe(time.Now(), "MintDomain")
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {
		//新增域名NFT记录
		_ = tx.QueryRow(countNftSQL, chainId, tokenId, domain, nftAddress).Scan(&count)
		if count == 0 {
			_, err := tx.NamedExec(insertNewNftSQL, domainNft)
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_nfts(%+v)", domainNft)
			}
		}
		//新增log记录
		_ = s.DataBase.Dao.QueryRow(queryNftIdSQL, nftAddress).Scan(&nftId)
		_ = tx.QueryRow(countNftLogSQL, nftId, "MintDomain", txHash).Scan(&count)
		if count == 0 {
			_, err = tx.NamedExec(insertNftEventLogSQL, map[string]interface{}{
				"nft_id":           nftId,
				"event":            "MintDomain",
				"from_address":     "",
				"to_address":       toAddress,
				"tx_hash":          txHash,
				"mail_send_status": 0,
				"date":             time.Now(),
			})
		} else {
			return 0, errors.Errorf("!insert into domain_nfts_log flase:%s", err)
		}
		//更新最后抓取区块高度
		_, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}
		//保存域名缓存
		vo := DomainNFT{}
		util.StructAssign(&vo, &domainNft)
		s.saveDomainCache(ctx, vo)
		return 0, nil
	})
}

// 固定价订单
func (s *store) FixedPriceOrderForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, curatorAddress string, tokenId uint64, price float64) (int64, error) {
	var (
		nftId, count int
		items        []DomainNFT
		err          = s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	)
	defer dataBase.Observe(time.Now(), "FixedPriceOrderForERC721")
	if err != nil {
		return 0, errors.Errorf("FixedPriceOrderForERC721 sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("FixedPriceOrderForERC721 tokenId is not exist:", tokenId)
	}
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {
		//更新域名NFT记录
		result, err := tx.NamedExec(updatePriceSQL, map[string]interface{}{
			"price":       price,
			"status":      1, //1-已发布
			"update_time": time.Now(),
			"id":          items[0].Id,
		})
		if err != nil {
			return 0, errors.Errorf("FixedPriceOrderForERC721 updatePriceSQL error: ", err)
		}
		//新增log记录
		err = tx.QueryRow(queryNftIdSQL, nftAddress).Scan(&nftId)
		err = tx.QueryRow(countNftLogSQL, nftId, "FixedPriceOrderForERC721", txHash).Scan(&count)
		if count == 0 {
			result, err = tx.NamedExec(insertNftEventLogSQL, map[string]interface{}{
				"nft_id":           nftId,
				"event":            "FixedPriceOrderForERC721",
				"from_address":     curatorAddress,
				"to_address":       "",
				"tx_hash":          txHash,
				"mail_send_status": 0,
				"date":             time.Now(),
			})
		}
		//更新最后抓取区块高度
		result, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}
		//更新域名缓存
		s.updateDomainCache(ctx, items[0].Domain, "", price, 1, 0)
		return result.RowsAffected()
	})
}

// 固定价订单取消
func (s *store) FixedPriceCancelForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, curatorAddress string, tokenId uint64, price float64) (int64, error) {
	var (
		nftId, count int
		items        []DomainNFT
		domainNftLog = DomainNFTLog{}
	)
	defer dataBase.Observe(time.Now(), "FixedPriceCancelForERC721")
	//查询域名NFT记录
	err := s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	if err != nil {
		return 0, errors.Errorf("FixedPriceCancelForERC721 sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("FixedPriceCancelForERC721 tokenId is not exist:", tokenId)
	}
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {
		//更新域名NFT记录
		result, err := tx.NamedExec(updatePriceSQL, map[string]interface{}{
			"price":       price,
			"status":      0, //0-已铸币
			"update_time": time.Now(),
			"id":          items[0].Id,
		})
		if err != nil {
			return 0, errors.Errorf("FixedPriceCancelForERC721 updatePriceSQL error: ", err)
		}
		//新增log记录
		_ = tx.QueryRow(queryNftIdSQL, nftAddress).Scan(&nftId)
		_ = tx.QueryRow(countNftLogSQL, nftId, "FixedPriceCancelForERC721", txHash).Scan(&count)
		if count == 0 {
			result, err = tx.NamedExec(insertNftEventLogSQL, map[string]interface{}{
				"nft_id":           nftId,
				"tx_hash":          txHash,
				"event":            "FixedPriceCancelForERC721",
				"from_address":     curatorAddress,
				"to_address":       "",
				"mail_send_status": 0,
				"date":             time.Now(),
			})
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_nfts_log(%+v)", domainNftLog)
			}
		}
		//更新最后抓取区块高度
		result, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}
		//更新域名缓存
		s.updateDomainCache(ctx, items[0].Domain, "", price, 1, 0)
		return result.RowsAffected()
	})
}

//固定价购买
func (s *store) FixedPriceBuyForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, buyerAddress string, tokenId uint64, price float64) (int64, error) {
	var (
		count int
	)
	defer dataBase.Observe(time.Now(), "BuyNft")
	//查询域名NFT记录
	var items []DomainNFT
	err := s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	if err != nil {
		return 0, errors.Errorf("FixedPriceBuyForERC721 sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("FixedPriceBuyForERC721 tokenId is not exist:", tokenId)
	}
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {
		//更新域名NFT记录
		result, err := tx.NamedExec(updateOwnerPriceSQL, map[string]interface{}{
			"owner":       buyerAddress,
			"price":       0,
			"status":      0, //0-已铸币
			"update_time": time.Now(),
			"id":          items[0].Id,
		})
		if err != nil {
			return 0, errors.Errorf("FixedPriceBuyForERC721 updateOwnerPriceSQL error: ", err)
		}

		//新增log记录
		_ = tx.QueryRow(countNftLogSQL, items[0].Id, "FixedPriceBuyForERC721", txHash).Scan(&count)
		if count == 0 {
			result, err = tx.NamedExec(insertNftEventLogSQL, map[string]interface{}{
				"nft_id":           items[0].Id,
				"tx_hash":          txHash,
				"event":            "FixedPriceBuyForERC721",
				"from_address":     items[0].CuratorAddress,
				"to_address":       buyerAddress,
				"mail_send_status": 0,
				"date":             time.Now(),
			})
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_nfts_log(%s)", err)
			}
		}
		//更新最后抓取区块高度
		result, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}

		//更新域名缓存
		s.updateDomainCache(ctx, items[0].Domain, buyerAddress, 0, 0, 0)

		return result.RowsAffected()
	})
}

//出价
func (s *store) MakeOfferForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64, nftAddress string, bidderAddress string, tokenId uint64, price float64, duration uint64) (int64, error) {
	var (
		count int
		items []DomainNFT
	)
	defer dataBase.Observe(time.Now(), "MakeOffer")
	//查询域名NFT记录
	err := s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	if err != nil {
		return 0, errors.Errorf("MakeOfferForERC721 sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("MakeOfferForERC721 tokenId is not exist:", tokenId)
	}
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {

		//新增出价记录
		err = tx.QueryRow(countNftOfferSQL, chainId, items[0].Domain, txHash).Scan(&count)
		if count == 0 {
			err = nil
			_, err := tx.NamedExec(insertOffersSQL, map[string]interface{}{
				"chain_id":       chainId,
				"block_num":      blockNum,
				"tx_hash":        txHash,
				"domain":         items[0].Domain,
				"token_id":       tokenId,
				"nft_address":    nftAddress,
				"bidder_address": bidderAddress,
				"price":          price,
				"offer_duration": duration,
				"block_ts":       time.Now(),
				"create_ts":      time.Now(),
			})
			if err != nil {
				if err != nil {
					return 0, errors.Wrapf(err, "!insert into domain_offers(%s)", err)
				}
			}
		}
		err = tx.QueryRow(countNftLogSQL, items[0].Id, "MakeOfferForERC721", txHash).Scan(&count)
		if count == 0 {
			_, err := tx.NamedExec(insertNftEventLogSQL, map[string]interface{}{
				"nft_id":           items[0].Id,
				"tx_hash":          txHash,
				"event":            "MakeOfferForERC721",
				"from_address":     bidderAddress,
				"to_address":       items[0].CuratorAddress,
				"mail_send_status": 0,
				"date":             time.Now(),
			})
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_offers(%s)", err)
			}
		}
		//更新最后抓取区块高度
		_, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}
		return 0, nil
	})
}

func (s *store) OfferToSellForERC721(ctx context.Context, chainId uint64, txHash string, blockNum uint64,
	nftAddress string, curatorAddress string, bidderAddress string, tokenId uint64, price float64) (int64, error) {
	var (
		items []DomainNFT
		count int
	)
	err := s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	if err != nil {
		return 0, errors.Errorf("MintVault sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("MintVault tokenId is not exist:", tokenId)
	}
	_, err = s.DataBase.Dao.NamedExec(updateNftStatusSQL, map[string]interface{}{
		"curator_address": curatorAddress,
		"price":           price,
		"update_time":     time.Now(),
		"status":          0,
	})
	err = s.DataBase.Dao.QueryRow(countNftLogSQL, items[0].Id, "OfferToSellForERC721", txHash).Scan(&count)
	if count == 0 {
		_, err := s.DataBase.Dao.NamedExec(insertNftEventLogSQL, map[string]interface{}{
			"nft_id":           items[0].Id,
			"tx_hash":          txHash,
			"event":            "OfferToSellForERC721",
			"from_address":     items[0].CuratorAddress,
			"to_address":       bidderAddress,
			"mail_send_status": 0,
			"date":             time.Now(),
		})
		if err != nil {
			return 0, errors.Wrapf(err, "!insert into domain_offers(%s)", err)
		}
		//更新最后抓取区块高度
		_, err = s.DataBase.Dao.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}
	}
	return 0, nil
}

//碎片化
func (s *store) MintVault(ctx context.Context, chainId uint64, txHash string, blockNum uint64, curatorAddress string, nftAddress string,
	tokenId uint64, erc20Address string, erc20TotalSupply uint64, vaultAddress string, reservePrice float64) (int64, error) {
	start := time.Now()
	defer dataBase.Observe(start, "MintVault")

	//查询域名NFT记录
	var items []DomainNFT
	err := s.DataBase.Dao.Select(&items, domainNftsSelectSQL, chainId, tokenId, nftAddress)
	if err != nil {
		return 0, errors.Errorf("MintVault sql error:", err)
	}
	if len(items) == 0 {
		return 0, errors.Errorf("MintVault tokenId is not exist:", tokenId)
	}

	domainNftsLog := DomainNFTLog{

		TxHash: txHash,

		Event:       "MintVault",
		FromAddress: curatorAddress,
		ToAddress:   vaultAddress,
		Price:       reservePrice,
	}

	domainVault := DomainVault{
		ChainId:      chainId,
		Domain:       items[0].Domain,
		TokenId:      tokenId,
		NftAddress:   nftAddress,
		VaultAddress: vaultAddress,
	}

	domainFragments := DomainFragments{
		ChainId:      chainId,
		Domain:       items[0].Domain,
		TokenId:      tokenId,
		NftAddress:   nftAddress,
		Erc20Address: erc20Address,
		Owner:        curatorAddress,
		TotalSupply:  erc20TotalSupply,
		Price:        reservePrice,
		Status:       0, //0-已铸币
		CreateTs:     time.Now(),
		UpdateTs:     time.Now(),
	}
	return s.DataBase.WithTx(func(tx *sqlx.Tx) (int64, error) {
		//更新域名NFT记录
		result, err := tx.NamedExec(updateOwnerPriceSQL,
			map[string]interface{}{
				"owner":     vaultAddress,
				"price":     0,
				"status":    2, //2-碎片化
				"update_ts": time.Now(),
				"id":        items[0].Id,
			},
		)
		if err != nil {
			return 0, errors.Errorf("MintVault updateOwnerPriceSQL error: ", err)
		}

		//新增log记录
		result, err = tx.NamedExec(insertNftEventLogSQL, domainNftsLog)
		if err != nil {
			return 0, errors.Wrapf(err, "!insert into domain_nfts_log(%+v)", domainNftsLog)
		}

		//判断domain内容重复以及数据修改逻辑
		var count int64
		s.DataBase.Dao.QueryRow(domainVaultSelectSQL, domainVault.ChainId, domainVault.Domain, domainVault.TokenId, domainVault.NftAddress).Scan(&count)
		if count == 1 {
			result, err = tx.NamedExec(domainVaultUpdateSQL, domainVault)
			if err != nil {
				return 0, errors.Wrapf(err, "!update into domain_vault(%+v)", domainVault)
			}
		} else if count >= 1 {
			result, err = tx.NamedExec(domainVaultUpdateByDomainSQL, domainVault)
			if err != nil {
				return 0, errors.Wrapf(err, "!update into domain_vault(%+v)", domainVault)
			}
		} else {
			//新增VaultAddress记录
			result, err = tx.NamedExec(domainVaultInsertSQL, domainVault)
			result, err = tx.NamedExec(domainFragmentsStatusUpdateSQL, domainVault)
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_vault(%+v)", domainVault)
			}
		}

		//新增碎片化记录
		s.DataBase.Dao.QueryRow(domainFragmentsSelectSQL, domainFragments.ChainId, domainFragments.Domain, domainFragments.TokenId, domainFragments.NftAddress).Scan(&count)
		if count == 0 {
			result, err = tx.NamedExec(domainFragmentsInsertSQL, domainFragments)
			if err != nil {
				return 0, errors.Wrapf(err, "!insert into domain_fragments(%+v)", domainFragments)
			}
		}

		//更新最后抓取区块高度
		result, err = tx.NamedExec(updateLastBlockNumSQL,
			map[string]interface{}{
				"chain_id":  chainId,
				"block_num": blockNum,
				"update_ts": time.Now(),
			},
		)
		if err != nil {
			return 0, errors.Wrapf(err, "!update domain_block(%+v)", blockNum)
		}

		//更新域名缓存
		s.updateDomainCache(ctx, items[0].Domain, curatorAddress, reservePrice, 2, erc20TotalSupply)

		return result.RowsAffected()
	})
}

//碎片化
func (s *store) QueryVaultAll(ctx context.Context, chainId uint64) ([]DomainVault, error) {
	var items []DomainVault
	err := s.DataBase.Dao.Select(&items, domainVaultSelectAllSQL, chainId)
	if err != nil {
		return items, errors.Wrapf(err, "!query domain_vault(%+v)", chainId)
	}
	if len(items) == 0 {
		return items, errors.Errorf("!query domain_vault(%+v)", chainId)
	}
	return items, nil
}
