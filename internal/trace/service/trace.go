package service

import (
	"context"
	"domain/internal/contracts/DomainERC721"
	"domain/internal/contracts/DomainExchange"
	"domain/internal/contracts/DomainMintFactory"
	"domain/internal/contracts/DomainVault"
	"domain/internal/helper"
	"domain/internal/pkg/util"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/pkg/errors"
	"math/big"
	"strings"
)

var CHAIN_ID uint64

type TraceService struct {
	Service
	client      *helper.SafeEthClient
	logHex      LogHex
	contractAbi ContractAbi
}

type ContractAbi struct {
	ERC721ContractAbi      abi.ABI
	ExchangeContractAbi    abi.ABI
	MintFactoryContractAbi abi.ABI
	DomainVaultAbi         abi.ABI
}

type LogHex struct {
	MintDomainHex                string
	MakeOfferForERC721Hex        string
	FixedPriceOrderForERC721Hex  string
	FixedPriceCancelForERC721Hex string
	FixedPriceBuyForERC721Hex    string
	MintVaultHex                 string
	VoteHex                      string
	OfferToSellForERC721Hex      string
	PreSaleOrderForERC20Hex      string
	PreSaleBuyForERC20Hex        string
	PreSaleWithdrawForERC20Hex   string
	RedeemHex                    string
	MakeOfferBidHex              string
	BidHex                       string
	WithdrawBidFundHex           string
	WithdrawERC721Hex            string
	WithdrawFundHex              string
}

type LogMintDomain struct {
	Curator common.Address
	TokenId *big.Int
	Domain  [32]byte
}

type Duration struct {
	Price         *big.Int
	OfferDuration *big.Int
}

type LogFixedPriceOrderForERC721 struct {
	//Curator common.Address
	//TokenId *big.Int
	Price *big.Int
}

type LogFixedPriceBuyForERC721 struct {
	//Buyer   common.Address
	//TokenId *big.Int
	Price *big.Int
}

type LogFixedPriceCancelForERC721 struct {
	//Curator   common.Address
	//TokenId *big.Int
	Price *big.Int
}

type LogMakeOfferForERC721 struct {
	//Bidder   common.Address
	//TokenId *big.Int
	Price *big.Int
}

type OfferToSellForERC721 struct {
	Price *big.Int
}

//event MintVault(address indexed curator, address erc721, uint256 erc721TokenId, address erc20, uint256 erc20TotalSupply, address vault, uint256 reservePrice)
type LogMintVault struct {
	Erc721        common.Address
	Erc721TokenId *big.Int
	Erc20         common.Address
	TotalSupply   *big.Int
	Vault         common.Address
	ReservePrice  *big.Int
}

type LogVote struct {
	voter common.Address
	votes *big.Int
}

func NewTraceService(srv Service) *TraceService {
	/*client, err := ethclient.Dial(srv.config.WsRpc)
	if err != nil {
		log.Fatal(err)
	}*/
	client, err := helper.NewSafeClient(srv.config.WsRpc)
	if err != nil {
		log.Error(fmt.Sprintf("Failed to connect to the client: %s", err))
	}
	chainId, err := client.ChainID(context.Background())
	if err != nil {
		log.Error("Trace getChainId error：", err)
	}
	CHAIN_ID = chainId.Uint64()

	ERC721ContractAbi, _ := abi.JSON(strings.NewReader(DomainERC721.DomainERC721ABI))
	ExchangeContractAbi, _ := abi.JSON(strings.NewReader(DomainExchange.DomainExchangeABI))
	MintFactoryContractAbi, _ := abi.JSON(strings.NewReader(DomainMintFactory.DomainMintFactoryABI))
	DomainVaultAbi, _ := abi.JSON(strings.NewReader(DomainVault.DomainVaultABI))

	contractAbi := ContractAbi{
		ERC721ContractAbi:      ERC721ContractAbi,
		ExchangeContractAbi:    ExchangeContractAbi,
		MintFactoryContractAbi: MintFactoryContractAbi,
		DomainVaultAbi:         DomainVaultAbi,
	}
	logHex := LogHex{
		MintDomainHex:                crypto.Keccak256Hash([]byte("MintDomain(address,uint256,bytes32)")).Hex(),
		FixedPriceOrderForERC721Hex:  crypto.Keccak256Hash([]byte("FixedPriceOrderForERC721(address,uint256,uint256)")).Hex(),
		FixedPriceCancelForERC721Hex: crypto.Keccak256Hash([]byte("FixedPriceCancelForERC721(address,uint256,uint256)")).Hex(),
		FixedPriceBuyForERC721Hex:    crypto.Keccak256Hash([]byte("FixedPriceBuyForERC721(address,uint256,uint256)")).Hex(),
		MakeOfferForERC721Hex:        crypto.Keccak256Hash([]byte("MakeOfferForERC721(address,uint256,uint256,uint256)")).Hex(),
		OfferToSellForERC721Hex:      crypto.Keccak256Hash([]byte("OfferToSellForERC721(address,address,uint256,uint256)")).Hex(),

		MintVaultHex:               crypto.Keccak256Hash([]byte("MintVault(address,address,uint256,address,uint256,address,uint256)")).Hex(),
		PreSaleOrderForERC20Hex:    crypto.Keccak256Hash([]byte("PreSaleOrderForERC20(address,address,uint256,uint256,uint256,uint256)")).Hex(),
		PreSaleBuyForERC20Hex:      crypto.Keccak256Hash([]byte("PreSaleBuyForERC20(address,address,uint256)")).Hex(),
		PreSaleWithdrawForERC20Hex: crypto.Keccak256Hash([]byte("PreSaleWithdrawForERC20(address,address,uint256)")).Hex(),

		RedeemHex:          crypto.Keccak256Hash([]byte("Redeem(address)")).Hex(),
		MakeOfferBidHex:    crypto.Keccak256Hash([]byte("MakeOfferBid(address,uint256)")).Hex(),
		VoteHex:            crypto.Keccak256Hash([]byte("Vote(address,uint256)")).Hex(),
		BidHex:             crypto.Keccak256Hash([]byte("Bid(address,uint256)")).Hex(),
		WithdrawBidFundHex: crypto.Keccak256Hash([]byte("WithdrawBidFund(address,uint256)")).Hex(),
		WithdrawERC721Hex:  crypto.Keccak256Hash([]byte("WithdrawERC721(address,uint256,uint256)")).Hex(),
		WithdrawFundHex:    crypto.Keccak256Hash([]byte("WithdrawFund(address,uint256,uint256)")).Hex(),
	}

	return &TraceService{
		Service: srv,
		client:  client,
		logHex:  logHex,

		contractAbi: contractAbi,
	}
}

// 订阅最新日志
func (s *TraceService) TraceNew(ctx context.Context) error {

	addresses := []common.Address{
		common.HexToAddress(s.config.DomainERC721Address),
		common.HexToAddress(s.config.DomainExchangeAddress),
		common.HexToAddress(s.config.DomainMintFactoryAddress),
	}

	vaultAddresses := s.getVaultAddresses(ctx)
	for index := range vaultAddresses {
		addresses = append(addresses, vaultAddresses[index])
	}

	query := ethereum.FilterQuery{
		Addresses: addresses,
	}

	logs := make(chan types.Log)
	sub, err := s.client.SubscribeFilterLogs(ctx, query, logs)
	if err != nil {
		log.Error("TraceNew SubscribeFilterLogs error：", err)
	}
	for {
		select {
		case err := <-sub.Err():
			log.Error("TraceNew Subscribe error：", err)
			s.client.RecoverDisconnect()
			sub, err = s.client.SubscribeFilterLogs(ctx, query, logs)
			if err != nil {
				log.Error("TraceNew SubscribeFilterLogs error：", err)
			}
		case vLog := <-logs:
			s.logHandle(ctx, vLog)
		}
	}
	return nil
}

// 填补历史日志
func (s *TraceService) FillHistory(ctx context.Context) error {
	lastBlock, err := s.store.GetLastBlockNum(ctx, CHAIN_ID)
	if err != nil {
		return err
	}
	toBlock, err := s.client.BlockNumber(ctx)
	if err != nil {
		return errors.Errorf("get blockNumber error: ", err)
	}
	fromBlock := lastBlock + 1
	err = s.TraceHistory(ctx, new(big.Int).SetUint64(fromBlock), new(big.Int).SetUint64(toBlock+100))
	if err != nil {
		return err
	}
	return nil
}

// 抓取历史日志
func (s *TraceService) TraceHistory(ctx context.Context, fromBlock *big.Int, toBlock *big.Int) error {

	addresses := []common.Address{
		common.HexToAddress(s.config.DomainERC721Address),
		common.HexToAddress(s.config.DomainExchangeAddress),
		//common.HexToAddress(s.config.DomainMintFactoryAddress),
	}

	for _, vaultAddresses := range s.getVaultAddresses(ctx) {
		addresses = append(addresses, vaultAddresses)
	}

	query := ethereum.FilterQuery{
		FromBlock: fromBlock,
		ToBlock:   toBlock,
		Addresses: addresses,
	}
	logs, err := s.client.FilterLogs(ctx, query)
	if err != nil {
		log.Error("TraceHistory error：", err)
	}
	for _, vLog := range logs {
		s.logHandle(ctx, vLog)
	}
	return nil
}

type PreSaleOrderForERC20 struct {
	DomainERC20 string
	Amount      *big.Int
	Price       *big.Int
}

type PreSaleBuyForERC20 struct {
	DomainERC20 string
	Amount      *big.Int
}

type PreSaleWithdrawForERC20 struct {
	DomainERC20 string
	Amount      *big.Int
}

func (s *TraceService) logHandle(ctx context.Context, vLog types.Log) {
	log.Info("Log Block Number: ", vLog.BlockNumber)
	switch vLog.Topics[0].Hex() {

	case s.logHex.MintDomainHex:
		txHash := vLog.TxHash.Hex()
		var event LogMintDomain
		err := s.contractAbi.ERC721ContractAbi.UnpackIntoInterface(&event, "MintDomain", vLog.Data)
		if err != nil {
			log.Error("ERC721ContractAbi Unpack error：", err)
		}
		domain := byte2String(event.Domain[:])
		_, err = s.store.MintDomain(ctx, CHAIN_ID, txHash, vLog.BlockNumber, domain, s.config.DomainERC721Address, event.Curator.Hex(), event.TokenId.Uint64())

		fmt.Println(vLog.Address.Hex(), vLog.Topics[0].Hex(), vLog.Data)
		if err != nil {
			log.Error("MintDomain error: ", err)
		} else {
			log.Info("MintDomain success, tokenId: ", event.TokenId.Uint64())
		}

	case s.logHex.FixedPriceOrderForERC721Hex:
		txHash := vLog.TxHash.Hex()
		curator := util.FormatAddressHex(vLog.Topics[1].Hex())
		tokenId := vLog.Topics[2].Big().Uint64()

		var event LogFixedPriceOrderForERC721
		err := s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "FixedPriceOrderForERC721", vLog.Data)
		if err != nil {
			log.Error("ExchangeContractAbi Unpack error：", err)
		}
		//price, _ := util.ToEther(event.Price.String()).Float64()
		price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()

		_, err = s.store.FixedPriceOrderForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, curator, tokenId, price)
		if err != nil {
			log.Error("FixedPriceOrderForERC721 error: ", err)
		} else {
			log.Info("FixedPriceOrderForERC721 success, tokenId: ", tokenId)
		}

	case s.logHex.FixedPriceCancelForERC721Hex:
		txHash := vLog.TxHash.Hex()
		buyer := util.FormatAddressHex(vLog.Topics[1].Hex())
		tokenId := vLog.Topics[2].Big().Uint64()

		var event LogFixedPriceCancelForERC721
		err := s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "FixedPriceCancelForERC721", vLog.Data)
		if err != nil {
			log.Error("ExchangeContractAbi Unpack error：", err)
		}
		//price, _ := util.ToEther(event.Price.String()).Float64()
		price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()

		_, err = s.store.FixedPriceCancelForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, buyer, tokenId, price)
		if err != nil {
			log.Error("FixedPriceCancelForERC721 error: ", err)
		} else {
			log.Info("FixedPriceCancelForERC721 success, tokenId: ", tokenId)
		}

	case s.logHex.FixedPriceBuyForERC721Hex:
		txHash := vLog.TxHash.Hex()
		buyer := util.FormatAddressHex(vLog.Topics[1].Hex())
		tokenId := vLog.Topics[2].Big().Uint64()

		var event LogFixedPriceBuyForERC721
		err := s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "FixedPriceBuyForERC721", vLog.Data)
		if err != nil {
			log.Error("ExchangeContractAbi Unpack error：", err)
		}
		//price, _ := util.ToEther(event.Price.String()).Float64()
		price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()

		_, err = s.store.FixedPriceBuyForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, buyer, tokenId, price)
		if err != nil {
			log.Error("FixedPriceBuyForERC721 error: ", err)
		} else {
			log.Info("FixedPriceBuyForERC721 success, tokenId: ", tokenId)
		}

	case s.logHex.MakeOfferForERC721Hex:
		txHash := vLog.TxHash.Hex()
		bidder := util.FormatAddressHex(vLog.Topics[1].Hex())
		tokenId := vLog.Topics[2].Big().Uint64()
		var duration Duration
		var time uint64 = 0
		var event LogMakeOfferForERC721
		err := s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "MakeOfferForERC721", vLog.Data)
		if err != nil {
			log.Error("ExchangeContractAbi Unpack error：", err)
		}
		_ = s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&duration, "MakeOfferForERC721", vLog.Data)
		if duration.OfferDuration != nil {
			time = duration.OfferDuration.Uint64()
		}
		price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()
		//time := event.OfferDuration.Uint64()
		_, err = s.store.MakeOfferForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, bidder, tokenId, price, time)
		if err != nil {
			log.Error("MakeOfferForERC721 error: ", err)
		} else {
			log.Info("MakeOfferForERC721 success, tokenId: ", tokenId)
		}

	case s.logHex.OfferToSellForERC721Hex:
		var (
			txHash  = vLog.TxHash.Hex()
			curator = util.FormatAddressHex(vLog.Topics[1].Hex())
			bidder  = util.FormatAddressHex(vLog.Topics[2].Hex())
			tokenId = vLog.Topics[3].Big().Uint64()
			event   OfferToSellForERC721
			err     = s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "OfferToSellForERC721", vLog.Data)
		)
		if err != nil {
			log.Error("ExchangeContractAbi Unpack error：", err)
		}
		price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()
		_, err = s.store.OfferToSellForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, curator, bidder, tokenId, price)
		if err != nil {
			log.Error("OfferToSellForERC721 error: ", err)
		} else {
			log.Info("OfferToSellForERC721 success, tokenId: ", tokenId)
		}

		//case s.logHex.MintVaultHex:
		//	txHash := vLog.TxHash.Hex()
		//	curator := util.FormatAddressHex(vLog.Topics[1].Hex())
		//
		//	var event LogMintVault
		//	err := s.contractAbi.MintFactoryContractAbi.UnpackIntoInterface(&event, "MintVault", vLog.Data)
		//	if err != nil {
		//		log.Error("MintFactoryContractAbi Unpack error：", err)
		//	}
		//	nftAddress := s.config.DomainERC721Address
		//	tokenId := event.Erc721TokenId.Uint64()
		//	erc20Address := event.Erc20.Hex()
		//	vaultAddress := event.Vault.Hex()
		//	erc20TotalSupply := new(big.Int).Quo(event.TotalSupply, math.BigPow(10, 18)).Uint64()
		//	reservePrice, _ := new(big.Float).Quo(new(big.Float).SetInt(event.ReservePrice), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()
		//
		//	_, err = s.store.MintVault(ctx, CHAIN_ID, txHash, vLog.BlockNumber, curator, nftAddress, tokenId,
		//		erc20Address, erc20TotalSupply, vaultAddress, reservePrice)
		//	if err != nil {
		//		log.Error("MintVault error: ", err)
		//	} else {
		//		log.Info("MintVault success, tokenId: ", tokenId)
		//	}
		//case s.logHex.PreSaleOrderForERC20Hex:
		//	var (
		//		event     PreSaleOrderForERC20
		//		txHash    = vLog.TxHash.Hex()
		//		curator   = util.FormatAddressHex(vLog.Topics[1].Hex())
		//		startTime = vLog.Topics[2].Big().Uint64()
		//		endTime   = vLog.Topics[3].Big().Uint64()
		//		err       = s.contractAbi.MintFactoryContractAbi.UnpackIntoInterface(&event, "PreSaleOrderForERC20", vLog.Data)
		//	)
		//	if err != nil {
		//		log.Error("PreSaleOrderForERC20 Unpack error：", err)
		//	}
		//	price, _ := new(big.Float).Quo(new(big.Float).SetInt(event.Price), new(big.Float).SetInt(math.BigPow(10, 18))).Float64()
		//	_, err = s.store.OfferToSellForERC721(ctx, CHAIN_ID, txHash, vLog.BlockNumber, s.config.DomainERC721Address, curator, bidder, tokenId, price)
		//	if err != nil {
		//		log.Error("OfferToSellForERC721 error: ", err)
		//	} else {
		//		log.Info("OfferToSellForERC721 success, tokenId: ", tokenId)
		//	}
		//case s.logHex.PreSaleBuyForERC20Hex:
		//	var (
		//		event  PreSaleBuyForERC20
		//		txHash = vLog.TxHash.Hex()
		//		buyer  = util.FormatAddressHex(vLog.Topics[1].Hex())
		//		err    = s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "PreSaleBuyForERC20", vLog.Data)
		//	)
		//case s.logHex.PreSaleWithdrawForERC20Hex:
		//	var (
		//		event   PreSaleWithdrawForERC20
		//		txHash  = vLog.TxHash.Hex()
		//		curator = util.FormatAddressHex(vLog.Topics[1].Hex())
		//		err     = s.contractAbi.ExchangeContractAbi.UnpackIntoInterface(&event, "PreSaleWithdrawForERC20", vLog.Data)
		//	)
		//case s.logHex.RedeemHex:
		//case s.logHex.MakeOfferBidHex:
		//case s.logHex.VoteHex:
		//case s.logHex.BidHex:
		//case s.logHex.WithdrawBidFundHex:
		//case s.logHex.WithdrawERC721Hex:
		//case s.logHex.WithdrawFundHex:
	}
}

func byte2String(p []byte) string {
	for i := 0; i < len(p); i++ {
		if p[i] == 0 {
			return string(p[0:i])
		}
	}
	return string(p)
}

func (s *TraceService) getVaultAddresses(ctx context.Context) []common.Address {
	//添加抵押库地址
	allDomainVault, err := s.store.QueryVaultAll(ctx, s.config.ChainId)
	if err != nil {
		log.Error("QueryVaultAll error：", err)
	}

	addresses := make([]common.Address, len(allDomainVault))
	if err != nil {
		log.Error("getVaultAddresses error：", err)
	}
	for _, domainVault := range allDomainVault {
		addresses = append(addresses, common.HexToAddress(domainVault.VaultAddress))
	}

	return addresses
}
