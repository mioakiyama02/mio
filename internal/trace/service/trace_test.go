package service

import (
	"context"
	"domain/internal/contracts/DomainERC721"
	"domain/internal/contracts/erc20"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"math/big"
	"strings"
	"testing"
)

// LogTransfer ..
type LogTransfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
}

// LogApproval ..
type LogApproval struct {
	Owner   common.Address
	Spender common.Address
	Value   *big.Int
}

const (
	url_bsc         = "wss://speedy-nodes-nyc.moralis.io/031d07bc10aeb1dd9497016a/bsc/testnet/ws"
	nft_contract    = "0xAa8d40ca60d317A399D4f8559Ab3AB556237ACe9"
	url_mumbai      = "wss://ws-matic-mumbai.chainstacklabs.com"
	contract_mumbai = "0x9bf18D220392eEa1e1600c9759603C1CF0bBaF84"
)

func TestTraceService_NftTrace(t *testing.T) {
	t.Run("trace_test", func(t *testing.T) {
		client, err := ethclient.Dial(url_bsc)
		if err != nil {
			log.Fatal(err)
		}
		contractAddress := common.HexToAddress(nft_contract)
		query := ethereum.FilterQuery{
			FromBlock: big.NewInt(14987948),
			Addresses: []common.Address{
				contractAddress,
			},
		}
		logs, err := client.FilterLogs(context.Background(), query)
		if err != nil {
			log.Fatal(err)
		}
		contractAbi, err := abi.JSON(strings.NewReader(DomainERC721.DomainERC721ABI))
		if err != nil {
			log.Fatal(err)
		}
		logMintDomainSig := []byte("MintDomain(address,uint256,bytes32)")
		logMintDomainSigHash := crypto.Keccak256Hash(logMintDomainSig)

		for _, vLog := range logs {
			switch vLog.Topics[0].Hex() {
			case logMintDomainSigHash.Hex():
				fmt.Printf("Log Block Number: %d\n", vLog.BlockNumber)
				fmt.Printf("Log Index: %d\n", vLog.Index)
				fmt.Printf("Log Name: MintDomain\n")

				var mintDomainEvent LogMintDomain

				err := contractAbi.UnpackIntoInterface(&mintDomainEvent, "MintDomain", vLog.Data)
				if err != nil {
					log.Fatal(err)
				}
				//mintDomainEvent.To = common.HexToAddress(vLog.Topics[1].Hex())
				domain := string(mintDomainEvent.Domain[:])
				fmt.Printf("To: %s\n", mintDomainEvent.Curator.Hex())
				fmt.Printf("TokenId: %s\n", mintDomainEvent.TokenId)
				fmt.Printf("Domain: %s\n", domain)
				fmt.Printf("\n\n")
			}
			fmt.Printf("\n\n")
		}
	})
}

func TestTraceService_Transfer(t *testing.T) {
	t.Run("trace_test", func(t *testing.T) {
		client, err := ethclient.Dial(url_mumbai)
		if err != nil {
			log.Fatal(err)
		}
		contractAddress := common.HexToAddress(contract_mumbai)
		query := ethereum.FilterQuery{
			FromBlock: big.NewInt(22348605),
			//ToBlock:   big.NewInt(22349740),
			Addresses: []common.Address{
				contractAddress,
			},
		}
		logs, err := client.FilterLogs(context.Background(), query)
		if err != nil {
			log.Fatal(err)
		}
		contractAbi, err := abi.JSON(strings.NewReader(erc20.TokenABI))
		if err != nil {
			log.Fatal(err)
		}
		logTransferSig := []byte("Transfer(address,address,uint256)")
		logTransferSigHash := crypto.Keccak256Hash(logTransferSig)

		for _, vLog := range logs {
			fmt.Printf("Log Block Number: %d\n", vLog.BlockNumber)
			fmt.Printf("Log Index: %d\n", vLog.Index)

			switch vLog.Topics[0].Hex() {
			case logTransferSigHash.Hex():
				fmt.Printf("Log Name: Transfer\n")

				var transferEvent LogTransfer
				err := contractAbi.UnpackIntoInterface(&transferEvent, "Transfer", vLog.Data)
				if err != nil {
					log.Fatal(err)
				}

				transferEvent.From = common.HexToAddress(vLog.Topics[1].Hex())
				transferEvent.To = common.HexToAddress(vLog.Topics[2].Hex())

				fmt.Printf("From: %s\n", transferEvent.From.Hex())
				fmt.Printf("To: %s\n", transferEvent.To.Hex())
				fmt.Printf("Value: %s\n", transferEvent.Value.String())
			}
			fmt.Printf("\n\n")
		}
	})
}
