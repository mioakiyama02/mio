package trace

import (
	"context"
	"domain/app/server"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
	"domain/internal/trace/conf"
	"domain/internal/trace/service"
	"domain/internal/trace/store"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type Server struct {
	config       *conf.Config
	traceService *service.TraceService
	servers      []server.Server
}

func New(cfg *conf.Config, cache *cache.Redis, dataBase *dataBase.DataBase) server.Server {
	log.Infof("New trace service with config: %+v", cfg)

	sto := store.New(cache, dataBase)
	svc := service.New(cfg, sto)

	traceService := service.NewTraceService(svc)

	//新建 server 实例
	srv := &Server{
		config:       cfg,
		traceService: traceService,
	}
	return srv
}

func (srv *Server) Start(ctx context.Context) error {
	log.Info("[Trace] server starting")
	g, ctx := errgroup.WithContext(ctx)

	// 启动先填补历史日志
	err := srv.traceService.FillHistory(ctx)
	if err != nil {
		return err
	}

	// 订阅最新日志
	err = srv.traceService.TraceNew(ctx)
	if err != nil {
		return err
	}

	// 启动监视器
	/*g.Go(func() error {
		return srv.startMonitor(ctx)
	})*/

	for _, s := range srv.servers {
		ss := s
		g.Go(func() error {
			return ss.Start(ctx)
		})
	}
	if err := g.Wait(); err != nil && !errors.Is(err, context.Canceled) {
		return err
	}
	return nil
}

func (srv *Server) Stop(ctx context.Context) error {
	log.Info("[Trace] server stopping")
	for _, s := range srv.servers {
		s.Stop(ctx)
	}
	return nil
}
