package conf

// Config 配置
type Config struct {
	Enabled                  bool   `yaml:"enabled"`                  //是否启用
	WsRpc                    string `yaml:"wsrpc"`                    //WS服务地址
	DomainERC721Address      string `yaml:"domainERC721Address"`      //NFT合约地址
	DomainExchangeAddress    string `yaml:"domainExchangeAddress"`    //交易合约地址
	DomainMintFactoryAddress string `yaml:"domainMintFactoryAddress"` //交易合约地址
	ChainId                  uint64 `yaml:"chainId"`                  //链ID
}

var DefaultConfig = Config{
	Enabled: false,
	//WsRpc:        "wss://speedy-nodes-nyc.moralis.io/031d07bc10aeb1dd9497016a/bsc/testnet/ws",
	WsRpc:                    "wss://polygon-mumbai.g.alchemy.com/v2/oXXhWC2vgcNsBDesFM0osnZjp2jZiicz",
	DomainERC721Address:      "0x",
	DomainExchangeAddress:    "0x606d92023e121faE09951F6d2b5FdB13534e77d1",
	DomainMintFactoryAddress: "0x48D135c89299ff819294a31604D289D045C082e6",
	ChainId:                  80001, // Polygon Mumbai
}
