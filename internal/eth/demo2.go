package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func main() {
	/*d := crypto.Keccak256Hash([]byte("baz(uint32,bool)"))
	r := d[0:4]
	//fmt.Printf("%0x", r)
	fmt.Println(hexutil.Encode(r))

	d2 := crypto.Keccak256Hash([]byte("buyAndFree22457070633(uint256)"))
	methodID := d2[0:4]
	fmt.Println(hexutil.Encode(methodID))

	fmt.Println(hexutil.Encode([]byte("Scofield")))
	fmt.Println(hex.EncodeToString([]byte("Scofield")))

	decodeBytes,_ := hexutil.Decode("0x53636f6669656c64")
	fmt.Println(string(decodeBytes))*/

	/*hex_string_data := hexutil.Encode([]byte("Scofield"))
	fmt.Println(hex_string_data)

	name := common.RightPadBytes([]byte("Scofield"), 32)
	fmt.Println(hexutil.Encode(name))*/

	/*byte_data := []byte(`Scofield`)

	// 将 byte 装换为 16进制的字符串
	hex_string_data := hex.EncodeToString(byte_data)
	println(hex_string_data)

	// 将 16进制的字符串 转换 byte
	hex_data, _ := hex.DecodeString(hex_string_data)
	println(string(hex_data))
	fmt.Println(hexutil.Encode(hex_data))

	name := common.RightPadBytes(hex_data, 33)
	fmt.Println(hexutil.Encode(name))*/

	d := crypto.Keccak256Hash([]byte("abcei51243fdgjkh(bytes)"))
	methodID := d[0:4]
	fmt.Println(hexutil.Encode(methodID))

	indexByte, _ := IntToBytes(32, 1)
	index := common.LeftPadBytes(indexByte, 32)
	fmt.Println("index：", hexutil.Encode(index))

	lenByte, _ := IntToBytes(8, 1)
	len := common.LeftPadBytes(lenByte, 32)
	fmt.Println("len：", hexutil.Encode(len))

	name := common.RightPadBytes([]byte("Scofield"), 32)
	fmt.Println("name：", hexutil.Encode(name))

	var data []byte
	data = append(data, methodID...)
	data = append(data, index...)
	data = append(data, len...)
	data = append(data, name...)

	fmt.Println(hexutil.Encode(data))
}

//整形转换成字节
func IntToBytes(n int, b byte) ([]byte, error) {
	switch b {
	case 1:
		tmp := int8(n)
		bytesBuffer := bytes.NewBuffer([]byte{})
		binary.Write(bytesBuffer, binary.BigEndian, &tmp)
		return bytesBuffer.Bytes(), nil
	case 2:
		tmp := int16(n)
		bytesBuffer := bytes.NewBuffer([]byte{})
		binary.Write(bytesBuffer, binary.BigEndian, &tmp)
		return bytesBuffer.Bytes(), nil
	case 3, 4:
		tmp := int32(n)
		bytesBuffer := bytes.NewBuffer([]byte{})
		binary.Write(bytesBuffer, binary.BigEndian, &tmp)
		return bytesBuffer.Bytes(), nil
	}
	return nil, fmt.Errorf("IntToBytes b param is invaild")
}
