package main

import (
	"context"
	"crypto/ecdsa"
	"domain/internal/contracts/erc20"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"golang.org/x/crypto/sha3"
	"log"
	"math"
	"math/big"
	"regexp"
)

var (
	ctx         = context.Background()
	client, err = ethclient.Dial("https://matic-mumbai.chainstacklabs.com")
)

func main() {
	/*client, err := ethclient.Dial("https://matic-mumbai.chainstacklabs.com")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("we have a connection")
	_ = client // we'll use this in the upcoming sections*/

	privateKey, _ := crypto.HexToECDSA("a85e1956451df3b616b02e481bd261b40bedc30a4e745b53624afdff54ef24a6")
	_ = privateKey

	account := common.HexToAddress("0xC3DcD99F326CE938EE957B28D5849b886c70248e")
	testAccount := common.HexToAddress("0xe99d1263dd97c02b315c94d777b4b4d80be64355")
	erc20Address := common.HexToAddress("0x9bf18D220392eEa1e1600c9759603C1CF0bBaF84")
	erc721Address := common.HexToAddress("0x7c12cc3D361eb454E88f5b3DF3049143bd6aDf3E")
	erc1155Address := common.HexToAddress("0x28CA41F5416d65C81CA8038aF0ae2932b4052AcD")
	_ = account
	_ = testAccount
	_ = erc20Address
	_ = erc721Address
	_ = erc1155Address
	/*fmt.Println(account.Hex())
	fmt.Println(account.Hash().Hex())
	fmt.Println(account.Bytes())*/

	//查询ETH余额
	//ethBalance(account)

	//查询ERC20余额
	//erc20Balance(account)

	//判断是否合约
	//isContract(erc20Address)

	//查询区块
	//block()

	//查询交易
	//tranctions()

	//转账ETH
	//value := big.NewInt(1000000000000000) // in wei (0.001 eth)
	//transferEth(testAccount, value, privateKey)

	//转账代币
	value := big.NewInt(1500000000000000000) // 1.5 token
	transferToken(erc20Address, testAccount, value, privateKey)
}

//查询ETH余额
func ethBalance(account common.Address) {
	//查询余额
	balance, err := client.BalanceAt(context.Background(), account, nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("balance:", balance)

	//转换精度
	fbalance := new(big.Float)
	fbalance.SetString(balance.String())
	ethValue := new(big.Float).Quo(fbalance, big.NewFloat(math.Pow10(18)))
	fmt.Println("ethValue:", ethValue)

	//未结余额
	pendingBalance, err := client.PendingBalanceAt(context.Background(), account)
	pbalance := new(big.Float)
	pbalance.SetString(pendingBalance.String())
	pethValue := new(big.Float).Quo(pbalance, big.NewFloat(math.Pow10(18)))
	fmt.Println("pendingBalance:", pethValue)
}

//查询ERC20余额
func erc20Balance(account common.Address) {
	tokenAddress := common.HexToAddress("0x9bf18D220392eEa1e1600c9759603C1CF0bBaF84")
	instance, err := erc20.NewToken(tokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	//代币名称
	name, err := instance.Name(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}

	//代币symbol
	symbol, err := instance.Symbol(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}

	//代币精度
	decimals, err := instance.Decimals(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}

	//代币发行量
	totalSupplyWei, err := instance.TotalSupply(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}
	fsupply := new(big.Float)
	fsupply.SetString(totalSupplyWei.String())
	totalSupply := new(big.Float).Quo(fsupply, big.NewFloat(math.Pow10(int(decimals))))

	fmt.Printf("name: %s\n", name)
	fmt.Printf("symbol: %s\n", symbol)
	fmt.Printf("decimals: %v\n", decimals)
	fmt.Printf("totalSupply: %f\n", totalSupply)

	//查询ERC20余额
	balance, err := instance.BalanceOf(&bind.CallOpts{}, account)
	if err != nil {
		log.Fatal(err)
	}
	//转换精度
	fbal := new(big.Float)
	fbal.SetString(balance.String())
	value := new(big.Float).Quo(fbal, big.NewFloat(math.Pow10(int(decimals))))
	fmt.Printf("erc20Balance: %f\n", value)

}

//ERC20转账
func erc20Transfer(tokenAddress common.Address, toAddress common.Address, value *big.Int, privateKey *ecdsa.PrivateKey) {
	instance, err := erc20.NewToken(tokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	fmt.Println("publicKeyECDSA:", publicKeyECDSA)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	gasLimit := uint64(60000)
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	opts := &bind.TransactOpts{
		From:      fromAddress,
		Nonce:     new(big.Int).SetUint64(nonce),
		Signer:    nil,
		Value:     value,
		GasPrice:  gasPrice,
		GasFeeCap: nil,
		GasTipCap: nil,
		GasLimit:  gasLimit,
		Context:   ctx,
		NoSend:    false,
	}
	txs, err := instance.Transfer(opts, toAddress, value)
	_ = txs
}

//判断是否合约
func isContract(address common.Address) {
	re := regexp.MustCompile("^0x[0-9a-fA-F]{40}$")
	fmt.Printf("is valid: %v\n", re.MatchString("0x323b5d4c32345ced77393b3530b1eed0f346429d")) // is valid: true
	fmt.Printf("is valid: %v\n", re.MatchString("0xZYXb5d4c32345ced77393b3530b1eed0f346429d")) // is valid: false

	bytecode, err := client.CodeAt(context.Background(), address, nil) // nil is latest block
	if err != nil {
		log.Fatal(err)
	}

	isContract := len(bytecode) > 0
	fmt.Printf("is contract: %v\n", isContract) // is contract: true
}

//查询区块
func block() {
	header, err := client.HeaderByNumber(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("header.Number:", header.Number.String())

	blockNumber := header.Number
	block, err := client.BlockByNumber(context.Background(), blockNumber)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("block.Number:", block.Number().Uint64())
	fmt.Println("block.Time:", block.Time())
	fmt.Println("block.Difficulty:", block.Difficulty().Uint64())
	fmt.Println("block.Hash:", block.Hash().Hex())
	fmt.Println("transactions.len:", len(block.Transactions()))

	count, err := client.TransactionCount(context.Background(), block.Hash())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("TransactionCount:", count)
}

//查询交易
func tranctions() {
	header, err := client.HeaderByNumber(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}
	blockNumber := header.Number
	block, err := client.BlockByNumber(context.Background(), blockNumber)
	if err != nil {
		log.Fatal(err)
	}

	for _, tx := range block.Transactions() {
		fmt.Println("tx.Hash:", tx.Hash().Hex())
		fmt.Println("tx.Value:", tx.Value().String())
		fmt.Println("tx.Gas:", tx.Gas())
		fmt.Println("tx.GasPrice:", tx.GasPrice().Uint64())
		fmt.Println("tx.Nonce:", tx.Nonce())
		fmt.Println("tx.Data:", tx.Data())
		fmt.Println("tx.To:", tx.To().Hex())

		chainID, err := client.NetworkID(context.Background())
		if err != nil {
			log.Fatal(err)
		}

		if msg, err := tx.AsMessage(types.NewEIP155Signer(chainID), nil); err == nil {
			fmt.Println("msg.From:", msg.From().Hex())
		}

		receipt, err := client.TransactionReceipt(context.Background(), tx.Hash())
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("receipt.Status:", receipt.Status) // 1
	}

	blockHash := common.HexToHash("0xd632d94c513d50ef309aba16e1465d12498cf5506d1adb904e920a5b9fd4162e")
	count, err := client.TransactionCount(context.Background(), blockHash)
	if err != nil {
		log.Fatal(err)
	}

	for idx := uint(0); idx < count; idx++ {
		tx, err := client.TransactionInBlock(context.Background(), blockHash, idx)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("tx.Hash2:", tx.Hash().Hex())
	}

	txHash := common.HexToHash("0x9fff0e52ca2d9152b3cc988df16994002b9ff131f95f1d2dcea828d8f71f6c5e")
	tx, isPending, err := client.TransactionByHash(context.Background(), txHash)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("tx.Hash3:", tx.Hash().Hex())
	fmt.Println("isPending:", isPending)
}

//转账ETH
func transferEth(toAddress common.Address, value *big.Int, privateKey *ecdsa.PrivateKey) {
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	fmt.Println("publicKeyECDSA:", publicKeyECDSA)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("fromAddress:", fromAddress)
	fmt.Println("nonce:", nonce)

	gasLimit := uint64(60000)
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("gasPrice:", gasPrice)

	var data []byte
	//tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, data)
	tx := types.NewTx(&types.LegacyTx{
		Nonce:    nonce,
		GasPrice: gasPrice,
		Gas:      gasLimit,
		To:       &toAddress,
		Value:    value,
		Data:     data,
	})

	chainID, err := client.NetworkID(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("chainID:", chainID)

	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), privateKey)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("signedTx:", signedTx)

	err = client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("tx sent: %s\n", signedTx.Hash().Hex())
}

//转账代币
func transferToken(tokenAddress common.Address, toAddress common.Address, value *big.Int, privateKey *ecdsa.PrivateKey) {
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	transferFnSignature := []byte("transfer(address,uint256)")
	hash := sha3.NewLegacyKeccak256()
	hash.Write(transferFnSignature)
	methodID := hash.Sum(nil)[:4]
	fmt.Println(hexutil.Encode(methodID))

	paddedAddress := common.LeftPadBytes(toAddress.Bytes(), 32)
	fmt.Println(hexutil.Encode(paddedAddress))

	paddedAmount := common.LeftPadBytes(value.Bytes(), 32)
	fmt.Println(hexutil.Encode(paddedAmount))

	var data []byte
	data = append(data, methodID...)
	data = append(data, paddedAddress...)
	data = append(data, paddedAmount...)

	/*gasLimit, err := client.EstimateGas(context.Background(), ethereum.CallMsg{
		To:   &toAddress,
		Data: data,
	})*/
	gasLimit := uint64(60000)
	if err != nil {
		log.Fatal(err)
	}
	//tx := types.NewTransaction(nonce, tokenAddress, big.NewInt(0), gasLimit, gasPrice, data)
	tx := types.NewTx(&types.LegacyTx{
		Nonce:    nonce,
		GasPrice: gasPrice,
		Gas:      gasLimit,
		To:       &tokenAddress,
		Value:    big.NewInt(0), // in wei (0 eth)
		Data:     data,
	})

	chainID, err := client.NetworkID(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), privateKey)
	if err != nil {
		log.Fatal(err)
	}

	err = client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("tx sent: %s", signedTx.Hash().Hex())
}
