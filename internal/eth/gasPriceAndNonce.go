package main

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
)

func main() {

	client, err := ethclient.Dial("https://polygon-mumbai.g.alchemy.com/v2/hzmOmzsETexTqJWD0XMukB9-ZMiNMUTC")
	if err != nil {
		log.Fatal(err)
	}
	getGasPrice(client)

	getNonce("0x8f8D3DdB5890A40de180E526AC0de105Ee02365c", client)
	getNonce("0x88805E14193CFC648e1E36b7E163352F96AB7EF9", client)
	getNonce("0x271c15C63ee3e91F5406e9929Fe098b01309421f", client)
	getNonce("0x5aC6B8F6B3B71A1AF78B0D960AFE81a5f230947E", client)
	getNonce("0x37b7E5Bb04E6b94860FDdD4258E038cAd2288803", client)
	getNonce("0x61cC25b38DFF4bA9C4A97fa0713698294f112Cb5", client)
	getNonce("0xE14389309680B848974Df8f944BD91543Daef553", client)
}

func getNonce(address string, client *ethclient.Client) {
	fmt.Println("address:", address)
	fromAddress := common.HexToAddress(address)
	pendingNonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("pendingNonce:", pendingNonce)

	blockNumber, _ := client.BlockNumber(context.Background())
	lastNonce, err := client.NonceAt(context.Background(), fromAddress, new(big.Int).SetUint64(blockNumber))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("lastNonce:", lastNonce)
	fmt.Println("")
}

func getGasPrice(client *ethclient.Client) *big.Int {
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("gasPrice:", gasPrice)
	return gasPrice
}
