package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rlp"
)

func main() {
	client, err := ethclient.Dial("https://matic-mumbai.chainstacklabs.com")
	if err != nil {
		log.Fatal(err)
	}
	rawTx := "f86e4b8519c3e6464c82ea6094e99d1263dd97c02b315c94d777b4b4d80be6435587093cafac6a80008083027125a03f1582a3d159d8140295f2dd09a9c6b51d5a8c59ba0015e0623fd194df1049d3a037f2d41a9e5efa3740c8b150091fd36aa124b7f4a8db5a545173e7e6ba0e76b9"
	rawTxBytes, err := hex.DecodeString(rawTx)
	tx := new(types.Transaction)
	rlp.DecodeBytes(rawTxBytes, &tx)
	err = client.SendTransaction(context.Background(), tx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("tx sent: %s", tx.Hash().Hex())
}
