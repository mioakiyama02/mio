package main

import (
	"domain/internal/pkg/util"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func main() {
	privateKey, err := crypto.HexToECDSA("323c44f9fffe8d3e8fca19392b1c3a24e007b1a119032c730a8a366996aa0ae3")
	if err != nil {
		log.Fatal(err)
	}
	msg := []byte("hello")
	signatureByte, err := crypto.Sign(util.SaltHash(msg), privateKey)
	if err != nil {
		log.Fatal(err)
	}
	// web逻辑
	signatureByte[64] += 27
	fmt.Println("signature:", hexutil.Encode(signatureByte))
}
