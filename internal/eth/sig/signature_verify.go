package main

import (
	"bytes"
	"crypto/ecdsa"
	"domain/internal/pkg/util"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func main() {
	privateKey, err := crypto.HexToECDSA("a85e1956451df3b616b02e481bd261b40bedc30a4e745b53624afdff54ef24a6")
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	fmt.Println("publicKey:", publicKey)

	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	fmt.Println("publicKeyECDSA:", publicKeyECDSA)
	fmt.Println("PubkeyToAddress1:", crypto.PubkeyToAddress(*publicKeyECDSA))
	if !ok {
		log.Fatal("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
	}

	publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)
	msg := []byte("hello")
	digestHash := util.SaltHash(msg)
	fmt.Println("digestHash:", digestHash)

	signature, err := crypto.Sign(digestHash, privateKey)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("signature:", hexutil.Encode(signature))

	sigPublicKey, err := crypto.Ecrecover(digestHash, signature)
	if err != nil {
		log.Fatal(err)
	}

	matches := bytes.Equal(sigPublicKey, publicKeyBytes)
	fmt.Println("sigPublicKey:", sigPublicKey)
	fmt.Println("publicKeyBytes:", publicKeyBytes)
	fmt.Println("matches1:", matches)

	sigPublicKeyECDSA, err := crypto.SigToPub(digestHash, signature)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("PubkeyToAddress2:", crypto.PubkeyToAddress(*sigPublicKeyECDSA))

	sigPublicKeyBytes := crypto.FromECDSAPub(sigPublicKeyECDSA)
	matches = bytes.Equal(sigPublicKeyBytes, publicKeyBytes)
	fmt.Println("sigPublicKeyECDSA:", sigPublicKeyECDSA)
	fmt.Println("sigPublicKeyBytes:", sigPublicKeyBytes)
	fmt.Println("publicKeyBytes:", publicKeyBytes)
	fmt.Println("matches2:", matches)

	signatureNoRecoverID := signature[:len(signature)-1]
	verified := crypto.VerifySignature(publicKeyBytes, digestHash, signatureNoRecoverID)
	fmt.Println("verified:", verified)
}
