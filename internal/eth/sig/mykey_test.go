package main

import (
	"encoding/hex"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"log"
	"strings"
	"testing"
)

const (
	signFormat = "\x19Ethereum Signed Message:\n%d%s"
)

func TestVerifty(t *testing.T) {
	ETHPrv := "323c44f9fffe8d3e8fca19392b1c3a24e007b1a119032c730a8a366996aa0ae3"
	ETHAdd := "0x5A607ffA2E10DB83f5F1a977dA484c02156e5b6c"
	//待签名的数据
	data := "hello"

	prvKey, _ := hex.DecodeString(ETHPrv)
	sig, _ := SignWithEth([]byte(data), prvKey)
	log.Println("sig:", sig)

	unsignDataHashByte := crypto.Keccak256Hash(common.FromHex(data)).Bytes()
	// 开始服务端签名验证
	log.Println("verify sign:", verifySig2(strings.ToLower(ETHAdd), sig, unsignDataHashByte))
}

func SignWithEthWeb3(unsignData string, privateByte []byte) (signature string, err error) {
	// 需要先做一次Hash
	unsignDataHashByte := crypto.Keccak256Hash(common.FromHex(unsignData)).Bytes()
	return SignWithEth(unsignDataHashByte, privateByte)
}

func SignWithEth(unsignData, privateKeyByte []byte) (signature string, err error) {
	// web3签名需要加盐后做Hash，对hash数据进行签名
	unsignDataHash := createSignHash(unsignData)

	key, err := crypto.ToECDSA(privateKeyByte)
	if err != nil {
		log.Println("sign ToECDSA err:", err.Error())
		return "", err
	}
	signatureByte, err := crypto.Sign(unsignDataHash, key)
	if err != nil {
		log.Println("sign Sign err:", err.Error())
		return "", err
	}
	// web逻辑
	signatureByte[64] += 27
	return hexutil.Encode(signatureByte), nil
}

// 验证签名
func verifySig2(from, sigHex string, msg []byte) bool {
	fmt.Println("from:", from)
	fmt.Println("sigHex:", sigHex)
	fmt.Println("msg:", hexutil.Encode(msg))

	fromAddr := common.HexToAddress(from)
	sig := hexutil.MustDecode(sigHex)
	if sig[64] != 1 && sig[64] != 0 && sig[64] != 27 && sig[64] != 28 {
		log.Println("in hexutil.MustDecode error.")
		return false
	}

	if sig[64] != 1 && sig[64] != 0 {
		sig[64] -= 27
	}
	pubKey, err := crypto.SigToPub(createSignHash(msg), sig)
	if err != nil {
		log.Println("in crypto.SigToPub error:", err.Error())
		return false
	}

	recoveredAddr := crypto.PubkeyToAddress(*pubKey)
	log.Println("recoveredAddr:", recoveredAddr.String())
	return fromAddr == recoveredAddr
}

// 待签名数据生成符合格式的hash
func createSignHash(data []byte) []byte {
	msg := fmt.Sprintf(signFormat, len(data), data)
	return crypto.Keccak256([]byte(msg))
}
