package main

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func main() {
	fmt.Println(verifySig(
		"0x5A607ffA2E10DB83f5F1a977dA484c02156e5b6c",
		"0xce44e2ba5568d765fc1b663f4c69de88d18d8e057185978afada752a9df530223d8ecb6ddfcef320ecaef988a2a0df36c290ec89ce73920053b151e995e699211b",
		[]byte("hello"),
	))
}

func verifySig(from, sigHex string, msg []byte) bool {
	fmt.Println("from:", from)
	fmt.Println("sigHex:", sigHex)
	fmt.Println("msg:", hexutil.Encode(msg))

	fromAddr := common.HexToAddress(from)
	sig := hexutil.MustDecode(sigHex)

	// https://github.com/ethereum/go-ethereum/blob/55599ee95d4151a2502465e0afc7c47bd1acba77/internal/ethapi/api.go#L442
	if sig[64] != 27 && sig[64] != 28 {
		return false
	}
	sig[64] -= 27

	fmt.Println("signature2:", hexutil.Encode(sig))

	pubKey, err := crypto.SigToPub(signHash(msg), sig)
	if err != nil {
		return false
	}
	recoveredAddr := crypto.PubkeyToAddress(*pubKey)
	return fromAddr == recoveredAddr
}

// https://github.com/ethereum/go-ethereum/blob/55599ee95d4151a2502465e0afc7c47bd1acba77/internal/ethapi/api.go#L404
// signHash is a helper function that calculates a hash for the given message that can be
// safely used to calculate a signature from.
//
// The hash is calculated as
//   keccak256("\x19Ethereum Signed Message:\n"${message length}${message}).
//
// This gives context to the signed message and prevents signing of transactions.
func signHash(data []byte) []byte {
	msg := fmt.Sprintf("\x19Ethereum Signed Message:\n%d%s", len(data), data)
	return crypto.Keccak256([]byte(msg))
}
