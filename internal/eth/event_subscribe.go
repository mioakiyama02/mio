package main

import (
	"context"
	"fmt"
	"log"
	"math/big"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

const (
	url0 = "http://184.72.2.83:8545"
	url1 = "ws://184.72.2.83:8546"
	url2 = "wss://ws-matic-mumbai.chainstacklabs.com"
)

func main() {
	client, err := ethclient.Dial(url2)
	if err != nil {
		log.Fatal(err)
	}

	contractAddress := common.HexToAddress("0x9bf18D220392eEa1e1600c9759603C1CF0bBaF84")
	query := ethereum.FilterQuery{
		FromBlock: big.NewInt(22348605),
		Addresses: []common.Address{contractAddress},
	}

	logs := make(chan types.Log)
	sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
	if err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case err := <-sub.Err():
			log.Fatal(err)
		case vLog := <-logs:
			fmt.Println(vLog) // pointer to event log
		}
	}
}
