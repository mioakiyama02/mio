package middlewares

import (
	"github.com/gin-gonic/gin"
	"strings"
)

func Filter() gin.HandlerFunc {
	return func(c *gin.Context) {
		account := c.GetHeader("account")
		if account != "" {
			c.Request.Header.Set("account", strings.ToLower(account))
		}
	}
}
