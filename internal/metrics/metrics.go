package metrics

import (
	"context"
	"errors"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"net"
	"net/http"
	_ "net/http/pprof"
)

var (
	Enabled = false
)

type Metrics struct {
	*http.Server
	config *Config
	router *mux.Router
}

func New(config *Config) *Metrics {
	log.Infof("New metrics service with config: %+v", config)
	srv := &Metrics{
		config: config,
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(version.Print("domain")))
	})

	http.Handle(config.Path, promhttp.Handler())
	srv.Server = &http.Server{Addr: config.Addr}
	Enabled = true
	return srv
}

func (m *Metrics) Start(ctx context.Context) error {
	m.BaseContext = func(net.Listener) context.Context {
		return ctx
	}
	log.Printf("[Metrics] server listening on: %s%s", m.config.Addr, m.config.Path)
	if err := m.Server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

func (m *Metrics) Stop(ctx context.Context) error {
	log.Info("[metrics] server stopping")
	return m.Shutdown(ctx)
}
