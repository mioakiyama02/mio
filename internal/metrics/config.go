package metrics

import "time"

type Config struct {
	Enabled bool          `yaml:"enabled"`
	Addr    string        `yaml:"addr"`
	Path    string        `yaml:"path"`
	Timeout time.Duration `yaml:"timeout"`
}

var DefaultConfig = Config{
	Enabled: true,
	Addr:    "localhost:9191",
	Path:    "/metrics",
	Timeout: time.Second,
}
