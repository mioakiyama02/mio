package service

import (
	"context"
	"domain/internal/send/store"
)

type SendService struct {
	Service
}

func NewSendService(srv Service) *SendService {
	return &SendService{
		Service: srv,
	}
}

func (srv *SendService) SendMail(ctx context.Context) error {
	var (
		list, err = srv.QueryMailEventList(ctx)
	)
	if err != nil {
		return err
	}
	for i := 0; i < len(list); i++ {
		switch list[i].Event {
		case "MakeOfferForERC721":
			mailAddress, err0 := srv.store.QueryMailAddress(list[i].ToAddress)
			if err0 == nil {
				srv.store.SendMailToMailAddress("MakeOfferForERC721", mailAddress, list[i])
			}
		case "OfferToSellForERC721":
		case "FixedPriceOrderForERC721":
		case "FixedPriceBuyForERC721":

		}
	}
	return nil
}

func (srv *SendService) QueryMailEventList(ctx context.Context) ([]store.MailEventBO, error) {
	return srv.store.QueryMailEventList(ctx)
}
