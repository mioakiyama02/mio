package service

import (
	"domain/internal/metrics"
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

// Metric descriptors.
var (
	durations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:  "domain",
			Subsystem:  "service",
			Name:       "durations_seconds",
			Help:       "send latency distributions.",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"method"},
	)
)

func init() {
	prometheus.MustRegister(durations)
}

func observe(start time.Time, lvs ...string) {
	if metrics.Enabled {
		durations.WithLabelValues(lvs...).Observe(time.Since(start).Seconds())
	}
}
