package service

import (
	"context"
	"domain/internal/send/conf"
	"domain/internal/send/store"
	"time"
)

type Service struct {
	config *conf.Config
	store  store.Store
}

func New(cfg *conf.Config, store store.Store) Service {
	return Service{
		config: cfg,
		store:  store,
	}
}

func (s *Service) startTimer(ctx context.Context, f func(ctx context.Context) error, d time.Duration) {
	if d == 0 {
		return
	}
	t := time.NewTimer(d)
	go func() {
		defer func() {
			if err := recover(); err != nil {
				log.Error(err)
			}
		}()

		//启动的时候先执行一次
		t.Reset(time.Second)
		for {
			select {
			case <-ctx.Done():
				log.Info("canceled!!!")
				return
			case <-t.C:
				err := f(ctx)
				if err != nil {
					log.Error(err)
				}
				t.Reset(d)
			}
		}
	}()
}
