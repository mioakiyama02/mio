package store

import (
	"context"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
)

type MailStore struct {
	Cache    *cache.Redis
	DataBase *dataBase.DataBase
}

//store Store 具体实现
type store struct {
	Cache    *cache.Redis
	DataBase *dataBase.DataBase
}

// GetBatchSize 获取 MySQL 每批次的大小
func (s store) GetBatchSize() int {
	return s.DataBase.Config.BatchSize
}

func New(cache *cache.Redis, mysql *dataBase.DataBase) Store {
	return &store{
		Cache:    cache,
		DataBase: mysql,
	}
}

type (
	// Store 存储接口
	Store interface {
		QueryMailEventList(ctx context.Context) ([]MailEventBO, error)
		QueryMailAddress(address string) (string, error)
		SendMailToMailAddress(mailType string, mailAddress string, list MailEventBO) error
	}
)
