package store

import (
	"context"
	"encoding/json"
	"github.com/jordan-wright/email"
	"net/smtp"
	"time"
)

type MailEventBO struct {
	NftId       int    `db:"nft_id"`
	Event       string `db:"event"`
	FromAddress string `db:"from_address"`
	ToAddress   string `db:"to_address"`
	TxHash      string `db:"tx_hash"`
}

type MailTextBO struct {
	MailId      int    `db:"mail_id"`
	NftId       int    `db:"nft_id"`
	Event       string `db:"event"`
	FromAddress string `db:"from_address"`
	ToAddress   string `db:"to_address"`
	TxHash      string `db:"tx_hash"`
}

type MailLogBO struct {
	NftId                int    `db:"nft_id"`
	Event                string `db:"event"`
	SendToAccountAddress string `db:"send_to_account_address"`
	SendToMailAddress    string `db:"send_to_mail_address"`
}

const (
	QueryMailEventListSQL = `
		SELECT 
			COALESCE(nft_id,'') AS nft_id,
			COALESCE(event,'') AS event,
			COALESCE(from_address,'') AS from_address,
			COALESCE(to_address,'') AS to_address,
			COALESCE(tx_hash,'') AS tx_hash
		FROM 
			t_nft_event_log
		WHERE 
			mail_send_status = 0`
	QueryMailSQL         = `SELECT email FROM t_account WHERE account_address=?`
	QueryMailTextSQL     = `SELECT text FROM t_mail WHERE mail_type=?`
	InsertMailLogSQL     = `INSERT INTO t_mail_log(nft_id, event, send_to_account_address, send_to_mail_address, text, date) VALUES (:nft_id,:event,:send_to_account_address,:send_to_mail_address,:text,:date)`
	UpdateMailSendStatus = `UPDATE t_nft_event_log SET mail_send_status=1 WHERE nft_id=:nft_id AND tx_hash=:tx_hash`
)

func (s *store) QueryMailEventList(ctx context.Context) ([]MailEventBO, error) {
	var (
		MailEventList []MailEventBO
		err1          error
	)
	err0 := s.DataBase.Dao.Select(&MailEventList, QueryMailEventListSQL)
	if err0 == nil && err1 == nil {
		if MailEventList != nil && len(MailEventList) > 0 {
			return MailEventList, nil
		} else {
			return MailEventList, nil
		}
	} else {
		return MailEventList, err0
	}
	return MailEventList, nil
}

func (s *store) QueryMailAddress(address string) (string, error) {
	var (
		mailAddress string
		err         = s.DataBase.Dao.QueryRow(QueryMailSQL, address).Scan(&mailAddress)
	)
	if err != nil {
		return "", err
	}
	return mailAddress, nil
}

func (s *store) SendMailToMailAddress(mailType string, mailAddress string, list MailEventBO) error {
	var (
		j, _ = json.Marshal(list)
		e    = email.NewEmail()
		//mail MailTextBO
		//err  = s.DataBase.Dao.QueryRow(QueryMailTextSQL, mailType).Scan(&mail.Text)
	)
	//if err != nil {
	//	return err
	//}
	e.From = "NFT.IO <shiina96@163.com>"
	e.To = []string{mailAddress}
	e.Subject = mailType
	e.Text = j
	err := e.Send("smtp.163.com:25", smtp.PlainAuth("", "shiina96@163.com", "ZJGSKHWCUQHFAQVU", "smtp.163.com"))
	if err != nil {
		log.Fatal(err)
	} else {
		_, err = s.DataBase.Dao.NamedExec(InsertMailLogSQL, map[string]interface{}{
			"nft_id":                  list.NftId,
			"event":                   list.Event,
			"send_to_account_address": list.ToAddress,
			"send_to_mail_address":    mailAddress,
			"text":                    string(j),
			"date":                    time.Now(),
		})
	}
	if err == nil {
		_, err = s.DataBase.Dao.NamedExec(UpdateMailSendStatus, map[string]interface{}{
			"nft_id":  list.NftId,
			"tx_hash": list.TxHash,
		})
	} else {
		return err
	}
	return nil
}
