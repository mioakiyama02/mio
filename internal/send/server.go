package send

import (
	"context"
	"domain/app/server"
	"domain/internal/pkg/cache"
	"domain/internal/pkg/dataBase"
	"domain/internal/send/conf"
	"domain/internal/send/service"
	"domain/internal/send/store"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"time"
)

type Server struct {
	config      *conf.Config
	servers     []server.Server
	sendService *service.SendService
}

func New(cfg *conf.Config, cache *cache.Redis, dataBase *dataBase.DataBase) server.Server {
	log.Infof("New trace service with config: %+v", cfg)

	sto := store.New(cache, dataBase)
	svc := service.New(cfg, sto)

	sendService := service.NewSendService(svc)

	//新建 server 实例
	srv := &Server{
		config:      cfg,
		sendService: sendService,
	}
	return srv
}

func (srv *Server) Start(ctx context.Context) error {
	log.Info("[TSEND] server starting")
	g, ctx := errgroup.WithContext(ctx)

	// 启用发送邮件
send:
	err := srv.sendService.SendMail(ctx)
	if err != nil {
		return err
	}
	time.Sleep(60)
	goto send
	// 启动监视器
	/*g.Go(func() error {
		return srv.startMonitor(ctx)
	})*/

	for _, s := range srv.servers {
		ss := s
		g.Go(func() error {
			return ss.Start(ctx)
		})
	}
	if err := g.Wait(); err != nil && !errors.Is(err, context.Canceled) {
		return err
	}
	return nil
}

func (srv *Server) Stop(ctx context.Context) error {
	log.Info("[SEND] server stopping")
	for _, s := range srv.servers {
		s.Stop(ctx)
	}
	return nil
}
