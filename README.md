

###创建合约对象
cd contracts

solc --abi erc20.sol

abigen --abi=abi/erc20_sol_ERC20.abi --pkg=token --out=erc20.go

abigen --abi=abi/DomainERC721.abi --pkg=DomainERC721 --out=DomainERC721/DomainERC721.go

abigen --abi=abi/DomainExchange.abi --pkg=DomainExchange --out=DomainExchange/DomainExchange.go

abigen --abi=abi/DomainMintFactory.abi --pkg=DomainMintFactory --out=DomainMintFactory/DomainMintFactory.go

abigen --abi=abi/DomainVault.abi --pkg=DomainVault --out=DomainVault/DomainVault.go


###交叉编译
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build main.go
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build main.go
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build main.go